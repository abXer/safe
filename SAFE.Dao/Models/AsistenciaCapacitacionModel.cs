﻿using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class AsistenciaCapacitacionModel : TrabajadorModel
    {
        private string _fecha { get; set; }



        public long id { get; set; }
        public string fecha
        {
            get
            {
                return this._fecha;
            }
            set
            {
                this._fecha = Services.ConvierteFechaJava(value, true);
            }
        }
        public string registro { get; set; }
        public long idTrabajador { get; set; }
    }
}
