﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class IngenieroModel : PersonaModel
    {
        public long? idIngeniero { get; set; }
    }
}
