﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class RegionModel
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public RegionModel() { }
    }
}
