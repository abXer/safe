﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class TrabajadorModel : PersonaModel
    {
        public long id { get; set; }
        public string cargo { get; set; }
        public long idEmpresa { get; set; }
    }
}
