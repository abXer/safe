﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class FichaMedicaModel
    {
        public long? id { get; set; }
        public string diagnostico { get; set; }
        public long idTrabajador { get; set; }
        public long? idAtencionMedica { get; set; }
        public long idEmpresa { get; set; }
    }
}
