﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class PersonaModel
    {
        public int idTipo { get; set; }
        public int? idUsuario { get; set; }
        public int rut { get; set; }
        public string nombre { get; set; }
        public string apepat { get; set; }
        public string apemat { get; set; }
        public string email { get; set; }
        public string direccion { get; set; }
        private string _fechaIngreso { get; set; }
        public string fechaIngreso
        {
            get => this._fechaIngreso;
            set => this._fechaIngreso = Utils.Services.ConvierteFechaJava(value, true);
        }
        public string celular { get; set; }
        private string _fechaNacimiento { get; set; }
        public string fechaNacimiento
        {
            get => this._fechaNacimiento;
            set => this._fechaNacimiento = Utils.Services.ConvierteFechaJava(value, true);
        }
        public int idComuna { get; set; }
        public int idEmpresa { get; set; }

    }
}
