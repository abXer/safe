﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class InstalacionEmpresaModel
    {

        public long? idInstalacion { get; set; }
        public string telefonoInstalacion { get; set; }
        public string direccionIntalacion { get; set; }
        public long idEmpresa { get; set; }
        public int idComuna { get; set; }
        public string nombreComuna { get; set; }
        public int idRegion { get; set; }
    }
}
