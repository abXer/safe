﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class UsuarioModel
    {
        public dataUsuarioModel usuario { get; set; }
        public PersonaModel persona { get; set; }
        public EmpresaModel empresa { get; set; }
    }
    public class dataUsuarioModel
    {
        public int id { get; set; }
        public string password { get; set; }
        public string estado { get; set; }
        public int idPerfil { get; set; }
    }
}
