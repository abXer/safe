﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class TokenModel
    {
        public long rutUsuario { get; set; }
        public string password { get; set; }

        public bool loginCorrecto { get; set; }
        public string token { get; set; }
        private string _fechaExpíracion { get; set; }
        public string fechaExpiracion
        {
            get => this._fechaExpíracion;
            set => this._fechaExpíracion = Utils.Services.ConvierteFechaJava(value, true);
        }

    }
}
