﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class PerfilModel
    {
        public int id { get; set; }
        public string descripcion { get; set; }
    }
}
