﻿using SAFE.Dao.Dao;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class EvaluacionModel
    {
        public long? id { get; set; }
        private string _fechaEvaluacion { get; set; }
        public string fechaEvaluacion
        {
            get
            {
                return this._fechaEvaluacion;
            }
            set
            {
                this._fechaEvaluacion = Utils.Services.ConvierteFechaJava(value, true);
            }
        }
        public string observacion { get; set; }
        public long idTecnico { get; set; }
        public int informeOK { get; set; }
        public int tipoEvaluacion { get; set; }
        //public int etapaEva { get; set; }
        public long idInstalacion { get; set; }
        public long idEmpresa { get; set; }
        public long? idInstaoTraba { get; set; }

        public int idComuna { get; set; }
        public int idRegion { get; set; }




    }
}
