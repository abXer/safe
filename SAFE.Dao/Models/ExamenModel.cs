﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class ExamenModel
    {
        public int idEmpresa { get; set; }
        public int idFichaMedica { get; set; }
        public string fechaExamen { get; set; }
        public string tipoExamen { get; set; }
        public int id { get; set; }
        
    }
}
