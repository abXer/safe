﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class AgendaMedicaModel
    {
        public long idEmpresa { get; set; }

        public long? id { get; set; }
        private string _fechaDisponible { get; set; }
        public string fechaDisponible
        {
            get
            {
                return this._fechaDisponible;
            }
            set
            {
                this._fechaDisponible = Utils.Services.ConvierteFechaJava(value, true);
            }
        }

        public string confirmado { get; set; }


        public long idMedico { get; set; }
    }
}
