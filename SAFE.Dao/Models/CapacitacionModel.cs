﻿using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class CapacitacionModel
    {
        private string _fechaInicio { get; set; }
        private string _fechaTermino { get; set; }


        public long? id { get; set; }
        public string asunto { get; set; }
        public string fechaInicio
        {
            get
            {
                return this._fechaInicio;
            }
            set
            {
                this._fechaInicio = Services.ConvierteFechaJava(value, true);
            }
        }
        public string fechaTermino
        {
            get
            {
                return this._fechaTermino;
            }
            set
            {
                this._fechaTermino = Services.ConvierteFechaJava(value, true);
            }
        }
        public int cupoMinimo { get; set; }
        public long? idSupervisor { get; set; }
        public long idExpositor { get; set; }
        public long idEmpresa { get; set; }
        public int cantidadAsistencia { get; set; }
    }
}

