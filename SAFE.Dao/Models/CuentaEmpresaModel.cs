﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class CuentaEmpresaModel
    {
        public int rut { get; set; }
        public char DV { get; set; }
        public string nombre { get; set; }
        public string teléfono { get; set; }
        public string dirección { get; set; }
        public ComunaModel comuna { get; set; }        
        public RegionModel región { get; set; }
        public string tipoDeEmpresa { get; set; }
    }
}
