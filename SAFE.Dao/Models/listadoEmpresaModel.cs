﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class listadoEmpresaModel
    {
        public EmpresaModel empresa { get; set; }
        public ComunaModel comuna { get; set; }
        public RegionModel region { get; set; }
    }
}
