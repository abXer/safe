﻿using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class EmpresaModel
    {

        private string _fechaIngreso { get; set; }

        public int? id { get; set; }
        public int rut { get; set; }
        public string nombre { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string fechaIngreso
        {
            get => this._fechaIngreso;
            set => this._fechaIngreso = Services.ConvierteFechaJava(value, true);
        }

        public string fechaTermino
        {
            get
            {
                if (!string.IsNullOrEmpty(this.fechaIngreso))
                {
                    if (DateTime.TryParse(this.fechaIngreso, out var nuevaFechaTermnino))
                    {
                        nuevaFechaTermnino = nuevaFechaTermnino.AddYears(1);
                        return Services.ConvierteFechaJava(nuevaFechaTermnino.ToShortDateString(), true);
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else return string.Empty;
            }
        }

        public string rubro { get; set; }
        public int idComuna { get; set; }
        public string estado { get; set; }
    }
}
