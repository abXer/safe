﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class AsociacionPerfilMenu
    {
        public int idPerfil { get; set; }
        public int idMenu { get; set; }
    }
}
