﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class AtencionMedicaModel
    {
        public long? id { get; set; }
        private string _fechaAtencion { get; set; }
        public string fechaAtencion
        {
            get
            {
                return this._fechaAtencion;
            }
            set
            {
                this._fechaAtencion = Utils.Services.ConvierteFechaJava(value, true);
            }
        }
        public string recomendacionesAM { get; set; }
        public long idAgendaMedico { get; set; }
        public long idEmpresa { get; set; }

    }
}
