﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class MedicoModel : PersonaModel
    {
        public long? id { get; set; }
        public string especialidad { get; set; }
    }
}
