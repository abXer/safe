﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Models
{
    public class RevisionEvaluacionModel
    {
        public long? idEmpresa { get; set; }

        public int tipoEvaluacion { get; set; }
        public long? idRevision { get; set; }
        public long? id { get; set;  }
        public long? idEvaInstOTrab { get; set; }
        public long? idIngeniero { get; set; }
        public string recomendacion { get; set; }
        public string recomendaciones { get; set; }
        public bool informeOK { get; set; }
        public long? idSupervisor { get; set; }
    }
}
