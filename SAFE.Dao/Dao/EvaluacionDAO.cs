﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class EvaluacionDAO
    {
        public static IList<EvaluacionModel> ObtenerEvaluacionRutEmpresa(long rutEmpresa)
        {
            return new JsonDeserializer().Deserialize<List<EvaluacionModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.EVALUACION.listaEvaluacionesEmpresa, rutEmpresa)));
        }

        public static IList<RevisionEvaluacionModel> listadoRevisionesEvaluacion(int rutEmpresa)
        {
            return new JsonDeserializer().Deserialize<List<RevisionEvaluacionModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.EVALUACION.leerRevisiones, rutEmpresa)));
            //return new List<RevisionEvaluacionModel>();
        }

        public static bool GuardarEvaluacion(EvaluacionModel evaluacion)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.EVALUACION.guardar, evaluacion);
        }

        public static bool AgregarRevision(RevisionEvaluacionModel rev)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.EVALUACION.agregarRevision, rev);
        }
    }
}
