﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class TrabajadorDAO
    {
        public static IList<TrabajadorModel> listaTrabajadoresEmpresa(long idEmpresa)
        {
            return new JsonDeserializer().Deserialize<List<TrabajadorModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.TRABAJADOR.OBTENER_TRABAJADOR, idEmpresa)));
        }
    }
}
