﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class ComunaDAO
    {

        public static IList<ComunaModel> listaComunas(int idRegion)
        {
            return new JsonDeserializer().Deserialize<List<ComunaModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.comuna, idRegion)));
        }

        public static ComunaModel obtenerComuna(int idComuna)
        {
            return listaComunas(idComuna).FirstOrDefault(x => x.id == idComuna);
        }
    }
}
