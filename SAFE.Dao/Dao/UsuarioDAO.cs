﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class UsuarioDAO
    {
        public static IList<UsuarioModel> listaUsuarios()
        {
            return new JsonDeserializer().Deserialize<List<UsuarioModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.USUARIO.obtenerListaUsuarios));
            //List<UsuarioModel> _lst = new List<UsuarioModel>();
            //return _lst;

        }

        public static TokenModel Autenticar(TokenModel _tokenAutenticacion)
        {
            //return new TokenModel() { loginCorrecto = true, fechaExpiracion = DateTime.Now.AddDays(1).ToShortDateString(), token = "contenido token" };

            dynamic respuesta = Services.LlamarApi(Enums.RUTAREQUESTS.USUARIO.Autenticar, _tokenAutenticacion, true);

            if (respuesta != null)
                respuesta = new JsonDeserializer().Deserialize<TokenModel>(respuesta);

            return respuesta;
        }

        public static IList<PerfilModel> listaPerfiles()
        {
            return new JsonDeserializer().Deserialize<List<PerfilModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.USUARIO.Perfiles));
        }

        public static List<AsociacionPerfilMenu> asociacionPerfilMenus()
        {
            return new JsonDeserializer().Deserialize<List<AsociacionPerfilMenu>>(Services.LlamarApi(Enums.RUTAREQUESTS.USUARIO.AsociacionPerfilMenu));
        }

        public static IList<MenuModel> listaMenus()
        {
            return new JsonDeserializer().Deserialize<List<MenuModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.USUARIO.Menus));
        }
    }
}
