﻿using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp.Deserializers;
using SAFE.Dao.Utils;

namespace SAFE.Dao.Dao
{
    public class SaludDAO
    {
        public static IList<AgendaMedicaModel> listaAgendaMedicaEmpresa(int rut)
        {
            return new JsonDeserializer().Deserialize<List<AgendaMedicaModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.SALUD.AgendasMedicasEmpresa, rut)));
        }

        public static IList<ExamenModel> listaExamenesEmpresa(int rutEmpresa)
        {
            return new JsonDeserializer().Deserialize<List<ExamenModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.SALUD.ExamenesEmpresa, rutEmpresa)));
        }

        public static IList<FichaMedicaModel> listaFichasMedicasEmpresa(int rutEmpresa)
        {
            return new JsonDeserializer().Deserialize<List<FichaMedicaModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.SALUD.fichasMedicasEmpresa, rutEmpresa)));
        }

        public static IList<AgendaMedicaModel> listaAgendaMedicaDoctor(int rutDoctor)
        {
            return new JsonDeserializer().Deserialize<List<AgendaMedicaModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.SALUD.AgendaMedicaDoctor, rutDoctor)));
        }

        public static IEnumerable<MedicoModel> listaMedicos()
        {
            return new JsonDeserializer().Deserialize<List<MedicoModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.SALUD.Medicos)));
        }

        public static AgendaMedicaModel ObtenerAgendaMedicaPorID(long idAgendaMedica)
        {
            return new JsonDeserializer().Deserialize<AgendaMedicaModel>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.SALUD.AgendaMedica, idAgendaMedica)));
        }

        public static bool guardarAgendaMedica(AgendaMedicaModel agenda)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.SALUD.GuardarAgendaMedica, agenda);
        }

        public static bool guardarAtencionMedica(AtencionMedicaModel atencion)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.SALUD.GuardarAtencionMedica, atencion);
        }

        public static bool guardarFichaMedica(FichaMedicaModel ficha)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.SALUD.GuardarFichaMedica, ficha);
        }

        public static IList<AtencionMedicaModel> listaAtencionesMedicas(long idAgendaMedica)
        {
            return new JsonDeserializer().Deserialize<List<AtencionMedicaModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.SALUD.AtencionesMedicas, idAgendaMedica)));
        }
    }
}
