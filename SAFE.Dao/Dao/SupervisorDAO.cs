﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class SupervisorDAO
    {
        public static List<SupervisorModel> listaSupervisores()
        {
            return new JsonDeserializer().Deserialize<List<SupervisorModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.SUPERVISOR.listaSupervisores));
            ////List<SupervisorModel> _lst = new List<SupervisorModel>();

            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #1", apepat = "ap paterno Supervisor #1", celular = "12321312", direccion = "av falsa Supervisor #1", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 1, idSupervisor = 1, nombre = "nombre Supervisor #1", rut = 11969133 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #2", apepat = "ap paterno Supervisor #2", celular = "12321312", direccion = "av falsa Supervisor #2", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 2, idSupervisor = 2, nombre = "nombre Supervisor #2", rut = 15963526 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #3", apepat = "ap paterno Supervisor #3", celular = "12321312", direccion = "av falsa Supervisor #3", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 3, idSupervisor = 3, nombre = "nombre Supervisor #3", rut = 12509618 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #4", apepat = "ap paterno Supervisor #4", celular = "12321312", direccion = "av falsa Supervisor #4", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 4, idSupervisor = 4, nombre = "nombre Supervisor #4", rut = 15869017 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #5", apepat = "ap paterno Supervisor #5", celular = "12321312", direccion = "av falsa Supervisor #5", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 5, idSupervisor = 5, nombre = "nombre Supervisor #5", rut = 15336785 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #6", apepat = "ap paterno Supervisor #6", celular = "12321312", direccion = "av falsa Supervisor #6", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 6, idSupervisor = 6, nombre = "nombre Supervisor #6", rut = 11245875 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #7", apepat = "ap paterno Supervisor #7", celular = "12321312", direccion = "av falsa Supervisor #7", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 7, idSupervisor = 7, nombre = "nombre Supervisor #7", rut = 10561637 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #8", apepat = "ap paterno Supervisor #8", celular = "12321312", direccion = "av falsa Supervisor #8", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 8, idSupervisor = 8, nombre = "nombre Supervisor #8", rut = 11739610 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #9", apepat = "ap paterno Supervisor #9", celular = "12321312", direccion = "av falsa Supervisor #9", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 9, idSupervisor = 9, nombre = "nombre Supervisor #9", rut = 13956253 });
            ////_lst.Add(new SupervisorModel() { apemat = "ap materno Supervisor #10", apepat = "ap paterno Supervisor #10", celular = "12321312", direccion = "av falsa }Supervisor #9", email = "asd@gmail.com", fechaIngreso = DateTime.Now.ToShortDateString(), fechaNacimiento = DateTime.Now.ToShortDateString(), idComuna = 10, idSupervisor = 10, nombre = "nombre }Supervisor #10", rut = 14219918 });


            ////return _lst;
        }
    }
}
