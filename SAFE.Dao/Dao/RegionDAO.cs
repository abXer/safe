﻿using RestSharp;
using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class RegionDAO
    {
        public static IList<RegionModel> obtenerRegiones()
        {
            return new JsonDeserializer().Deserialize<List<RegionModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.listaRegiones));
        }
    }
}
