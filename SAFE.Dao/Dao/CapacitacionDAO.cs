﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class CapacitacionDAO
    {

        public static IList<CapacitacionModel> listaCapacitacionesEmpresa(int rutEmpresa)
        {
            return new JsonDeserializer().Deserialize<List<CapacitacionModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.CAPACITACION.listaCapacitacionesEmpresa, rutEmpresa)));
        }

        public static bool Guardar(CapacitacionModel cap)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.CAPACITACION.guardar, cap);
        }

        public static bool AgregarAsistenciaCapacitacion(AsistenciaCapacitacionModel asi)
        {
            Services.LlamarApi(Enums.RUTAREQUESTS.CAPACITACION.agregarAsistente, asi);
            return true;
        }

        public static IList<AsistenciaCapacitacionModel> listaAsistentesCapacitacion(long idCapacitacion)
        {
            return new JsonDeserializer().Deserialize<List<AsistenciaCapacitacionModel>>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.CAPACITACION.listaAsistentesCapacitacion, idCapacitacion)));
        }
    }
}
