﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;

namespace SAFE.Dao.Dao
{
    public class AdministradorDAO
    {
        public static IList<AdministradorModel> listaAdministradores()
        {
            return new JsonDeserializer().Deserialize<List<AdministradorModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.ADMINISTRADOR.LISTA_ADMINISTRADORES));
        }
    }
}
