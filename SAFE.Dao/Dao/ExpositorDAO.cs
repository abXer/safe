﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class ExpositorDAO
    {
        public static List<ExpositorModel> listaExpositores()
        {
            return new JsonDeserializer().Deserialize<List<ExpositorModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.EXPOSITOR.listaExpositores));
        }
    }
}
