﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class EmpresaDAO
    {
        public static IList<listadoEmpresaModel> listadoEmpresas()
        {
            return new JsonDeserializer().Deserialize<List<listadoEmpresaModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.EMPRESA.listadoEmpresas)); ;
        }

        public static IList<InstalacionEmpresaModel> listaInstalacionesEmpresa(int rutEmpresa)
        {
            IList<InstalacionEmpresaModel> lst = new List<InstalacionEmpresaModel>();

            dynamic respuesta = Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.EMPRESA.instalacionesEmpresa, rutEmpresa));
            
           lst = new JsonDeserializer().Deserialize<List<InstalacionEmpresaModel>>(respuesta);

            return lst;
        }
        public static bool GuardarEmpresa(EmpresaModel empresa)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.EMPRESA.guardarEmpresa, empresa);
        }

        #region ...
        public static void ondb()
        {
            Services.LlamarApi(Enums.BASEAPI.ONDB);
        }
        #endregion
    }
}
