﻿using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp.Deserializers;
using SAFE.Dao.Utils;

namespace SAFE.Dao.Dao
{
    public class IngenieroDAO
    {
        public static IList<IngenieroModel> listadoIngenieros()
        {
            return new JsonDeserializer().Deserialize<List<IngenieroModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.INGENIERO.LISTA_INGENIEROS));
        }
    }
}
