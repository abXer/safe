﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class PersonaDAO
    {
        public static PersonaModel ObtenerRut(int rut)
        {
            return new JsonDeserializer().Deserialize<PersonaModel>(Services.LlamarApi(string.Format(Enums.RUTAREQUESTS.PERSONA.OBTENER_PERSONA_RUT, rut)));
        }

        public static bool GuardarPersona(PersonaModel persona)
        {
            return Services.LlamarApi(Enums.RUTAREQUESTS.PERSONA.GUARDAR_PERSONA, persona);
        }
    }
}
