﻿using RestSharp.Deserializers;
using SAFE.Dao.Models;
using SAFE.Dao.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Dao
{
    public class TecnicoDAO
    {
        public static List<TecnicoModel> listaTecnicos()
        {
            return new JsonDeserializer().Deserialize<List<TecnicoModel>>(Services.LlamarApi(Enums.RUTAREQUESTS.TECNICO.OBTENER_TECNICO_LISTA));
        }
    }
}
