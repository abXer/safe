using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Utils
{
    public static class Enums
    {
        public class RUTAREQUESTS
        {
            public const string
                listaRegiones = "/Region",
                comuna = "/Region/{0}/Comuna"
                ;

            public class EVALUACION
            {
                public const string
                    listaEvaluacionesEmpresa = "/Evaluacion/{0}",
                    guardar = "/AddAndUpdateEvaluacion",
                    agregarRevision = "/AddAndUpdateRevEvaluacion",
                    leerRevisiones = "/EvaluacionRevision/{0}"
                    ;
            }

            public class CAPACITACION
            {
                public const string
                    listaCapacitacionesEmpresa = "/Capacitaciones/{0}",
                    guardar = "/InsertarCapacitacion",
                    agregarAsistente = "/InsertarAsistenciaCapacitacion",
                    listaAsistentesCapacitacion = "/AsistenciaCapacitaciones/{0}"
                    ;
            }

            public class SALUD
            {
                public const string
                    AgendasMedicasEmpresa = "/AgendaMedica/{0}",
                    AgendaMedica = "/AgendaMedica/id/{0}",
                    AtencionesMedicas = "/AtencionMedica/{0}",
                    GuardarAgendaMedica = "/AgendaMedica",
                    GuardarAtencionMedica = "/AtencionMedica",
                    GuardarFichaMedica = "/FichaMedica",
                    AgendaMedicaDoctor = "/AgendaMedica/rutDoctor/{0}",
                    ExamenesEmpresa = "/ExamenEmpresa/{0}",
                    fichasMedicasEmpresa = "/FichaMedica/{0}",
                    Medicos = "/Medicos"
                    ;


            }


            public class EMPRESA
            {
                public const string
                obtenerCuentaEmpresa = "/Empresa/{0}",
                listadoEmpresas = "/Empresa",
                instalacionesEmpresa = "/InstalacionEmpresa/{0}", //rutEmpresa
                guardarEmpresa = "/Empresa"
                ;
            }

            public class USUARIO
            {
                public const string
                 obtenerListaUsuarios = "/Usuario",
                 Autenticar = "/Autenticar",
                 Perfiles = "/Perfiles",
                 Menus = "/Menu",
                 AsociacionPerfilMenu = "/AsociacionPerfilMenu"
                ;
            }

            public class PERSONA
            {
                public const string 
                OBTENER_PERSONA_RUT = "/Persona/{0}",
                GUARDAR_PERSONA = "/Persona"
                    
                ;
            }

            public class TECNICO
            {
                public const string
                    OBTENER_TECNICO_RUT = "/Tecnico/{0}",
                    OBTENER_TECNICO_ID = "/Tecnico?/{0}",
                    OBTENER_TECNICO_LISTA = "/Tecnico"
                    ;
            }

            public class SUPERVISOR
            {
                public const string
                    listaSupervisores = "/Supervisor"
                    ;
            }

            public class TRABAJADOR
            {
                public const string
                    OBTENER_TRABAJADOR = "/Trabajador/{0}"
                    ;
            }

            public class INGENIERO
            {
                public const string
                    LISTA_INGENIEROS = "/Ingenieros"
                    ;
            }

            public class ADMINISTRADOR
            {
                public const string
                    LISTA_ADMINISTRADORES = "/Administrador"
                    ;
            }

            public class EXPOSITOR
            {
                public const string
                    listaExpositores = "/Expositores"
                    ;
            }
        }
        public class BASEAPI
        {
            public const string
                API_SAFE = "https://safeservice.azurewebsites.net/ProyectoSAFE-1.0/",
                ONDB = "/onBD"
                ;
        }
        public class METODO
        {
            public const Method
                GET = Method.GET,
                POST = Method.POST
                ;
        }

        public class TIPOEVALUACION
        {
            public const int
                TRABAJADORES = 2,
                INSTALACIONES = 1
            ;
        }
    }
}
