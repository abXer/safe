﻿using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Dao.Utils
{
    internal static class Services
    {
        private const string baseApi = Enums.BASEAPI.API_SAFE;
        internal static IRestResponse LlamarApi(string rutaRequest, int intentos = 0)
        {
            IRestResponse respuesta = null;

            try
            {
                Debug.WriteLine($"Inicio GET: {rutaRequest}");

                var cliente = new RestClient(baseApi) { Timeout = (1000 * 3) };
                IRestRequest request = null;

                cliente.ClearHandlers();
                cliente.AddHandler("application/json", new JsonDeserializer());
                request = new RestRequest(rutaRequest, RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                var s = new Stopwatch();
                s.Start();
                respuesta = cliente.Execute(request);
                s.Stop();


                if (respuesta.ResponseStatus == ResponseStatus.TimedOut && intentos <= 3)
                    return LlamarApi(rutaRequest, intentos++);

                Debug.WriteLine($"Termina GET: {rutaRequest} en {intentos} intentos, tiempo: {s.Elapsed}");
                Debug.WriteLine($"Respuesta GET: Status: {respuesta.StatusCode}; Content {respuesta.Content}");

            }
            catch (Exception e)
            {

                var a = e;
            }

            if (intentos > 3 || (respuesta.StatusCode == HttpStatusCode.InternalServerError || respuesta.StatusCode == HttpStatusCode.NotFound))
            {
                respuesta = null;
            }

            //if (respuesta != null && respuesta.StatusCode == HttpStatusCode.OK)

            return respuesta;
        }

        public static bool LlamarApi(string rutaRequest, object obj)
        {
            try
            {
                RestClient cliente = new RestClient(baseApi);
                IRestRequest request = null;
                IRestResponse respuesta = null;

                cliente.ClearHandlers();
                cliente.AddHandler("application/json", new JsonDeserializer());

                request = new RestRequest(rutaRequest, Method.POST) { RequestFormat = DataFormat.Json }.AddBody(obj);

                respuesta = cliente.Execute(request);

                return (respuesta.StatusCode == HttpStatusCode.OK && respuesta.Content == "true") ? true : false;
            }
            catch (Exception e)
            {
                var a = e;
                return false;
            }
        }

        public static IRestResponse LlamarApi(string rutaRequest, object obj, bool post = true)
        {
            try
            {
                RestClient cliente = new RestClient(baseApi);
                IRestRequest request = null;
                IRestResponse respuesta = null;

                cliente.ClearHandlers();
                cliente.AddHandler("application/json", new JsonDeserializer());

                request = new RestRequest(rutaRequest, Method.POST) { RequestFormat = DataFormat.Json }.AddBody(obj);

                respuesta = cliente.Execute(request);

                return (respuesta.StatusCode == HttpStatusCode.OK) ? respuesta : (IRestResponse)null;
            }
            catch (Exception e)
            {
                var a = e;
                return (IRestResponse)null;
            }
        }

        public static string ConvierteFechaJava(string fecha, bool formatoCorto)
        {
            if (!DateTime.TryParse(fecha, out var _fecha))
            {
                var fechaInicial = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return formatoCorto ? fechaInicial.AddMilliseconds(Convert.ToDouble(fecha)).ToShortDateString() : fechaInicial.AddMilliseconds(Convert.ToDouble(fecha)).ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                return _fecha.ToString("yyyy-MM-dd");
            }
        }
    }
}
