﻿namespace SAFE.Desk
{
    using System.Windows;
    using GalaSoft.MvvmLight.Threading;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            DashboardView window = new DashboardView();
            
            window.ShowDialog();
        }
        static App()
        {
            DispatcherHelper.Initialize();
        }
    }
}
