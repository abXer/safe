﻿using SAFE.Desk.Model;
using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Desk.Services
{
    public sealed class BusinessContext : IDisposable
    {
        private readonly DataContext context;
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessContext"/> class.
        /// </summary>
        public BusinessContext()
        {
            context = new DataContext();
        }

        /// <summary>
        /// Gets the underlying <see cref="DataContext"/>.
        /// </summary>
        public DataContext DataContext
        {
            get { return context; }
        }

        /// <summary>
        /// Adds a new customer entity to the data store.
        /// </summary>
        //public void AddNewEmpresa(Empresa _empresa)
        //{
        //    Check.Require(_empresa.rut.ToString());
        //    Check.Require(_empresa.nombre);
        //    Check.Require(_empresa.direccion);
        //    Check.Require(_empresa.fechaIngreso);
        //    Check.Require(_empresa.rubroEmpresa);
        //    Check.Require(_empresa.telefono);
        //    Check.Require(_empresa.comuna.idComuna.ToString());

        //    context.FormEmpresaFactory.Add(_empresa);
        //    context.SaveChanges();
        //}



    static class Check
        {
            // ReSharper disable once UnusedParameter.Local
            public static void Require(string value)
            {
                if (value == null)
                    throw new ArgumentNullException();

                if (value.Trim().Length == 0)
                    throw new ArgumentException();
            }
        }

        #region COMANDOS DISPOSABLE
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed || !disposing)
                return;

            if (context != null)
                context.Dispose();

            disposed = true;
        }
        #endregion


    }
}
