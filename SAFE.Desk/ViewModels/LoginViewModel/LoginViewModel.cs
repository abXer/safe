﻿using System;
using SAFE.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using SAFE.Desk.ViewModels.Base;
using System.Windows.Input;

namespace SAFE.Desk.ViewModels
{
    public class LoginViewModel : ViewModel
    {
        //private readonly ILoginService loginService;
        private string username;
        private string password;

        //public LoginViewModel(ILoginService loginService)
        //{
        //    this.LoginService = loginService;
        //    LoginCommand = new ActionCommand(password => Login());
        //}

        public ICommand LoginCommand { get; }

        public string Username
        {
            get { return username; }
            set
            {
                username = value;
                NotifyPropertyChanged("Username");
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                NotifyPropertyChanged("Password");
            }
        }
       // #region Commands

       // public RelayCommand SignInCommand
       // {
       //     get { return _signInCommand; }
       //     set
       //     {
       //         _signInCommand = value;
       ////         this.RaisePropertyChanged(() => SignInCommand);
       //     }
       // }

       // #endregion

      
    }
}
