﻿using SAFE.Domain.Entities;

namespace SAFE.Desk.ViewModels
{
    public class InstalacionViewModel : ViewModel
    {
        public long IdInstalacionEmpresa { get; set; }
        public Contacto contacto { get; set; }
        public string Direccion { get; set; }
        public Comuna Comuna { get; set; }
        public long IdEmpresa { get; set; }
    }
}
