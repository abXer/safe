﻿namespace SAFE.Desk.ViewModels
{
    using Caliburn.Micro;
    using SAFE.Domain.Forms;
    using SAFE.Domain.Entities;
    using System.Windows.Controls;
    using System.Collections.ObjectModel;
    

    public class InstalacionCollectionViewModel : ViewModel
    {
        //private int _idempresa;
        private readonly int _rutEmpresa = 12;
    

        public ObservableCollection<DataGridColumn> ColumnCollection
        {
            get;
            private set;
        }


        public InstalacionCollectionViewModel()
        {
            cargaInstalacionEmpresaEnGrid();
        }
        public BindableCollection<Instalacion> listadoInstalaciones { get; set; }
        public void cargaInstalacionEmpresaEnGrid()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoInstalaciones = new BindableCollection<Instalacion>(FormServiciosGeneralesFactory.obtenerInstalacionesEmpresa(_rutEmpresa));
        }

    }
}
