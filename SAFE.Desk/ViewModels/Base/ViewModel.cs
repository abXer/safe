﻿namespace SAFE.Desk.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using SAFE.Desk.ViewModels.Base;
    using Prism.Commands;
    using SAFE.Domain.Entities;
    using System.Windows.Input;

    public abstract class ViewModel : ObservableObject, IDataErrorInfo
    {
        public string this[string columnName]
        {
            get { return OnValidate(columnName); }
        }

         string IDataErrorInfo.Error
        {
            get { return null; }
        }

        protected virtual string OnValidate(string propertyName)
        {
            var context = new ValidationContext(this)
            {
                MemberName = propertyName
            };
            var results = new Collection<ValidationResult>();
            var isValid = Validator.TryValidateObject(this, context, results, true);

            return !isValid ? results[0].ErrorMessage : null;
        }

     
        
   

        public RelayCommand CancelarCommand { get; set; }
        public RelayCommand GuardarCommand { get; set; }
        public RelayCommand EliminarEmpresaCommand { get; set; }

     

    }
}
