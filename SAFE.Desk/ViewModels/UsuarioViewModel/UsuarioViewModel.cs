﻿using System;
using SAFE.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace SAFE.Desk.ViewModels
{
    public class UsuarioViewModel : ViewModel
    {
        private int idUsuario;
        private string password;
        private string rut;
        private bool estado;
        private string tipousuario;
        private Perfil idPerfil;

        public int IDUsuario
        {
            get { return idUsuario; }
            set
            {
                idUsuario = value;
                NotifyPropertyChanged();
            }
        }
        
        [Required]
        [StringLength(32, MinimumLength = 6)]
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                NotifyPropertyChanged();
            }
        }

        [Required]
        [StringLength(10, MinimumLength =3)]
        public string Rut
        {
            get { return rut; }
            set
            {
                rut = value;
                NotifyPropertyChanged();
            }
        }

        [Required]
        public bool Estado
        {
            get { return estado; }
            set
            {
                estado = value;
                NotifyPropertyChanged();
            }
        }

        [Required]
        public Perfil IDPerfil
        {
            get { return idPerfil; }
            set
            {
                idPerfil = value;
                NotifyPropertyChanged();
            }
        }

        [Required]
        public string TipoUsuario
        {
            get { return tipousuario; }
            set
            {
                tipousuario = value;
                NotifyPropertyChanged();
            }
        }

        protected override string OnValidate(string propertyName)
        {
            if (Rut != null && Rut.Contains(" "))
                return "Rut ingresado no puede ser menor a 1 caracter";
            if(Password != null && Password.Contains(" "))
                return "Password no puede ser nulo y no puede contener espacio.";
            else
                return base.OnValidate(propertyName);
        }
    }
}
