﻿using SAFE.Domain.Forms;
using SAFE.Domain.Entities;
using Caliburn.Micro;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;
using SAFE.Desk.View;
using SAFE.Desk.ViewModel;

namespace SAFE.Desk.ViewModels
{
    public class PersonaCollectionViewModel : ViewModel
    {
      

        public ObservableCollection<DataGridColumn> ColumnCollection
        {
            get;
            private set;
        }

        public PersonaCollectionViewModel() => cargaPersonasEnGrid();

        public BindableCollection<Persona> listadoPersonas { get; set; }
        public void cargaPersonasEnGrid()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoPersonas = new BindableCollection<Persona>(FormServiciosGeneralesFactory.obtenerExpositores());
        }

        #region COMANDO PARA GUARDAR 

        #region ICommand
        public ICommand AddCommand
        {
            get;
            internal set;
        }

        public ICommand CancelCommand
        {
            get;
            internal set;
        }
        public ICommand EditCommand
        {
            get;
            internal set;
        }

        public ICommand DeleteCommand
        {
            get;
            internal set;
        }

        public ICommand SearchCommand
        {
            get;
            internal set;
        }
        #endregion

        #region CREA BOTONES

        private void CrearAddCommand()
        {
            AddCommand = new RelayCommand(AddExecute);
        }

        private void CrearDeleteCommand()
        {
            DeleteCommand = new RelayCommand(DeleteExecute);
        }

        private void CrearSearchCommand()
        {
            DeleteCommand = new RelayCommand(SearchExecute);
        }

        private void CrearEditCommand()
        {
            EditCommand = new RelayCommand(EditExecute);
        }

        private void CrearCancelCommand()
        {
            CancelCommand = new RelayCommand(CancelExecute);
        }
        #endregion

        public void AddExecute()
        {
            this.CloseWindow();
            PersonaView window = new PersonaView
            {
                DataContext = new PersonaViewModel()
            };
            window.ShowDialog();
        }

        public void DeleteExecute()
        {

            //MessageBox.Show(RutEmpresa + " " + NombreEmpresa + " " + DireccionEmpresa + " " + FonoEmpresa + " " + RubroEmpresa + " " + ComunaSeleccionada, "Nueva Empresa");

            //FormEmpresaFactory.GuardarEmpresa(_newFormEmpresa);
        }

        public void EditExecute()
        {
            this.CloseWindow();
            PersonaView window = new PersonaView
            {
                DataContext = new PersonaViewModel()
            };
            window.ShowDialog();
        }
        public void SearchExecute()
        {

            //MessageBox.Show(RutEmpresa + " " + NombreEmpresa + " " + DireccionEmpresa + " " + FonoEmpresa + " " + RubroEmpresa + " " + ComunaSeleccionada, "Nueva Empresa");

            //FormEmpresaFactory.GuardarEmpresa(_newFormEmpresa);
        }

        public void CancelExecute()
        {
            this.CloseWindow();
            MainWindow window = new MainWindow
            {
                DataContext = new MainViewModel()
            };
            window.ShowDialog();
        }


        #endregion


    }
}