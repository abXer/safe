﻿namespace SAFE.Desk.ViewModels
{
    using System;
    using SAFE.Domain.Utils;
    using SAFE.Domain.Forms;
    using SAFE.Domain.Entities;
    using SAFE.Desk.ViewModels.Base;
    using System.ComponentModel.DataAnnotations;
    using System.Windows.Input;
    using Prism.Commands;
    using Prism.Mvvm;
    using System.Collections.Generic;
    using Caliburn.Micro;
    using SAFE.Desk.Services;

    public class PersonaViewModel : ViewModel
    {
        private Lazy<Persona> _model = new Lazy<Persona>();
      //  public ICollection<Persona> _personas { get; private set; }


        #region DECLARACION DE CAMPOS

        private Rut _rutPersona;
        private string _nombre;
        private string _apellidoPaterno;
        private string _apellidoMaterno;
        private string _email;
        //private string _direccion;
        private string _fechaIngreso;
        private string _celular;
        private string _fechaNacimiento;
        private Comuna _idComuna;
        private Region _idRegion;
        private Empresa _idEmpresa;

        private Perfil idPerfil;
        private string password;
        private bool estado = true;

        [Required]
        [StringLength(10, MinimumLength = 10)]
        public Rut RutPersona
        {
            get { return _rutPersona; }
            set { _rutPersona = value; NotifyPropertyChanged("RutPersona"); }
        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string NombrePersona
        {
            get { return _nombre; }
            set { _model.Value.nombre = value; NotifyPropertyChanged("NombrePersona"); }
        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string ApellidoPaterno
        {
            get { return _apellidoPaterno; }
            set { _model.Value.apellidoPaterno = value; NotifyPropertyChanged("ApellidoPaterno"); }
        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string ApellidoMaterno
        {
            get { return _apellidoMaterno; }
            set { _model.Value.apellidoMaterno = value; NotifyPropertyChanged("ApellidoMaterno"); }
        }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string EmailPersona
        {
            get { return _email; }
            set { _model.Value.contacto.email = value; NotifyPropertyChanged("EmailPersona"); }
        }

        //[Required]
        //[StringLength(50, MinimumLength = 2)]
        //public string DireccionPersona
        //{
        //    get { return _direccion; }
        //    set { _model.Value.direccion = value; NotifyPropertyChanged("DireccionPersona"); }
        //}

        public string FechaIngreso
        {
            get { return _fechaIngreso; }
            set { _model.Value.fechaIngreso = value; NotifyPropertyChanged("FechaIngreso"); }
        }

        [Required]
        [StringLength(50, MinimumLength = 9)]
        public string CelularPersona
        {
            get { return _celular; }
            set { _model.Value.contacto.celular= value; NotifyPropertyChanged("CelularPersona"); }
        }

        [Required]
        public string FechaNacimiento
        {
            get { return _fechaNacimiento; }
            set { _model.Value.fechaNacimiento = value; NotifyPropertyChanged("FechaNacimiento"); }
        }

        [Required]
        [StringLength(5, MinimumLength = 1)]
        public Comuna IDComuna
        {
            get { return _idComuna; }
            set { _idComuna = value; NotifyPropertyChanged("IDComuna"); }
        }

        [Required]
        [StringLength(5, MinimumLength = 1)]
        public Region IDRegion
        {
            get { return _idRegion; }
            set { _idRegion = value; NotifyPropertyChanged("IDRegion"); }
        }

        [Required]
        [StringLength(5, MinimumLength = 1)]
        public Empresa IDEmpresa
        {
            get { return _idEmpresa; }
            set { _idEmpresa = value; NotifyPropertyChanged("IDEmpresa"); }
        }

        [Required]
        [StringLength(5, MinimumLength = 1)]
        public Perfil IDPerfil
        {
            get { return idPerfil; }
            set { idPerfil = value; NotifyPropertyChanged("IDPerfil"); }
        }

        [Required]
        [StringLength(30, MinimumLength = 6)]
        public string Password
        {
            get { return password; }
            set { password = value; NotifyPropertyChanged("Password"); }
        }

        [Required]
        public bool Estado
        {
            get { return estado; }
            set { estado = value; NotifyPropertyChanged("Estado"); }
        }

        #endregion

        #region VALIDACION DE VISTA
        public bool IsValid
        {
            get
            {
                return
                    !String.IsNullOrWhiteSpace(NombrePersona) &&
                    !String.IsNullOrWhiteSpace(ApellidoPaterno) &&
                    !String.IsNullOrWhiteSpace(ApellidoMaterno) &&
                    !String.IsNullOrWhiteSpace(EmailPersona) &&
                    !String.IsNullOrWhiteSpace(CelularPersona) ;
            }
        }
        #endregion

        #region CARGA DE COMBOBOX



        public BindableCollection<Region> loadCargarRegiones { get; set; }
        public BindableCollection<Comuna> loadCargarComunas { get; set; }

        public BindableCollection<Empresa> loadCargarEmpresas { get; set; }

     
        public void cargaRegiones()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            loadCargarRegiones = new BindableCollection<Region>(FormServiciosGeneralesFactory.obtenerRegiones());
        }

        public void cargaComunas(int idRegion)
        {
            //     FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            loadCargarComunas = new BindableCollection<Comuna>(FormServiciosGeneralesFactory.obtenerComunas(idRegion));
        }

        public void cargaEmpresas()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            loadCargarEmpresas = new BindableCollection<Empresa>(FormServiciosGeneralesFactory.obtenerEmpresas());
        }
     
        #endregion
        public PersonaViewModel()
        {
            //this.SaveCommand = new DelegateCommand(this.OnSave);
        }

        public ICommand SaveCommand
        {
            get;
            private set;
        }

        //private async void OnSave()
        //{
        //    this.IsEnabled = false;
        //    using (IDataService service = new FileDataService())
        //    {
        //        await service.SavePersonaAsync(this.Persona);
        //    }
        //    this.IsEnables = true;

        //}



        //public ActionCommand AddPersonaCommand
        //{
        //    get
        //    {
        //        return new ActionCommand(p => p.AddPersona
        //               (RutPersona, NombrePersona, ApellidoPaterno, ApellidoMaterno, EmailPersona, DireccionPersona, FechaIngreso, CelularPersona, FechaNacimiento, IDComuna, IDEmpresa),
        //                  p => IsValid);
        //    }
        //}

        //private void AddPersona
        //    (string rutPersona, string nombrePersona, string apellidoPaterno, string apellidoMaterno,
        //     string emailPersona, string direccionPersona, DateTime fechaIngresoPersona, string celularPersona,
        //     DateTime fechaNacimientoPersona, Comuna idComuna, Empresa idEmpresa)
        //{
        //    using (var api = new Persona())
        //    {
        //        var persona = new Persona
        //        {
        //            rut = rutPersona,
        //            NombrePersona = nombrePersona,
        //            ApellidoPaterno = apellidoPaterno,
        //            ApellidoMaterno = apellidoMaterno,
        //            EmailPersona = emailPersona,
        //            DireccionPersona = direccionPersona,
        //            FechaIngreso = fechaIngresoPersona,
        //            CelularPersona = celularPersona,
        //            FechaNacimiento = fechaNacimientoPersona,
        //            IDComuna = idComuna,
        //            IDEmpresa = idEmpresa
        //        };
        //        api.AddNuevaPersona(persona);
        //    }


        //}

        protected override string OnValidate(string propertyName)
        {
            if (RutPersona != null && RutPersona.rutCompleto.Contains(" "))
                return "Rut ingresado no puede ser menor a 1 caracter";
            if (Password != null && Password.Contains(" "))
                return "Password no puede ser nulo y no puede contener espacio.";
            if (NombrePersona != null || ApellidoPaterno != null || ApellidoMaterno != null 
                || CelularPersona != null)
                return "El campo NO puede ser nulo. Intente nuevamente";
            if (Password !=null && !Password.Contains(" "))
                return "La contraseña no puede estar en blanco. Intente nuevamente";

            else
                return base.OnValidate(propertyName);
        }


        #region CUADRO DE CONFIRMACION 

        private readonly IDialogService dialogService;

        public PersonaViewModel(IDialogService dialogService)
        {
            this.dialogService = dialogService;
            DisplayMessageCommand = new ActionCommand(p => DisplayMessage());
        }

        public ICommand DisplayMessageCommand { get; }

        private void DisplayMessage()
        {
            var viewModel = new DialogWindowViewModel("DESEA CCONFIRMAR ESTE PROCESO!");

            bool? result = dialogService.ShowDialog(viewModel);

            if (result.HasValue)
            {
                if (result.Value)
                {
                    // Accepted
                }
                else
                {
                    // Cancelled
                }
            }
        }
        #endregion

    }
}
