﻿namespace SAFE.Desk.ViewModels
{
    using SAFE.Domain.Forms;
    using SAFE.Domain.Entities;
    using Caliburn.Micro;
    using System.Collections.ObjectModel;
    using System.Windows.Controls;



    public class EmpresaCollectionViewModel : ViewModel
    {
        //private int _idempresa;

        private Instalacion instalaccion;
        private Rut _rut;

      
        public ObservableCollection<DataGridColumn> ColumnCollection
        {
            get;
            private set;
        }



        public EmpresaCollectionViewModel()
        {
            cargaEmpresasEnGrid();
        }
        public BindableCollection<Empresa> listadoEmpresas { get; set; }
        public BindableCollection<Instalacion> LoadInstalacionesEmpresa { get; set; }
        public void cargaEmpresasEnGrid()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoEmpresas = new BindableCollection<Empresa>(FormServiciosGeneralesFactory.obtenerEmpresas());
        }

        //public void cargaInstalacionesEmpresas()
        //{
        //    FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
        //    LoadInstalacionesEmpresa = new BindableCollection<Instalacion>(FormServiciosGeneralesFactory.obtenerInstalacionesEmpresa();
        //}
    }
}