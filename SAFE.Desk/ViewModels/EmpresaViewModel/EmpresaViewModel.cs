﻿namespace SAFE.Desk.ViewModels
{
    using System;
    using Caliburn.Micro;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using SAFE.Desk.Model;
    using System.Windows.Input;
    using SAFE.Domain.Forms;
    using SAFE.Domain.Entities;
    using SAFE.Desk.ViewModels.Base;
    using System.ComponentModel.DataAnnotations;
    using SAFE.Dao.Dao;
    using System.Windows;
    using SAFE.Desk.Services;

    public class EmpresaViewModel : ViewModel
    {
        FormEmpresa _newFormEmpresa = new FormEmpresa() { empresa = new Empresa() { rut = new Rut() } };
        //private readonly Rut _rut;
        private bool isEnabled;

        public EmpresaViewModel()
        {
            FormEmpresa _newFormEmpresa = new FormEmpresa();
            CrearSaveCommand();
            CrearCancelCommand();


        //    FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
        //    CargarRegiones = new BindableCollection<Region>(FormServiciosGeneralesFactory.obtenerRegiones());
        //   CargarComunas = new BindableCollection <Comuna> (ComunaFactory.listadoComunas(1));
        }

        #region DECLARACION DE CAMPOS

        private const string _rutEmpresa = "RutEmpresa";
        private const string _nombreEmpresa = "NombreEmpresa";
        private const string _direccionEmpresa = "DireccionDireccion";
        private const string _fechaIngresoEmpresa = "FechaIngreso";
        private const string _rubroEmpresa = "RubroEmpresa";
        private const string _telefonoEmpresa = "TelefonoEmpresa";



        [Required]
        [StringLength(8, MinimumLength = 3)]
        public string RutEmpresa
        {
            get { return Convert.ToString(_newFormEmpresa?.empresa?.rut?.rut); }
            set { _newFormEmpresa.empresa.rut.rut = Convert.ToInt32(value); RaisePropertyChanged(_rutEmpresa); }
        }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string NombreEmpresa
        {
            get { return _newFormEmpresa?.empresa?.nombre; }
            set { _newFormEmpresa.empresa.nombre = value; RaisePropertyChanged(_nombreEmpresa); }
        }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string DireccionEmpresa
        {
            get { return _newFormEmpresa?.empresa?.direccion; }
            set { _newFormEmpresa.empresa.direccion = value; NotifyPropertyChanged(_direccionEmpresa); }
        }

        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string FechaIngresoEmpresa
        {
            get { return _newFormEmpresa?.empresa?.fechaIngreso; }
            set
            {
                _newFormEmpresa.empresa.fechaIngreso = value; RaisePropertyChanged(_fechaIngresoEmpresa);
            }
        }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string RubroEmpresa
        {
            get { return _newFormEmpresa?.empresa?.rubroEmpresa; }
            set { _newFormEmpresa.empresa.rubroEmpresa = value; RaisePropertyChanged(_rubroEmpresa); }
        }

        [Required]
        [StringLength(12, MinimumLength = 9)]
        public string FonoEmpresa
        {
            get { return _newFormEmpresa?.empresa?.telefono; }
            set { _newFormEmpresa.empresa.telefono = value; NotifyPropertyChanged(_telefonoEmpresa); }
        }


        #endregion
        #region VALIDACION DE VISTA

        //  Valida el valor que indica que si la vista es valida.
        public bool IsValid
        {
            get
            {
                return
                    (!String.IsNullOrWhiteSpace(_nombreEmpresa) &&
                    !String.IsNullOrWhiteSpace(_rutEmpresa) &&
                    !String.IsNullOrWhiteSpace(_direccionEmpresa) &&
                    !String.IsNullOrWhiteSpace(_fechaIngresoEmpresa) &&
                    !String.IsNullOrWhiteSpace(_rubroEmpresa) &&
                    !String.IsNullOrWhiteSpace(_telefonoEmpresa));
            }
        }

        /// <summary>
        /// Obtiene o establec un valor que indica si la vista esta habilitada.
        /// </summary>
        /// <value>
        ///   <c>Verdadero</c> Si la vista esta habilitada; de otra manera, <c>falso</c>.
        /// </value>
        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set
            {
                if (this.isEnabled != value)
                {
                    this.isEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        #endregion

        #region CARGA DE COMBOBOX

        public BindableCollection<Region> CargarRegiones { get; set; }
        public BindableCollection<Comuna> CargarComunas { get; set; }
        private Comuna _idComuna;
        private Region _idRegion;

        [Required]
        public Region RegionSeleccionada
        {
            get { return _idRegion; }
            set
            {
                NotifyPropertyChanged("RegionSeleccionada");
                _idRegion = value;

            }
        }
        [Required]
        public Comuna IDComuna
        {
            get { return _idComuna; }
            set
            {
                _idComuna = value;
                //  _newFormEmpresa.empresa.comuna.idComuna = Convert.ToInt32(value);
                //IDRegion.idRegion = Convert.ToInt32(value);
                NotifyPropertyChanged("IDComuna");
            }
        }

        //public void CargaRegiones()
        //{
        //    FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
        //    CargarRegiones = new BindableCollection<Region>(FormServiciosGeneralesFactory.obtenerRegiones());
        //}


        public void CargaComunas(int idRegion)
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            CargarComunas = new BindableCollection<Comuna>(FormServiciosGeneralesFactory.obtenerComunas(idRegion));
        }


        #endregion

        #region COMANDO PARA GUARDAR 



        public ICommand SaveCommand
        {
            get;
            internal set;
        }

        private bool CanExecuteSaveCommand()
        {
            return !string.IsNullOrEmpty(NombreEmpresa);
        }

        private void CrearSaveCommand()
        {
            SaveCommand = new RelayCommand(SaveExecute, IsValid);
        }

        public void SaveExecute()
        {

            MessageBox.Show(RutEmpresa + " " + NombreEmpresa + " " + DireccionEmpresa + " " + FonoEmpresa + " " + RubroEmpresa, "Nueva Empresa");

            FormEmpresaFactory.GuardarEmpresa(_newFormEmpresa);
        }


        public ICommand CancelCommand
        {
            get;
            internal set;
        }

        private void CrearCancelCommand()
        {
            CancelCommand = new RelayCommand(CancelExecute);
        }

        public void CancelExecute()
        {
            NombreEmpresa = string.Empty;
            RubroEmpresa = string.Empty;
            RutEmpresa = null;
            DireccionEmpresa = string.Empty;
            FonoEmpresa = string.Empty;
            FechaIngresoEmpresa = string.Empty;
            //     IDRegion = null;
            //     IDComuna = null;
        }


        #endregion


        #region CUADRO DE CONFIRMACION

        private readonly IDialogService dialogService;

        public EmpresaViewModel(IDialogService dialogService)
        {
            this.dialogService = dialogService;
            DialogGuardar = new ActionCommand(p => DisplayMessage());
        }

        public ICommand DialogGuardar { get; }

        private void DisplayMessage()
        {
            var viewModel = new DialogWindowViewModel("GUARDAR REGISTRO?");

            bool? result = dialogService.ShowDialog(viewModel);

            if (result.HasValue)
            {
                if (result.Value)
                {
                    // Accepted
                }
                else
                {
                    // Cancelled
                }
            }
        }
        #endregion
    }
}
