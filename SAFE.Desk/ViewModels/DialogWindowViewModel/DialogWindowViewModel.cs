﻿namespace SAFE.Desk
{
    using SAFE.Desk.ViewModels.Base;
    using System.Windows.Input;
    using SAFE.Desk.Services;
    using System;

    public class DialogWindowViewModel : IDialogRequestClose
    {
        public DialogWindowViewModel(string message)
        {
            Message = message;
            OkCommand = new ActionCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(true)));
            CancelCommand = new ActionCommand(p => CloseRequested?.Invoke(this, new DialogCloseRequestedEventArgs(false)));
        }


        public event EventHandler<DialogCloseRequestedEventArgs> CloseRequested;

        public string Message { get; }
        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }

    }


}
