﻿using System;
using SAFE.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace SAFE.Desk.ViewModels
{
    public class PerfilesViewModel : ViewModel
    {
        public int idPerfil { get; set; }
        public string descripcion { get; set; }
        public Menu menu { get; set; }
    }
}
