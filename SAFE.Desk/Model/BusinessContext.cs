﻿namespace SAFE.Desk.Model
{

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.Remoting.Contexts;
    using SAFE.Domain.Entities;

    public sealed class BusinessContext : IDisposable
    {
        private readonly DataContext context;
        private bool disposed;

        public BusinessContext()
        {
            context = new DataContext();
        }

        //public DataContext DataContext { get { return context; } }
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public DataContext DataContext
        {
            get { return context; }
        }

        #region VALIDACION DE CAMPOS
        static class Check
        {
            public static void Require(string value)
            {
                if (value == null)
                    throw new ArgumentNullException();
                else if (value.Trim().Length == 0)
                    throw new ArgumentException();
            }
        }

        //  PARA APLICAR LA REVISION
        //  Check.Require(empresa.direccion);
        #endregion


        #region OPERACIONES CRUD
        public void AddNewEmpresa(Empresa empresa)
        {
            //if (empresa.rut == null)
            //    throw new ArgumentNullException("rut", "Rut no puede ser nulo.");
            //if (true)
            //{


            //}
            Check.Require(empresa.nombre);
            Check.Require(empresa.rubroEmpresa);
            Check.Require(empresa.fechaIngreso);
            Check.Require(empresa.telefono);
            Check.Require(empresa.direccion);

            //context.Empresa.Add(empresa);
            //context.SaveChanges();

            //Check.Require(empresa.rubroEmpresa);
            //throw new ArgumentNullException("nombre", "Nombre de la empresa no puede ser nulo.");
            //if (string.IsNullOrEmpty(empresa.nombre))
            //    throw new ArgumentNullException("Nombre de la empresa no puede ser una cadena vacia.", "nombre");
            //if (empresa.rubroEmpresa == null)
            //    throw new ArgumentNullException("rubroEmpresa", "Giro Comercial no puede ser nulo.");
            //if (string.IsNullOrEmpty(empresa.rubroEmpresa))
            //    throw new ArgumentNullException("Giro Comercial no puede ser una cadena vacia.", "rubroEmpresa");
            //if (empresa.fechaIngreso == null)
            //    throw new ArgumentNullException("fechaIngreso", "La fecha de ingreso no puede ser nulo.");
            //if (string.IsNullOrEmpty(empresa.fechaIngreso))
            //    throw new ArgumentNullException("Fecha de Ingreso no puede ser una cadena vacia.", "fechaIngreso");
            //if (empresa.telefono == null)
            //    throw new ArgumentNullException("telefono", "El teléfono de la empresa no puede ser nulo.");
            //if (string.IsNullOrEmpty(empresa.telefono))
            //    throw new ArgumentNullException("Teléfono de la empresa no puede ser una cadena vacia.", "telefono");
            //if (empresa.comuna == null)
            //    throw new ArgumentNullException("comuna", "La comuna donde se ubica la empresa no puede ser nulo.");


        }

        //public void UpdateEmpresa(Empresa empresa)
        //{
        //    var entity = context.Empresa.Find(empresa.rut);
        //    if (entity == null)
        //    {
        //        throw new NotImplementedException("Manejar de manera adecuada el diseño de la API.");
        //    }
        //    context.Entry(empresa).CurrentValues.SetValues(empresa);
        //    context.SaveChanges();
        //}

        //public ICollection<Empresa> GetEmpresasList()
        //{
        //    return context.Empresa.OrderBy(p => p.rut).ToArray();
        //}

        #endregion


    }
}
