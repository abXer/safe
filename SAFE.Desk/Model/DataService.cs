﻿using System;
using System.Threading.Tasks;
using SAFE.Domain.Entities;

namespace SAFE.Desk.Model
{
    public class DataService : IDataService
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to connect to the actual data service

            var item = new DataItem("Welcome to MVVM Light");
            callback(item, null);
        }

        public Task SaveEmpresaAsync(Empresa _empresa)
        {
            throw new NotImplementedException();
        }
    }
}