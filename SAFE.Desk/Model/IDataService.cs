﻿namespace SAFE.Desk.Model
{
    using System;
    using System.Linq;
    using System.Text;
    using SAFE.Domain.Entities;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    public interface IDataService : IDisposable
    {
        void GetData(Action<DataItem, Exception> callback);

        /// <summary>
        /// Guarda una empresa.
        /// </summary>
        /// <param name="_empresa">La empresa.</param>
        /// <returns>
        /// A task.
        /// </returns>
        Task SaveEmpresaAsync(Empresa _empresa);
    }
}
