﻿
namespace SAFE.Desk.Design
{
    using System;
    using System.Threading.Tasks;
    using SAFE.Desk.Model;
    using SAFE.Domain.Entities;

    public class DesignDataService : IDataService
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Light [design]");
            callback(item, null);
        }

        public Task SaveEmpresaAsync(Empresa _empresa)
        {
            throw new NotImplementedException();
        }
    }
}