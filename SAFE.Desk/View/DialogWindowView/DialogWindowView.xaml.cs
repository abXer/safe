﻿namespace SAFE.Desk
{ 
    using MahApps.Metro.Controls;
    using SAFE.Desk.Services;
/// <summary>
/// Lógica de interacción para DialogWindowView.xaml
/// </summary>
    public partial class DialogWindowView : MetroWindow, IDialog
    {
        public DialogWindowView()
        {
            InitializeComponent();
        }
    }
}
