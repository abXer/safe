﻿using Caliburn.Micro;
using MahApps.Metro.Controls;
using SAFE.Domain.Entities;
using SAFE.Domain.Forms;
using System;
using System.Windows;

namespace SAFE.Desk
{
    /// <summary>
    /// Interaction logic for DoctoresView.xaml
    /// </summary>
    public partial class DoctoresView : MetroWindow
    {

        private BindableCollection<Medico> listadoDoctores;
        private BindableCollection<AgendaMedica> listadoAgendaDoctores;

        public DoctoresView()
        {
            InitializeComponent();
            cargarDoctores();
        }
   
        public void cargarDoctores()
        {
            listadoDoctores = new BindableCollection<Medico>(FormServiciosGeneralesFactory.listaMedicos());
            foreach (var item in listadoDoctores)
            {
                grdDoctores.ItemsSource = null;
                grdDoctores.ItemsSource = listadoDoctores;
            }
        }

        public void cargarAgendaMedica( Rut rutDoctor)
        {
            listadoAgendaDoctores = new BindableCollection<AgendaMedica>(AgendaMedicaFactory.listaAgendaMedicaDoctor(rutDoctor));
            foreach (var item in listadoAgendaDoctores)
            {
                grdDoctores.ItemsSource = null;
                grdDoctores.ItemsSource = listadoDoctores;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            //FormEmpresa _emp = new FormEmpresa();
            //try
            //{
       
            // }
            //catch (Exception)
            //{
            //    string error = (bool)FormEmpresaFactory.GuardarEmpresa(_emp) ? "Instalación de Empresa guardada correctamente." : "Hubo un problema al registrar la Instalación, favor intente nuevamente más tarde.";
            //    MessageBox.Show(error);
            //}

        }

    

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {

     
        }

    }
}
