﻿using Caliburn.Micro;
using MahApps.Metro.Controls;
using SAFE.Domain.Entities;
using SAFE.Domain.Forms;
using System;
using System.Windows;

namespace SAFE.Desk
{
    /// <summary>
    /// Interaction logic for EmpresaView.xaml
    /// </summary>
    public partial class TrabajadoresView : MetroWindow
    {
        private BindableCollection<Comuna> cargarComunas;
        private BindableCollection<Evaluacion> listadoEvaluciones;
        private BindableCollection<Instalacion> listadoTrabajadores;

        public TrabajadoresView()
        {
            InitializeComponent();
            cargaRegion();
        }

        public BindableCollection<Region> CargarRegiones { get; private set; }

        public void cargaRegion()
        {
            CargarRegiones = new BindableCollection<Region>(FormServiciosGeneralesFactory.obtenerRegiones());
            foreach (var item in CargarRegiones)
            {
                cmbRegion.Items.Add(item);
            }
        }

    

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            FormEmpresa _emp = new FormEmpresa();
            try
            {
                //if(!((txtRut.Text.Contains(" ")) || (txtRut.Text.Equals("")) || txtRut.Text == null))
                //    _emp.empresa.rut.rut = Convert.ToInt32(txtRut.Text);
                //if (!((txtRut.Text.Contains(" ")) || (txtNombre.Text.Equals("")) || txtNombre.Text == null))
                //    _emp.empresa.nombre = txtNombre.Text;
                //if (!((txtFono.Text.Contains(" ")) || (txtFono.Text.Equals("")) || txtFono.Text == null))
                //    _emp.empresa.telefono = txtFono.Text;
                //if (!((txtDireccion.Text.Contains(" ")) || (txtDireccion.Text.Equals("")) || txtDireccion.Text == null))
                //    _emp.empresa.direccion = txtDireccion.Text;
                //if (!((txtRubro.Text.Contains(" ")) || (txtRubro.Text.Equals("")) || txtRubro.Text == null))
                //    _emp.empresa.rubroEmpresa = txtRubro.Text;
                //if (!(cmbComuna.SelectedIndex == 0  || (cmbComuna.SelectedIndex.ToString() == null)))
                //    _emp.empresa.comuna.idComuna = Convert.ToInt32(cmbComuna.SelectedItem);
                //FormEmpresaFactory.GuardarEmpresa(_emp);
             }
            catch (Exception)
            {
                string error = (bool)FormEmpresaFactory.GuardarEmpresa(_emp) ? "Instalación de Empresa guardada correctamente." : "Hubo un problema al registrar la Instalación, favor intente nuevamente más tarde.";
                MessageBox.Show(error);
            }

        }

        private void cmbComuna_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            try
            {
                int var = Convert.ToInt32(cmbRegion.SelectedIndex.ToString());
                cargarComunas = new BindableCollection<Comuna>(ComunaFactory.listadoComunas(var));
                foreach (var item in cargarComunas)
                {
                    cmbComuna.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se han detectado problemas al cargar las comunas. Intente nuevamente." + ex, "Error",MessageBoxButton.OK,MessageBoxImage.Error);
            //    this.Close();
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            EmpresaView _modal = new EmpresaView();
            _modal.ShowDialog();
            this.Close();
        }

        //public void cargarTrabajadoresEmpresas(string var)
        //{
        //    // var = txtRutIncompleto.Text;
        //    String varLocal = var;
        //    FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
        //    listadoTrabajadores = new BindableCollection<Trabajador>(FormServiciosGeneralesFactory.obtenerTrabajadoresEmpresa(Convert.ToInt64(var)));
        //    foreach (var item in listadoTrabajadores)

        //    {
        //        grdTrabajadores.ItemsSource = null;
        //        grdTrabajadores.ItemsSource = listadoTrabajadores;
        //    }
        //}
    }
}
