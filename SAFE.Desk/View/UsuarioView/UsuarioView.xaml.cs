﻿namespace SAFE.Desk
{
    using Caliburn.Micro;
    using MahApps.Metro.Controls;
    using MahApps.Metro.Controls.Dialogs;
    using SAFE.Domain.Entities;
    using SAFE.Domain.Forms;
    using System;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Lógica de interacción para UsuarioCollectionView.xaml
    /// </summary>
    public partial class UsuarioView : MetroWindow
    {
        private BindableCollection<Usuario> listadoUsuarios;

        public UsuarioView()
        {
            InitializeComponent();
            cargarUsuarios();
        }

        private void btnCancelar(object sender, System.Windows.RoutedEventArgs e)
        {
           
        }

        private async Task CancelarTransaccionAsync()
        {


            var result = await this.ShowMessageAsync("Cancelar Transacción", "Esta seguro de cancelar este proceso?", MessageDialogStyle.AffirmativeAndNegative);
            if (result == MessageDialogResult.Affirmative)
            {
                limpiarCampos();
            }
            else
            {
                this.Close();
            }
            MessageBox.Show("Esta cancelando la transacción. Esta seguro", "Cancelar Transacción",MessageBoxButton.OKCancel, MessageBoxImage.Warning);
        }

        //private async Task GuardarTransaccionAsync()
        //{
        //    FormUsuario _emp = new FormUsuario();
        //    Enum result;
        //    try
        //    {
        //        _emp.usuario = new Usuario();
        //        if ((!(
        //            string.IsNullOrEmpty(txtUsuario.Text) ||
        //            string.IsNullOrEmpty(txtPassword.ToString()) ||
        //            string.IsNullOrEmpty(txtPasswordRep.ToString()))) &&
        //            (txtPassword.ToString().Equals(txtPasswordRep.ToString())))

        //        {
        //                _emp.usuario.rut.rut = Convert.ToInt32(txtUsuario.Text);
        //                _emp.usuario.password = txtPasswordRep.ToString();
        //                _emp.usuario.estado = chkEstado.ToString();
        //                _emp.usuario.tipoUsuario = Convert.ToInt32(cmbTipoPerfil.SelectedItem);

        //                result = await this.ShowMessageAsync("Guardar Transacción", "Esta seguro de que desea guardar el nuevo registro?", MessageDialogStyle.AffirmativeAndNegative);
        //                if (result.Equals(MessageDialogResult.Affirmative))
        //                {
        //                  //  FormUsuario.GuardarUsuario(_emp);
        //                    limpiarCampos();
        //                    this.Close();
        //                }
        //                else
        //                {
        //                    await this.ShowMessageAsync("Guardar Transacción", "La inserción del registro en la Tabla USUARIO, ha sido cancelada.");
        //                    txtUsuario.Focus();
        //                    this.Close();
        //                }
        //        }
        //        else
        //        {
        //            MessageBox.Show("Error", "Debe completar todos los campos. Intente nuevamente.", MessageBoxButton.OK, MessageBoxImage.Error);
        //            txtUsuario.Focus();
        //            this.Close();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        await this.ShowMessageAsync("Control Excepciones", "Se ha producido una Excepción no controlada. " + ex.Message, MessageDialogStyle.Affirmative);
        //        this.Close();
        //        //MessageBox.Show(ex.Message);
        //    }


        //}

        private void limpiarCampos()
        {
            txtApeMaterno.Text = string.Empty;
            txtApePaterno.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtPassword.Password = string.Empty;
            txtPasswordRep.Password = string.Empty;
            txtRut.Text = string.Empty;
            txtUsuario.Text = string.Empty;
            txtRut.Focus();
        }

        private void cargarUsuarios()
        {
            FormUsuario da = new FormUsuario();
            listadoUsuarios = new BindableCollection<Usuario>(FormServiciosGeneralesFactory.obtenerListaUsuarios());
            foreach (var item in listadoUsuarios)
            {
                grdUsuarios.ItemsSource = null;
                grdUsuarios.ItemsSource = listadoUsuarios;
            }
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            UsuarioCollectionView _modal = new UsuarioCollectionView
            {
                Owner = this
            };
            this.Close();
            _modal.ShowDialog();
        }

    
    }
}
