﻿namespace SAFE.Desk
{
    using System.Windows;
    using Caliburn.Micro;
    using MahApps.Metro.Controls;
    using SAFE.Domain.Entities;
    using SAFE.Domain.Forms;


    /// <summary>
    /// Interaction logic for EmpresaView.xaml
    /// </summary>
    public partial class UsuarioCollectionView : MetroWindow
    {
        public UsuarioCollectionView()
        {
            InitializeComponent();
            cargarUsuarios();
        }

        private BindableCollection<Usuario> listadoUsuarios;
   

        private void cargarUsuarios()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoUsuarios = new BindableCollection<Usuario>(FormServiciosGeneralesFactory.obtenerListaUsuarios());
            foreach (var item in listadoUsuarios)
            {
                grdUsuarios.ItemsSource = null;
                grdUsuarios.ItemsSource = listadoUsuarios;
            }
        }
      

        private void grdEmpresas_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }

        private void btn_verEvaluaciones(object sender, System.Windows.RoutedEventArgs e)
        {

        }

        private void btn_VerTrabajadores(object sender, System.Windows.RoutedEventArgs e)
        {
       }

       
        private void btn_newEmpresa(object sender, System.Windows.RoutedEventArgs e)
        {
          }

        private void btnEliminar(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            UsuarioView _modal = new UsuarioView
            {
                Owner = this
            };
            _modal.ShowDialog();
        }
    }
}
