﻿using Caliburn.Micro;
using MahApps.Metro.Controls;
using SAFE.Domain.Entities;
using SAFE.Domain.Forms;
using System;
using System.Windows;

namespace SAFE.Desk
{
    /// <summary>
    /// Interaction logic for InstalacionesView.xaml
    /// </summary>
    public partial class InstalacionesView : MetroWindow
    {
        private BindableCollection<Comuna> cargarComunas;
        private BindableCollection<Instalacion> listadoInstalaciones;
        public Comuna comuna;
        public Empresa empresa;

        public InstalacionesView()
        {
            InitializeComponent();
            cargaRegion();
        }

        public BindableCollection<Region> CargarRegiones { get; private set; }

        public void cargaRegion()
        {
            CargarRegiones = new BindableCollection<Region>(FormServiciosGeneralesFactory.obtenerRegiones());
            foreach (var item in CargarRegiones)
            {
                cmbRegion.Items.Add(item);
            }
        }

        public void cargarInstalacionEmpresas(string var)
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoInstalaciones = new BindableCollection<Instalacion>(FormServiciosGeneralesFactory.obtenerInstalacionesEmpresa(Convert.ToInt32(var)));
            foreach (var item in listadoInstalaciones)
                
            {
                grdInstalaciones.ItemsSource = null;
                grdInstalaciones.ItemsSource = listadoInstalaciones;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            //FormEmpresa _emp = new FormEmpresa();
            //try
            //{
       
            // }
            //catch (Exception)
            //{
            //    string error = (bool)FormEmpresaFactory.GuardarEmpresa(_emp) ? "Instalación de Empresa guardada correctamente." : "Hubo un problema al registrar la Instalación, favor intente nuevamente más tarde.";
            //    MessageBox.Show(error);
            //}

        }

        private void cmbComuna_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            try
            {
                int var = Convert.ToInt32(cmbRegion.SelectedIndex.ToString());
                cargarComunas = new BindableCollection<Comuna>(ComunaFactory.listadoComunas(var));
                foreach (var item in cargarComunas)
                {
                    cmbComuna.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se han detectado problemas al cargar las comunas. Intente nuevamente." + ex, "Error");
            }
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            txtDireccion.Text = string.Empty;
            txtFono.Text = string.Empty;
     
        }

        private void cmbRegion_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            cargaRegion();
        }

        private void cmbRegion_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int var = Convert.ToInt32(cmbRegion.SelectedIndex) + 1;

            cargarComunas = new BindableCollection<Comuna>(ComunaFactory.listadoComunas(var));
            foreach (var item in cargarComunas)
            {
                cmbComuna.Items.Add(item);
            }
        }
    }
}
