﻿using System.Windows;
using MahApps.Metro.Controls;


namespace SAFE.Desk
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : MetroWindow
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            UsuarioView frm = new UsuarioView();
            frm.Show();
        }
    }
}
