﻿using System;
using System.Windows;
using MahApps.Metro.Controls;

namespace SAFE.Desk
{
    /// <summary>
    /// Interaction logic for UsuariosPersonasView.xaml
    /// </summary>
    public partial class UsuariosPersonasView : MetroWindow
    {
        public UsuariosPersonasView()
        {
            InitializeComponent();
        }

        private void Tile_Click(object sender, RoutedEventArgs e)
        {
            //flyprueba.IsOpen = true;
        }

        private void btnControl(object sender, RoutedEventArgs e)
        {
            UsuarioCollectionView _modal = new UsuarioCollectionView();
            this.Close();
            _modal.ShowDialog();
        }

        private void btnRegistro(object sender, RoutedEventArgs e)
        {
            PersonaCollectionView _modal = new PersonaCollectionView();
            this.Close();
            _modal.ShowDialog();
        }
    }
}
