﻿namespace SAFE.Desk
{
    using Caliburn.Micro;
    using MahApps.Metro.Controls;
    using SAFE.Domain.Entities;
    using SAFE.Domain.Forms;
    using System;
    using System.Windows;

    /// <summary>
    /// Lógica de interacción para EmpresaInfoView.xaml
    /// </summary>
    public partial class EmpresaInfoView : MetroWindow
    {
        private BindableCollection<Instalacion> listadoInstalaciones;
        private BindableCollection<Evaluacion> listadoEvaluciones;
        private BindableCollection<Persona> listadoTrabajador;

        public EmpresaInfoView()
        {
            InitializeComponent();
        }

        public void cargarInstalacionEmpresas(string var)
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoInstalaciones = new BindableCollection<Instalacion>(FormServiciosGeneralesFactory.obtenerInstalacionesEmpresa(Convert.ToInt32(var)));
            foreach (var item in listadoInstalaciones)
            {
                grdInstalaciones.ItemsSource = null;
                grdInstalaciones.ItemsSource = listadoInstalaciones;
            }
        }

        public void cargarEvaluacionesEmpresas(string var)
        {
            FormEvaluacionFactory da = new FormEvaluacionFactory();
            listadoEvaluciones = new BindableCollection<Evaluacion>(FormEvaluacionFactory.EvaluacionesEmpresa(Convert.ToInt32(var)).listaEvaluaciones);
            {
                grdEvaluaciones.ItemsSource = null;
                grdEvaluaciones.ItemsSource = listadoEvaluciones;
            }
        }

        public void cargarTrabajadoresEmpresas(string var)
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoTrabajador = new BindableCollection<Persona>(FormServiciosGeneralesFactory.obtenerTrabajadoresEmpresa(Convert.ToInt32(var)));
            {
                grdTrabajadores.ItemsSource = null;
                grdTrabajadores.ItemsSource = listadoTrabajador;
            }
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            EmpresaView _modal = new EmpresaView
            {
                Owner = this
            };
            
            _modal.ShowDialog();
            
        }
    }
}
