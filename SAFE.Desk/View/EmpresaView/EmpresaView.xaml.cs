﻿namespace SAFE.Desk
{
    using Caliburn.Micro;
    using MahApps.Metro.Controls;
    using SAFE.Domain.Entities;
    using SAFE.Domain.Forms;
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    /// <summary>
    /// Interaction logic for EmpresaView.xaml
    /// </summary>
    public partial class EmpresaView : MetroWindow
    {
        private BindableCollection<Comuna> cargarComunas;
     
        public EmpresaView()
        {
            InitializeComponent();
            cargaRegion();
        }

        public BindableCollection<Region> CargarRegiones { get; private set; }

        public void cargaRegion()
        {
            CargarRegiones = new BindableCollection<Region>(FormServiciosGeneralesFactory.obtenerRegiones());
            foreach (var item in CargarRegiones)
            {
                cmbRegion.Items.Add(item);
            }
        }

        //private async Task btnGuardar_ClickAsync(object sender, RoutedEventArgs e)
        //{
        //    FormEmpresa _emp = new FormEmpresa();
        //    try
        //    {
        //        _emp.empresa = new Empresa()
        //        {
        //            rut = new Rut(),
        //            comuna = new Comuna()
        //        };

        //        if (!(
        //            string.IsNullOrEmpty(txtRut.Text) ||
        //            string.IsNullOrEmpty(txtNombre.Text) ||
        //            string.IsNullOrEmpty(txtFono.Text) ||
        //            string.IsNullOrEmpty(txtDireccion.Text) ||
        //            string.IsNullOrEmpty(txtRubro.Text) ||
        //            string.IsNullOrEmpty(cmbComuna.SelectedItem?.ToString())))
        //        {
        //            _emp.empresa.rut.rut = Convert.ToInt32(txtRut.Text);
        //            _emp.empresa.nombre = txtNombre.Text;
        //            _emp.empresa.telefono = txtFono.Text;
        //            _emp.empresa.direccion = txtDireccion.Text;
        //            _emp.empresa.rubroEmpresa = txtRubro.Text;
        //            _emp.empresa.comuna.idComuna = Convert.ToInt32(((Comuna)cmbComuna.SelectedItem).idComuna);

        //            FormEmpresaFactory.GuardarEmpresa(_emp);
        //            await mensajeAsync("Exito", "La empresa " + _emp.empresa.nombre + " ha sido registrada y/o actualizada con exito.");
        //            //cargarEmpresas();
        //            this.Close();
        //        }
        //        else
        //        {
        //            MessageBox.Show("Se ha producido un error. \nPor favor de completar todos los campos del formulario.","Error",MessageBoxButton.OK, MessageBoxImage.Error);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Se ha presentado un error\n" + ex.Message, "Error");
        //    }

        //}

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            txtDireccion.Text = string.Empty;
            txtFono.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtRubro.Text = string.Empty;
            txtRut.Text = string.Empty;
        }

        private void cmbRegion_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int var = Convert.ToInt32(cmbRegion.SelectedIndex) + 1;
            
            cargarComunas = new BindableCollection<Comuna>(ComunaFactory.listadoComunas(var));
            foreach (var item in cargarComunas)
            {
                cmbComuna.Items.Add(item);
            }
        }
        private async Task mensajeAsync(string titulo, string mensaje)
        {
            await this.ShowMessageAsync(titulo, mensaje);

        }

        private Task ShowMessageAsync(string titulo, object ensaje)
        {
            throw new NotImplementedException();
        }

        //private void btnCancelar_Click_1(object sender, RoutedEventArgs e)
        //{

        //}

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            FormEmpresa _emp = new FormEmpresa();
            try
            {
                _emp.empresa = new Empresa()
                {
                    rut = new Rut(),
                    comuna = new Comuna()
                };

                if (!(
                    string.IsNullOrEmpty(txtRut.Text) ||
                    string.IsNullOrEmpty(txtNombre.Text) ||
                    string.IsNullOrEmpty(txtFono.Text) ||
                    string.IsNullOrEmpty(txtDireccion.Text) ||
                    string.IsNullOrEmpty(txtRubro.Text) ||
                    string.IsNullOrEmpty(txtEmail.Text) ||
                    string.IsNullOrEmpty(cmbComuna.SelectedItem?.ToString())))
                {

                    if (txtRut.Text.Contains("."))
                    {
                        string sRut = txtRut.Text;
                        sRut = sRut.Replace(".", "");
                        sRut = sRut.Replace("-", "");
                        sRut = sRut.Remove(sRut.Length - 1);
                        _emp.empresa.rut.rut = Convert.ToInt32(sRut);
                    }
                    else
                    {
                        _emp.empresa.rut.rut = Convert.ToInt32(txtRut.Text);
                    }
                  
                    _emp.empresa.nombre = txtNombre.Text;
                    _emp.empresa.telefono = txtFono.Text;
                    _emp.empresa.email = txtEmail.Text;
                    _emp.empresa.direccion = txtDireccion.Text;
                    _emp.empresa.rubroEmpresa = txtRubro.Text;
                    _emp.empresa.comuna.idComuna = Convert.ToInt32(((Comuna)cmbComuna.SelectedItem).idComuna);
                    if (txtIdEmpresa.Text == null || txtIdEmpresa.Text.ToString() == string.Empty)
                    {
                        FormEmpresaFactory.GuardarEmpresa(_emp);
                        MessageBox.Show("Excelente noticia.!\n La empresa " + _emp.empresa.nombre + " ha sido registrada con exito.", "GENIAL", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    }
                    else
                    {
                        _emp.empresa.idEmpresa = Convert.ToInt32(txtIdEmpresa.Text);
                        FormEmpresaFactory.GuardarEmpresa(_emp);
                        MessageBox.Show("Excelente\n La empresa " + _emp.empresa.nombre + " ha sido actualizada con exito.", "Exito", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Close();
                    }
                    
                }
                else
                {
                    MessageBox.Show("Se ha producido un error. \nPor favor de completar todos los campos del formulario.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error inesperado.\nERROR: " + ex.Message, "Error", MessageBoxButton.OK,MessageBoxImage.Error);
            }

        }
    }
}
