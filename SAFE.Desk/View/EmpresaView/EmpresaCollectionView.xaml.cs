﻿namespace SAFE.Desk
{
    using System.Windows;
    using Caliburn.Micro;
    using MahApps.Metro.Controls;
    using SAFE.Domain.Entities;
    using SAFE.Domain.Forms;


    /// <summary>
    /// Interaction logic for EmpresaView.xaml
    /// </summary>
    public partial class EmpresaCollectionView : MetroWindow
    {
        public EmpresaCollectionView()
        {
            InitializeComponent();
            cargarEmpresas();
        }

        private BindableCollection<Empresa> listadoEmpresas;

        public BindableCollection<Region> CargarRegiones { get; private set; }

        private void cargarEmpresas()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoEmpresas = new BindableCollection<Empresa>(FormServiciosGeneralesFactory.obtenerEmpresas());
             foreach (var item in listadoEmpresas)
            {
                grdEmpresas.ItemsSource = null;
                grdEmpresas.ItemsSource = listadoEmpresas;
            }
        }

    
        private void SetEmpresaInstalaciones(Empresa objEmpresa)
        {
            EmpresaInfoView _modal = new EmpresaInfoView();
            Region _reg = new Region();
            _modal.txtNombre.Text = objEmpresa.nombre;
            _modal.Title= "SAFE Desk | Detalle Empresa : " + objEmpresa.nombre;
            _modal.txtRut.Text = objEmpresa.rut.rutCompleto;
            _modal.txtDireccion.Text = objEmpresa.direccion;
            _modal.txtFono.Text = objEmpresa.telefono;
            _modal.txtComuna.Text = objEmpresa.comuna.comuna;
            _modal.txtRegion.Text = _reg.region;
            _modal.cargarInstalacionEmpresas(objEmpresa.rut.rut.ToString());
            _modal.cargarEvaluacionesEmpresas(objEmpresa.rut.rut.ToString());
            _modal.cargarTrabajadoresEmpresas(objEmpresa.rut.rut.ToString());
            _modal.Owner = this;
            _modal.ShowDialog();
        }

        private void SetEmpresaAEditar(Empresa objEmpresa)
        {
            EmpresaView _modal = new EmpresaView();
            Region _reg = new Region();
            
            _modal.txtRut.IsEnabled = false;
            _modal.Title = "SAFE Desk | Editar Empresa : " + objEmpresa.nombre;
            _modal.txtIdEmpresa.Text = objEmpresa.idEmpresa.ToString();
            _modal.txtNombre.Text = objEmpresa.nombre;
            _modal.txtRut.Text = objEmpresa.rut.rutCompleto;
            _modal.txtRubro.Text = objEmpresa.rubroEmpresa;
            _modal.txtDireccion.Text = objEmpresa.direccion;
            _modal.txtEmail.Text = objEmpresa.email;
            _modal.txtFono.Text = objEmpresa.telefono;
            _modal.cmbComuna.SelectedValue = objEmpresa.comuna.comuna;
            _modal.cmbRegion.SelectedValue = objEmpresa.comuna.idRegion;
            _modal.Owner = this;
            _modal.ShowDialog();
            Close();
        }
        //private void SetEmpresaTrabajador(Empresa objEmpresa)
        //{
        //    TrabajadoresView _eva = new TrabajadoresView();
        //    _eva.txtEmpresa.Text = objEmpresa.nombre;
        //    _eva.txtRutEmpresa.Text = objEmpresa.rut.rutCompleto;
        //    _eva.txtDireccion.Text = objEmpresa.direccion;
        //    _eva.txtFono.Text = objEmpresa.telefono;
        //    _eva.cmbComuna.SelectedIndex = objEmpresa.comuna.idComuna;
        //    _eva.cmbRegion.SelectedIndex = objEmpresa.comuna.idRegion;
        //    //_eva.cargarTrabajadoresEmpresas(objEmpresa.rut.rut.ToString());
        //    _eva.Owner = this;
        //    _eva.ShowDialog();
        //}
        //private void SetEmpresaEvaluaciones(Empresa objEmpresa)
        //{
        //    EvaluacionesView _eva = new EvaluacionesView();
        //    _eva.txtEmpresa.Text = objEmpresa.nombre;
        //    _eva.txtRutEmpresa.Text = objEmpresa.rut.rutCompleto;
        //    _eva.txtDireccion.Text = objEmpresa.direccion;
        //    _eva.txtFono.Text = objEmpresa.telefono;
        //    _eva.cmbComuna.SelectedIndex = objEmpresa.comuna.idComuna;
        //    _eva.cmbRegion.SelectedIndex = objEmpresa.comuna.idRegion;
        //    //   _modal.txtRutIncompleto.Text = objEmpresa.rut.rut.ToString();
        //    _eva.cargarEvaluacionesEmpresas(objEmpresa.rut.rut.ToString());
        //    _eva.Owner = this;
        //    _eva.ShowDialog();
        //}
        private void grdEmpresas_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            grdAccesoDirecto.IsEnabled = true;
        }

        private void btn_verEvaluaciones(object sender, System.Windows.RoutedEventArgs e)
        {
            //if (grdEmpresas.SelectedIndex != 1)
            //{
            //    Empresa objEmpresaSeleccionada = this.grdEmpresas.SelectedItem as Empresa;
            //    SetEmpresaEvaluaciones(objEmpresaSeleccionada);
            //}
            //else
            //{
            //    if (grdEmpresas.SelectedIndex == 1)
            //    {
            //        MessageBox.Show("ERROR: No ha seleccionado una Empresa valida.", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    }
            //}
        }

        private void btn_VerTrabajadores(object sender, System.Windows.RoutedEventArgs e)
        {
         }

        private void btn_verInstalaciones(object sender, System.Windows.RoutedEventArgs e)
        {
            if (grdEmpresas.SelectedIndex != 1)
            {
                Empresa objEmpresaSeleccionada = this.grdEmpresas.SelectedItem as Empresa;
                SetEmpresaInstalaciones(objEmpresaSeleccionada);
            }
            else
            {
                if (grdEmpresas.SelectedIndex == 1)
                {
                    MessageBox.Show("Alerta: No ha seleccionado una Empresa valida.", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btn_newEmpresa(object sender, System.Windows.RoutedEventArgs e)
        {
            EmpresaView _modal = new EmpresaView
            {
                Owner = this
            };
            _modal.ShowDialog();
        }

        private void btnEliminar(object sender, RoutedEventArgs e)
        {

        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (grdEmpresas.SelectedIndex != 1)
            {
                Empresa objEmpresaSeleccionada = this.grdEmpresas.SelectedItem as Empresa;
                SetEmpresaAEditar(objEmpresaSeleccionada);
            }
            else
            {
                if (grdEmpresas.SelectedIndex == 1)
                {
                    MessageBox.Show("No ha seleccionado una Empresa valida para actualizar sus registros.", "Alerta", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            
        }


        //private void SetEmpresaInstalaciones(Empresa objEmpresa)
        //{
        //    InstalacionesView _modal = new InstalacionesView();
        //    _modal.txtEmpresa.Text = objEmpresa.nombre;
        //    _modal.txtRut.Text = objEmpresa.rut.rutCompleto;
        //    _modal.txtDireccion.Text = objEmpresa.direccion;
        //    _modal.txtFono.Text = objEmpresa.telefono;
        //    _modal.cmbComuna.SelectedIndex = objEmpresa.comuna.idComuna;
        //    _modal.cmbRegion.SelectedIndex = objEmpresa.comuna.idRegion;
        // //   _modal.txtRutIncompleto.Text = objEmpresa.rut.rut.ToString();
        //    _modal.cargarInstalacionEmpresas(objEmpresa.rut.rut.ToString());
        //    _modal.Owner = this;
        //    _modal.ShowDialog();
        //}

    }
}
