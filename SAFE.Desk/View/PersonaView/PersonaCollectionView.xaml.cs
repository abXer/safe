﻿using System;
using System.Windows;
using Caliburn.Micro;
using MahApps.Metro.Controls;
using SAFE.Domain.Entities;
using SAFE.Domain.Forms;

namespace SAFE.Desk
{
    /// <summary>
    /// Interaction logic for UsuarioCollectionView.xaml
    /// </summary>
    public partial class PersonaCollectionView : MetroWindow
    {
        private BindableCollection<Tecnico> listadoTecnicos;
        private BindableCollection<Expositor> listadoExpositor;
        private BindableCollection<Supervisor> listadoSupervisor;
        private BindableCollection<Administrador> listadoAdmins;
        private BindableCollection<Ingeniero> listadoIngeniero;

        //private BindableCollection<Ingeniero> listadoIngeniero;

        public PersonaCollectionView()
        {
            InitializeComponent();
        }

        private void cargarAdministradores()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoAdmins = new BindableCollection<Administrador>(FormServiciosGeneralesFactory.obtenerAdministradores());
            foreach (var item in listadoAdmins)
            {
                grdTrabajadores.ItemsSource = null;
                grdTrabajadores.ItemsSource = listadoAdmins;
            }
        }
        private void cargarTecnicos()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoTecnicos = new BindableCollection<Tecnico>(FormServiciosGeneralesFactory.obtenerTecnicos());
            foreach (var item in listadoTecnicos)
            {
                grdTrabajadores.ItemsSource = null;
                grdTrabajadores.ItemsSource = listadoTecnicos;
            }
        }

        private void cargarExpositor()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoExpositor = new BindableCollection<Expositor>(FormServiciosGeneralesFactory.obtenerExpositores());
            foreach (var item in listadoExpositor)
            {
                grdTrabajadores.ItemsSource = null;
                grdTrabajadores.ItemsSource = listadoExpositor;
            }
        }

        private void cargarSupervisor()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoSupervisor = new BindableCollection<Supervisor>(FormServiciosGeneralesFactory.obtenerSupervisores());
            foreach (var item in listadoSupervisor)
            {
                grdTrabajadores.ItemsSource = null;
                grdTrabajadores.ItemsSource = listadoSupervisor;
            }
        }

        private void cargarIngeniero()
        {
            FormServiciosGeneralesFactory da = new FormServiciosGeneralesFactory();
            listadoIngeniero = new BindableCollection<Ingeniero>(FormServiciosGeneralesFactory.ListaIngenieros());
            foreach (var item in listadoIngeniero)
            {
                grdTrabajadores.ItemsSource = null;
                grdTrabajadores.ItemsSource = listadoIngeniero;
            }
        }

         private void btnAll_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnExpo_Click(object sender, RoutedEventArgs e)
        {
            cargarExpositor();
        }

        private void btnSupervisor_Click(object sender, RoutedEventArgs e)
        {
            cargarSupervisor();
        }

        private void btnTecnico_Click(object sender, RoutedEventArgs e)
        {
            cargarTecnicos();
        }

        private void btnIngenierio_Click(object sender, RoutedEventArgs e)
        {
            cargarIngeniero();
        }

        private void btnAdmin_Click(object sender, RoutedEventArgs e)
        {
            cargarAdministradores();
        }

        private void btnBajas_Click(object sender, RoutedEventArgs e)
        {
            cargarTecnicos();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            PersonaView _modal = new PersonaView
            {
                Owner = this
            };
            _modal.ShowDialog();
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            DashboardView _modal = new DashboardView();
            this.Close();
            _modal.ShowDialog();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            PersonaView _modal = new PersonaView
            {
                Owner = this
            };
            _modal.ShowDialog();
        }
    }
}
