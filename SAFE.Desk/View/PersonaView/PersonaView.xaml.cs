﻿using Caliburn.Micro;
using MahApps.Metro.Controls;
using SAFE.Domain.Entities;
using SAFE.Domain.Forms;
using System;
using System.Collections.Generic;
using System.Windows;

namespace SAFE.Desk
{
    /// <summary>
    /// Lógica de interacción para UsuarioCollectionView.xaml
    /// </summary>
    public partial class PersonaView : MetroWindow
    {
        private BindableCollection<Comuna> cargarComunas;

        public PersonaView()
        {
            InitializeComponent();
            cargaRegion();
            cargaEmpresas();
            cargaTipoPerfiles();
        }

        public BindableCollection<Region> CargarRegiones { get; private set; }
        public IEnumerable<object> CargarEmpresas { get; private set; }
        public BindableCollection<Perfil> CargarPerfiles { get; private set; }

        public void cargaRegion()
        {
            CargarRegiones = new BindableCollection<Region>(FormServiciosGeneralesFactory.obtenerRegiones());
            foreach (var item in CargarRegiones)
            {
                cmbRegion.Items.Add(item);
            }
        }

        public void cargaEmpresas()
        {
            CargarEmpresas = new BindableCollection<Empresa>(FormServiciosGeneralesFactory.obtenerEmpresas());
            foreach (var item in CargarEmpresas)
            {
                cmbEmpresa.Items.Add(item);
            }
        }

        public void cargaTipoPerfiles()
        {
            CargarPerfiles = new BindableCollection<Perfil>(FormServiciosGeneralesFactory.listaPerfiles());
            foreach (var item in CargarPerfiles)
            {
                cmbPerfil.Items.Add(item.descripcion);
            }
        }

        private void cmbRegion_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int var = Convert.ToInt32(cmbRegion.SelectedIndex) + 1;

            cargarComunas = new BindableCollection<Comuna>(ComunaFactory.listadoComunas(var));
            foreach (var item in cargarComunas)
            {
                cmbComuna.Items.Add(item);
            }
        }

        private void cmbEmpresa_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
           
        }

        private void cmbRegion_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            
        }

        private void cmbEmpresa_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
           
        }

        private void txtRut_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            txtCtaUsuario.Text = txtRut.Text;
        }

        private void btnGuardar_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Persona _emp = new Persona();
            try
            {
                _emp = new Persona()
                {
                    rut = new Rut(),
                    contacto = new Contacto()
                };

                if (!(
                    string.IsNullOrEmpty(txtRut.Text) ||
                    string.IsNullOrEmpty(txtNombre.Text) ||
                    string.IsNullOrEmpty(txtApePaterno.Text) ||
                    string.IsNullOrEmpty(txtApeMaterno.Text) ||
                    string.IsNullOrEmpty(txtDireccion.Text) ||
                    string.IsNullOrEmpty(txtCelular.Text) ||
                    string.IsNullOrEmpty(txtEmail.Text) ||
                    string.IsNullOrEmpty(dtpFechaNacimiento.Text) ||
                    string.IsNullOrEmpty(dtpFechaIngreso.Text) ||
                    string.IsNullOrEmpty(cmbEmpresa.SelectedItem?.ToString()) ||
                    string.IsNullOrEmpty(cmbRegion.SelectedItem?.ToString()) ||
                    string.IsNullOrEmpty(cmbPerfil.SelectedItem?.ToString()) ||
                    string.IsNullOrEmpty(cmbComuna.SelectedItem?.ToString())))

                {

                    if (txtRut.Text.Contains("."))
                    {
                        string sRut = txtRut.Text;
                        sRut = sRut.Replace(".", "");
                        sRut = sRut.Replace("-", "");
                        sRut = sRut.Remove(sRut.Length - 1);
                        _emp.rut.rut = Convert.ToInt32(sRut);
                    }
                    else
                    {
                        _emp.rut.rut = Convert.ToInt32(txtRut.Text);
                    }

                    _emp.nombre = txtNombre.Text;
                    _emp.apellidoPaterno = txtApePaterno.Text;
                    _emp.apellidoMaterno = txtApeMaterno.Text;
                    _emp.direccion = txtDireccion.Text;
                    _emp.contacto.celular = txtCelular.Text;
                    _emp.contacto.email = txtEmail.Text;
                    _emp.fechaNacimiento = dtpFechaNacimiento.Text;
                    _emp.direccion = txtDireccion.Text;

                    //_emp.empresa.comuna.idComuna = Convert.ToInt32(((Comuna)cmbComuna.SelectedItem).idComuna);
                    int intPerfil =cmbPerfil.SelectedIndex;
                    if (txtIdPersona.Text == null || txtIdPersona.Text.ToString() == string.Empty)
                    {
                        FormServiciosGeneralesFactory.GuardarPersona(_emp, intPerfil );
                        MessageBox.Show("Excelente noticia.!\n El usuario " + _emp.nombre + " "+ _emp.apellidoPaterno + " "+ _emp.apellidoMaterno + " ha sido registrado con exito.", "INSERTAR PERSONA", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    }
                    else
                    {
                        //_emp.idresa = Convert.ToInt32(txtIdPersona.Text);
                        FormServiciosGeneralesFactory.GuardarPersona(_emp, intPerfil);
                        MessageBox.Show("Excelente\n El usuario  " + _emp.nombre + " " + _emp.apellidoPaterno + " " + _emp.apellidoMaterno + " ha sido actualizado con exito.", "ACTUALIZA PERSONA", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Close();
                    }

                }
                else
                {
                    MessageBox.Show("Se ha producido un error. \nPor favor de completar todos los campos del formulario.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error inesperado.\nERROR: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
