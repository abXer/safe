﻿namespace SAFE.Desk
{
    using MahApps.Metro.Controls;

    /// <summary>
    /// Lógica de interacción para DashboardView.xaml
    /// </summary>
    public partial class DashboardView : MetroWindow
    {
    
        public DashboardView()
        {
            InitializeComponent();
        }

        private void menuUsuarios_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            UsuariosPersonasView _modal = new UsuariosPersonasView
            {
                Owner = this
            };
            _modal.ShowDialog();

        }

        private void menuEvaluciones_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            EvaluacionesView _modal = new EvaluacionesView
            {
                Owner = this
            };
            _modal.ShowDialog();
        }

        private void menuCapacitacion_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //CapacitacionesView _modal = new CapacitacionesView();
            //_modal.Owner = this;
            //_modal.ShowDialog();
        }

        private void menuEmpresa_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            EmpresaCollectionView _modal = new EmpresaCollectionView
            {
                Owner = this
            };
            _modal.ShowDialog();
        }

        private void menuInstalacion_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //InstalacionesCollectionView _modal = new InstalacionesCollectionView
            //{
            //    Owner = this
            //};
            //_modal.ShowDialog();
        }

        private void btnMedicos_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DoctoresView _modal = new DoctoresView
            {
                Owner = this
            };
            _modal.ShowDialog();

        }
    }
}
