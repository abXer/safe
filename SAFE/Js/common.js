﻿$(window).on('beforeunload', function () {
    loading();
});

function loading() {
    $('#logoCarga').addClass('is-active');
}

function closeModal() {
    $('#logoCarga').removeClass('is-active');
}

function mdlMensaje(titulo, mensaje) {
    $('#mdlTitulo').text(titulo);
    $('#mdlContenido').text(mensaje);
    $('#mdlMensaje').modal({ backdrop: true });
    $('#mdlMensaje').show();
}

function actualizarComunas(idRegion, controlDestino) {
    if (idRegion !== "") {
        $.ajax({
            url: "/Common/Comunas",
            data: {
                "idRegion": idRegion
            },
            cache: false,
            type: "GET",
            success: function (data) {
                $(controlDestino).empty();
                $(controlDestino).append($('<option>').text('-- Seleccione --').attr('value', 0));

                $.each(data, function (index, comuna) {
                    $(controlDestino).append($('<option>').text(comuna.comuna).attr('value', comuna.idComuna));
                });
            },
            error: function (xhr) {

            }
        });
    } else {
        $(controlDestino).empty();
        $(controlDestino).append($('<option>').text('-- Seleccione una región --'));
    }
}

function agregarRevision() {

    var reco = $('#nuevaRevision').val();

    var TipoEvaluacion;
    var tipoEvaluacionPersona = $('#radioTipoEvaluacionPersonal').is(':checked');
    var tipoEvaluacionInstalacion = $('#radioTipoEvaluacionInstalaciones').is(':checked');

    if (tipoEvaluacionPersona && !tipoEvaluacionInstalacion) {
        TipoEvaluacion = 2;
    } else {
        TipoEvaluacion = 1;
    }


    var IdIngeniero = $('#idIngeniero').val();
    var IdSupervisor = $('#idSupervisor').val();
    var IdEvaluacion = $('#idEvaluacion').val();
    var idEmpresa = $('#idEmpresa').val();

    if (reco !== "") {
        $.ajax({
            url: "/Evaluacion/AgregarRevision",
            data: {
                "tipoEvaluacion": TipoEvaluacion,
                "idIngeniero": IdIngeniero,
                "idSupervisor": IdSupervisor,
                "recomendacion": reco,
                "idEvaluacion": IdEvaluacion,
                "idEmpresa": idEmpresa
            },
            cache: false,
            type: "POST",
            success: function (data) {

                if (data === true) {
                    mdlMensaje('Confirmación', 'Revisión guardada correctamente');
                    setTimeout(function () {
                        location.reload();
                    }, 3000);
                }
                else
                    mdlMensaje('Confirmación', 'No se guardó la revisión');
            },
            error: function (xhr) {

            }
        });
    } else {
        mdlMensaje('Error', 'Debe completar la recomendación');
    }

}

function agregarAsistenciaCapacitacion() {

    var idCapacitacion = $('#idCapacitacion').val();
    var rutTrabajador = $('#rutTrabajador').val();
    var rutEmpresa = $('#rutEmpresa').val();


    if (reco !== "") {

        $.ajax({
            url: "/Capacitacion/AgregarAsistencia",
            data: {
                "idCapacitacion": idCapacitacion,
                "rutTrabajador": rutTrabajador,
                "rutEmpresa": rutEmpresa
            },
            cache: false,
            type: "POST",
            success: function (data) {

                if (data === true)
                    mdlMensaje('Confirmación', 'Revisión guardada correctamente');
                else
                    mdlMensaje('Confirmación', 'No se guardó la revisión');
            },
            error: function (xhr) {

            }
        });
    } else {
        mdlMensaje('Error', 'Debe completar la recomendación');
    }

}

function agregarFichaMedica() {

    var idEmpresa = $("#idEmpresa").val();
    var diagnostico = $("#txtDiagnostico").val();
    var idTrabajador = $("#idTrabajador").val();
    var idAtencionMedica = $("#idAtencionMedica").val();

    console.log(idEmpresa);
    console.log(diagnostico);
    console.log(idTrabajador);
    console.log(idAtencionMedica);


    if (diagnostico !== "") {
        $.ajax({
            url: "/Salud/AgregarFichaMedica",
            data: {
                nuevaFichaMedica: {
                    idEmpresa: idEmpresa,
                    diagnostico: diagnostico,
                    idTrabajador: idTrabajador,
                    idAtencionMedica: idAtencionMedica
                }
            },
            cache: false,
            type: "POST",
            success: function (data) {

                if (data === true) {
                    mdlMensaje('Confirmación', 'diagnóstico guardado correctamente');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
                else
                    mdlMensaje('Confirmación', 'No se guardó la revisión');
            },
            error: function (xhr) {

            }
        });
    } else {
        mdlMensaje('Error', 'Debe completar el diagnóstico');
    }

}


function validaRut() {
    (function ($) {
        jQuery.fn.Rut = function (options) {
            var defaults = {
                digito_verificador: null,
                on_error: function () { },
                on_success: function () { },
                validation: true,
                format: true,
                format_on: 'change'
            };

            var opts = $.extend(defaults, options);

            return this.each(function () {

                if (defaults.format) {
                    jQuery(this).bind(defaults.format_on, function () {
                        jQuery(this).val(jQuery.Rut.formatear(jQuery(this).val(), defaults.digito_verificador == null));
                    });
                }
                if (defaults.validation) {
                    if (defaults.digito_verificador == null) {
                        jQuery(this).bind('blur', function () {
                            var rut = jQuery(this).val();
                            if (jQuery(this).val() != "" && !jQuery.Rut.validar(rut)) {
                                defaults.on_error();
                            }
                            else if (jQuery(this).val() != "") {
                                defaults.on_success();
                            }
                        });
                    }
                    else {
                        var id = jQuery(this).attr("id");
                        jQuery(defaults.digito_verificador).bind('blur', function () {
                            var rut = jQuery("#" + id).val() + "-" + jQuery(this).val();
                            if (jQuery(this).val() != "" && !jQuery.Rut.validar(rut)) {
                                defaults.on_error();
                            }
                            else if (jQuery(this).val() != "") {
                                defaults.on_success();
                            }
                        });
                    }
                }
            });
        }
    })(jQuery);

    /**
      Funciones
    */

    jQuery.Rut = {

        formatear: function (Rut, digitoVerificador) {
            var sRut = new String(Rut);
            var sRutFormateado = '';
            sRut = jQuery.Rut.quitarFormato(sRut);
            if (digitoVerificador) {
                var sDV = sRut.charAt(sRut.length - 1);
                sRut = sRut.substring(0, sRut.length - 1);
            }
            while (sRut.length > 3) {
                sRutFormateado = "." + sRut.substr(sRut.length - 3) + sRutFormateado;
                sRut = sRut.substring(0, sRut.length - 3);
            }
            sRutFormateado = sRut + sRutFormateado;
            if (sRutFormateado != "" && digitoVerificador) {
                sRutFormateado += "-" + sDV;
            }
            else if (digitoVerificador) {
                sRutFormateado += sDV;
            }

            return sRutFormateado;
        },

        quitarFormato: function (rut) {
            var strRut = new String(rut);
            while (strRut.indexOf(".") != -1) {
                strRut = strRut.replace(".", "");
            }
            while (strRut.indexOf("-") != -1) {
                strRut = strRut.replace("-", "");
            }

            return strRut;
        },

        digitoValido: function (dv) {
            if (dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4'
                && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9'
                && dv != 'k' && dv != 'K') {
                return false;
            }
            return true;
        },

        digitoCorrecto: function (crut) {
            largo = crut.length;
            if (largo < 2) {
                return false;
            }
            if (largo > 2) {
                rut = crut.substring(0, largo - 1);
            }
            else {
                rut = crut.charAt(0);
            }
            dv = crut.charAt(largo - 1);
            jQuery.Rut.digitoValido(dv);

            if (rut == null || dv == null) {
                return 0;
            }

            dvr = jQuery.Rut.getDigito(rut);

            if (dvr != dv.toLowerCase()) {
                return false;
            }
            return true;
        },

        getDigito: function (rut) {
            var dvr = '0';
            suma = 0;
            mul = 2;
            for (i = rut.length - 1; i >= 0; i--) {
                suma = suma + rut.charAt(i) * mul;
                if (mul == 7) {
                    mul = 2;
                }
                else {
                    mul++;
                }
            }
            res = suma % 11;
            if (res == 1) {
                return 'k';
            }
            else if (res == 0) {
                return '0';
            }
            else {
                return 11 - res;
            }
        },

        validar: function (texto) {
            texto = jQuery.Rut.quitarFormato(texto);
            largo = texto.length;

            // rut muy corto
            if (largo < 2) {
                return false;
            }

            // verifica que los numeros correspondan a los de rut
            for (i = 0; i < largo; i++) {
                // numero o letra que no corresponda a los del rut
                if (!jQuery.Rut.digitoValido(texto.charAt(i))) {
                    return false;
                }
            }

            var invertido = "";
            for (i = (largo - 1), j = 0; i >= 0; i-- , j++) {
                invertido = invertido + texto.charAt(i);
            }
            var dtexto = "";
            dtexto = dtexto + invertido.charAt(0);
            dtexto = dtexto + '-';
            cnt = 0;

            for (i = 1, j = 2; i < largo; i++ , j++) {
                if (cnt == 3) {
                    dtexto = dtexto + '.';
                    j++;
                    dtexto = dtexto + invertido.charAt(i);
                    cnt = 1;
                }
                else {
                    dtexto = dtexto + invertido.charAt(i);
                    cnt++;
                }
            }

            invertido = "";
            for (i = (dtexto.length - 1), j = 0; i >= 0; i-- , j++) {
                invertido = invertido + dtexto.charAt(i);
            }

            if (jQuery.Rut.digitoCorrecto(texto)) {
                return true;
            }
            return false;
        }
    };
}