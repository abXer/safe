﻿using System.Web;
using System.Web.Optimization;

namespace SAFE
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Librerias/jquery/jquery.min.js"                
                ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapminjs").Include(
                 "~/Librerias/bootstrap/js/bootstrap.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Js/jquery.validate*"
                ));



            bundles.Add(new ScriptBundle("~/bundles/customjs").Include(
                "~/Librerias/php-mail-form/validate.js",
                "~/Librerias/prettyphoto/js/prettyphoto.js",
                "~/Librerias/isotope/isotope.min.js",
                "~/Librerias/hover/hoverdir.js",
                "~/Librerias/hover/hoverex.min.js",
                "~/Librerias/unveil-effects/unveil-effects.js",
                "~/Librerias/owl-carousel/owl-carousel.js",
                "~/Librerias/jetmenu/jetmenu.js",
                "~/Librerias/animate-enhanced/animate-enhanced.min.js",
                "~/Librerias/jigowatt/jigowatt.js",
                "~/Librerias/easypiechart/easypiechart.min.js",
                "~/Js/main.js",
                "~/Js/bootstrap-toggle.min.js",
                "~/ExternalLibraries/DataTables/datatables.js",
                "~/ExternalLibraries/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js",
                "~/ExternalLibraries/bootstrap-datepicker-1.6.4-dist/locales/bootstrap-datepicker.es.min.js",                
                "~/Js/summernote.js",
                "~/Js/common.js"
                ));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Librerias/bootstrap/css/bootstrap.css",
                "~/Librerias/prettyphoto/css/prettyphoto.css",
                "~/Librerias/hover/hoverex-all.css",
                "~/Librerias/jetmenu/jetmenu.css",
                "~/Librerias/owl-carousel/owl-carousel.css",
                "~/Css/colors/blue.css",
                "~/Css/css-loader.css",
                "~/Css/bootstrap-toggle.min.css",
                "~/ExternalLibraries/DataTables/datatables.css",
                "~/ExternalLibraries/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.min.css",
                "~/Css/summernote.css",
                "~/Css/style.css"
                ));

            bundles.Add(new StyleBundle("~/bundles/editorcss").Include(
                "~/Css/editor/css/style.css",
                 "~/Css/editor/css/froala_editor.css",
                 "~/Css/editor/css/froala_style.css",
                 "~/Css/editor/css/plugins/code_view.css",
                 "~/Css/editor/css/plugins/draggable.css",
                 "~/Css/editor/css/plugins/colors.css",
                 "~/Css/editor/css/plugins/emoticons.css",
                 "~/Css/editor/css/plugins/image_manager.css",
                 "~/Css/editor/css/plugins/image.css",
                 "~/Css/editor/css/plugins/line_breaker.css",
                 "~/Css/editor/css/plugins/table.css",
                 "~/Css/editor/css/plugins/char_counter.css",
                 "~/Css/editor/css/plugins/video.css",
                 "~/Css/editor/css/plugins/fullscreen.css",
                 "~/Css/editor/css/plugins/file.css",
                 "~/Css/editor/css/plugins/quick_insert.css",
                 "~/Css/editor/css/plugins/help.css",
                 "~/Css/editor/css/third_party/spell_checker.css",
                 "~/Css/editor/css/plugins/special_characters.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/editorjs").Include(
                  "~/Js/editor/js/froala_editor.min.js",
                  "~/Js/editor//js/plugins/align.min.js",
                  "~/Js/editor//js/plugins/char_counter.min.js",
                  "~/Js/editor//js/plugins/code_beautifier.min.js",
                  "~/Js/editor//js/plugins/code_view.min.js",
                  "~/Js/editor//js/plugins/colors.min.js",
                  "~/Js/editor//js/plugins/draggable.min.js",
                  "~/Js/editor//js/plugins/emoticons.min.js",
                  "~/Js/editor//js/plugins/entities.min.js",
                  "~/Js/editor//js/plugins/file.min.js",
                  "~/Js/editor//js/plugins/font_size.min.js",
                  "~/Js/editor//js/plugins/font_family.min.js",
                  "~/Js/editor//js/plugins/fullscreen.min.js",
                  "~/Js/editor//js/plugins/image.min.js",
                  "~/Js/editor//js/plugins/image_manager.min.js",
                  "~/Js/editor//js/plugins/line_breaker.min.js",
                  "~/Js/editor//js/plugins/inline_style.min.js",
                  "~/Js/editor//js/plugins/link.min.js",
                  "~/Js/editor//js/plugins/lists.min.js",
                  "~/Js/editor//js/plugins/paragraph_format.min.js",
                  "~/Js/editor//js/plugins/paragraph_style.min.js",
                  "~/Js/editor//js/plugins/quick_insert.min.js",
                  "~/Js/editor//js/plugins/quote.min.js",
                  "~/Js/editor//js/plugins/table.min.js",
                  "~/Js/editor//js/plugins/save.min.js",
                  "~/Js/editor//js/plugins/url.min.js",
                  "~/Js/editor//js/plugins/video.min.js",
                  "~/Js/editor//js/plugins/help.min.js",
                  "~/Js/editor//js/plugins/print.min.js",
                  "~/Js/editor//js/third_party/spell_checker.min.js",
                  "~/Js/editor//js/plugins/special_characters.min.js",
                  "~/Js/editor//js/plugins/word_paste.min.js",
                  "~/Js/editor//js/languages/es.js"
                ));


            bundles.Add(new StyleBundle("~/bundles/fonts").Include(
                "~/Librerias/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform()
                ));

            BundleTable.EnableOptimizations = true;
        }
    }
}
