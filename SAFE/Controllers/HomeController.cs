﻿using SAFE.Domain.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FormServiciosGeneralesFactory = SAFE.Domain.Forms.FormServiciosGeneralesFactory;

namespace SAFE.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            Task.Run(() => { FormServiciosGeneralesFactory.OnBD(); });

            //if (!Request.IsAuthenticated)
            //    return RedirectToAction("Login", "Account");
            //else


            //var aa = FormServiciosGeneralesFactory.obtenerInstalacionesEmpresa(96299044);
            //var a = FormServiciosGeneralesFactory.obtenerPersonaRut(168084897);
            

            return View();
        }

        [AllowAnonymous]
        public ActionResult NotFound()
        {
            return View("~/Views/Shared/lostpage.cshtml");
        }

        [AllowAnonymous]
        public ActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult descargaPDF()
        {

            var html = Domain.Utils.Helper.leerTextoArchivo(Domain.Utils.Enums.PLANTILLAS_REPORTES.PRUEBA);
            var a = Domain.Utils.Helper.obtenerPDFdesdeHTML(html);


            return File(a, "nombreArchivo.pdf", "application/pdf");
        }
    }
}