﻿using SAFE.Domain.Abstract;
using SAFE.Domain.Constrols;
using SAFE.Domain.Entities;
using SAFE.Domain.Forms;
using SAFE.Domain.Utils;
using SAFE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAFE.Controllers
{
    public class EvaluacionController : Controller
    {


        [HttpGet]
        [Authorize]
        public ActionResult Index(string rE)
        {
            int rutEmpresa = Convert.ToInt32(Helper.Decrypt(rE));
            return View(FormEvaluacionFactory.EvaluacionesEmpresa(rutEmpresa));
        }

        [HttpGet]
        [Authorize]
        public ActionResult Evaluacion(string rE, string iTE, string iE)
        {
            int? rutEmpresa = !string.IsNullOrEmpty(rE) ? Convert.ToInt32(Helper.Decrypt(rE)) : (int?)null;
            int? idTipoEvaluacion = !string.IsNullOrEmpty(iTE) ? Convert.ToInt32(Helper.Decrypt(iTE)) : (int?)null;
            int? idEvaluacion = !string.IsNullOrEmpty(iE) ? Convert.ToInt32(Helper.Decrypt(iE)) : (int?)null;

            if (rutEmpresa != null && (idTipoEvaluacion == null && idEvaluacion == null)) //Para crear evaluaciones
            {
                return View(FormEvaluacionFactory.nuevaEvaluacion(Convert.ToInt32(rutEmpresa)));
            }
            else //Para leer una existente
            {
                return View(FormEvaluacionFactory.Obtener(Convert.ToInt32(rutEmpresa), Convert.ToInt32(idTipoEvaluacion), Convert.ToInt64(idEvaluacion)));
            }
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Evaluacion(FormEvaluacion form)
        {

            List<Observacion> Observaciones = new FormEvaluacionValidate().ValidarForm(form);

            Utils.limpiarModelo(ModelState);
            foreach (var item in Observaciones)
            {
                ModelState.AddModelError(string.Empty, (item as Observacion).detalle);
            }

            if (ModelState.IsValid)
            {

                Session["msjForm"] = (bool)FormEvaluacionFactory.Guardar(form) ? "Evaluación guardada correctamente." : "Hubo un problema al guardar la evaluación, favor intertar nuevamente más tarde.";
                return RedirectToAction("Index", "Evaluacion", new { rE = Helper.Encrypt(form.empresa.rut.rut) });
            }

            else
            {
                FormEvaluacionFactory.CargaDDL(form);
                return View(form);
            }
            //var a = form;
            //var b = ModelState;
            //bool c = ModelState.IsValid;

            ////Agregar lógica para guardado de evaluación cuando servicio esté listo

            //Session["insercionCorrecta"] = true; ;
            //return RedirectToAction("Evaluacion", new { rutEmpresa = 123 });
        }

        [Authorize]
        public ActionResult AgregarRevision(string tipoEvaluacion, string idIngeniero, string idSupervisor, string recomendacion, string idEvaluacion, string idEmpresa)
        {

            RevisionEvaluacion _rev = new RevisionEvaluacion();
            _rev.idTipoEvaluacion = Convert.ToInt32(tipoEvaluacion);
            _rev.ingeniero = IngenieroControl.listaIngenieros().Single(x => x.idIngeniero == Convert.ToInt32(idIngeniero));
            _rev.Supervisor = SupervisorControl.listaExpositores().Single(x => x.idSupervisor == Convert.ToInt32(idSupervisor));
            _rev.recomendacion = recomendacion;
            _rev.idEvaluacion = Convert.ToInt32(idEvaluacion);
            _rev.idEmpresa = Convert.ToInt32(idEmpresa);

            return Json(FormEvaluacionFactory.AgregarRevision(_rev), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImprimirEvaluacion(string rE, string tE, string iE)
        {
            int rutEmpresa = Convert.ToInt32(Helper.Decrypt(rE));
            int tipoEvaluacion = Convert.ToInt32(Helper.Decrypt(tE));
            long idEvaluacion = Convert.ToInt32(Helper.Decrypt(iE));

            var evaluacion = FormEvaluacionFactory.Obtener(rutEmpresa, tipoEvaluacion, idEvaluacion);
            string htmlInforme = Domain.Utils.Helper.leerTextoArchivo(Enums.PLANTILLAS_REPORTES.REPORTE_EVALUACION_TERRENO).Replace("{cuerpo}", evaluacion.evaluacion.Observaciones);

            var a = Domain.Utils.Helper.obtenerPDFdesdeHTML(htmlInforme);

            return File(a, "application/pdf", $"Informe evaluación código {evaluacion.evaluacion.idEvaluacion} empresa {evaluacion.empresa.nombre}.pdf");
        }
    }
}
