﻿using SAFE.Domain.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAFE.Controllers
{
    public class CommonController : Controller
    {
        [HttpGet]
        [Authorize]
        public ActionResult Comunas(int idRegion)
        {
            return Json(FormServiciosGeneralesFactory.obtenerComunas(idRegion), JsonRequestBehavior.AllowGet);
        }
    }
}