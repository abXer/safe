﻿using SAFE.Domain.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAFE.Controllers
{
    public class EmpresaController : Controller
    {
        // GET: Empresa
        [Authorize]
        public ActionResult Index()
        {
            FormEmpresa _form = new FormEmpresa();

            _form.listaEmpresas = FormServiciosGeneralesFactory.obtenerEmpresas();

            return View(_form);
        }
    }
}