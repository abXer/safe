﻿using SAFE.Domain.Abstract;
using SAFE.Domain.Forms;
using SAFE.Domain.Utils;
using SAFE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAFE.Controllers
{
    public class SaludController : Controller
    {


        [Authorize]
        public ActionResult Index(string rE)
        {
            return View(FormSaludFactory.Portada(Convert.ToInt32(Helper.Decrypt(rE))));
        }

        [HttpGet]
        [Authorize]
        public ActionResult AgendaMedica(string rE, string iA)
        {
            long? rutEmpresa = !string.IsNullOrEmpty(rE) ? Convert.ToInt32(Helper.Decrypt(rE)) : (int?)null;
            long? idAgendaMedica = !string.IsNullOrEmpty(iA) ? Convert.ToInt32(Helper.Decrypt(iA)) : (int?)null;

            return View(iA == null ? FormSaludFactory.nuevaAgendaMedica((int)rutEmpresa) : FormSaludFactory.leerAgendaMedica((long)idAgendaMedica));
        }

        [HttpPost]
        [Authorize]
        public ActionResult AgendaMedica(FormSalud form)
        {
            List<Observacion> Observaciones = new FormSaludValidate().ValidarFormAgendaMedica(form as object);

            Utils.limpiarModelo(ModelState);
            foreach (var item in Observaciones)
            {
                ModelState.AddModelError(string.Empty, (item as Observacion).detalle);
            }

            if (ModelState.IsValid)
            {
                string urlConfirmarVisitaMedica = Url.Action("confirmarVisitaMedica", "Salud", new { rE = Helper.Encrypt(form.empresa.rut.rut), iA = "{idUltimaAgendaMedica}" });
                var exito = (bool)FormSaludFactory.GuardarAgendaMedica(form, urlConfirmarVisitaMedica);

                Session["msjForm"] =
                    exito
                        ? "Agenda médica guardada correctamente."
                        : "Hubo un problema al guardar la agenda médica, favor intentar nuevamente más tarde.";
                return RedirectToAction("Index", new { rE = Helper.Encrypt(form.empresa.rut.rut) });
            }
            else
            {
                FormSaludFactory.CargaDDL(form);
                return View(form);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult confirmarVisitaMedica(string rE, string iA)
        {
            long? rutEmpresa = !string.IsNullOrEmpty(rE) ? Convert.ToInt32(Helper.Decrypt(rE)) : (int?)null;
            long? idAgendaMedica = !string.IsNullOrEmpty(iA) ? Convert.ToInt32(Helper.Decrypt(iA)) : (int?)null;

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult VisitaMedica(string rE, string iA, string iV)
        {
            long? rutEmpresa = !string.IsNullOrEmpty(rE) ? Convert.ToInt32(Helper.Decrypt(rE)) : (int?)null;
            long? idAgendaMedica = !string.IsNullOrEmpty(iA) ? Convert.ToInt32(Helper.Decrypt(iA)) : (int?)null;
            long? idVisitaMedica = !string.IsNullOrEmpty(iV) ? Convert.ToInt32(Helper.Decrypt(iV)) : (int?)null;

            if (rutEmpresa != null && iA != null)
            {
                return View(FormSaludFactory.nuevaVisitaMedica((long)rutEmpresa, (long)idAgendaMedica));
            }
            else
            {
                return View(FormSaludFactory.leerVisitaMedica((long)idVisitaMedica));
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult VisitaMedica(FormSalud form)
        {
            List<Observacion> Observaciones = new FormSaludValidate().ValidarFormAtencionMedica(form as object);

            Utils.limpiarModelo(ModelState);
            foreach (var item in Observaciones)
            {
                ModelState.AddModelError(string.Empty, (item as Observacion).detalle);
            }

            if (ModelState.IsValid)
            {
                Session["msjForm"] = (bool)FormSaludFactory.GuardarAtencionMedica(form) ? "Atención médica guardada correctamente." : "Hubo un problema al guardar la atención médica, favor intentar nuevamente más tarde.";
                return RedirectToAction("Index", new { rE = Helper.Encrypt(form.empresa.rut.rut), iA = Helper.Encrypt(form.agendaMedica.idAgendaMedica) });
            }
            else
            {
                FormSaludFactory.CargaDDL(form);
                return View(form);
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult FichaMedica(string iA)
        {
            long idAgendaMedica = Convert.ToInt64(Helper.Decrypt(iA));

            return View(FormSaludFactory.nuevaFichaMedica(idAgendaMedica));
        }

        [HttpPost]
        [Authorize]
        public ActionResult FichaMedica(FormSalud form)
        {
            List<Observacion> Observaciones = new FormSaludValidate().ValidarFormFichaMedica(form as object);

            Utils.limpiarModelo(ModelState);
            foreach (var item in Observaciones)
            {
                ModelState.AddModelError(string.Empty, (item as Observacion).detalle);
            }

            if (ModelState.IsValid)
            {
                Session["msjForm"] = (bool)FormSaludFactory.GuardarFichaMedica(form) ? "Ficha médica guardada correctamente." : "Hubo un problema al guardar su ficha médica, favor intentar nuevamente más tarde.";
                return RedirectToAction("VisitaMedica",
                    new
                    {
                        rE = Helper.Encrypt(form.empresa.rut.rut),
                        iA = Helper.Encrypt(form.atencionMedica.idAtencionMedica)
                    });
            }
            else
            {
                FormSaludFactory.CargaDDL(form);
                return View(form);
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult FichaMedicaTrabajador(string rT, string iE, string iA, string iAM)
        {
            long rutTrabajador = Convert.ToInt32(Helper.Decrypt(rT));
            int? idEmresa = !string.IsNullOrEmpty(iE) ? Convert.ToInt32(Helper.Decrypt(iE)) : (int?)null;
            long? idAgendaMedica = !string.IsNullOrEmpty(iA) ? Convert.ToInt32(Helper.Decrypt(iA)) : (long?)null;
            long? idAtencionMedica = !string.IsNullOrEmpty(iAM) ? Convert.ToInt32(Helper.Decrypt(iAM)) : (long?)null;

            if (iA != null)
            {
                if (idEmresa == null)
                    return View(FormSaludFactory.leerPerfilMedicoTrabajador(rutTrabajador));
                else
                    return View(FormSaludFactory.leerPerfilMedicoTrabajador((int)idEmresa, rutTrabajador, (long)idAtencionMedica));
            }
            else
            {
                return View(FormSaludFactory.leerPerfilMedicoTrabajador((long)idAgendaMedica, rutTrabajador));
            }
        }

        [HttpPost]
        public ActionResult agregarFichaMedica(FormSalud form)
        {
            return Json((bool)FormSaludFactory.GuardarFichaMedica(form), JsonRequestBehavior.AllowGet);
        }
    }
}