﻿using SAFE.Domain.Abstract;
using SAFE.Domain.Forms;
using SAFE.Domain.Utils;
using SAFE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAFE.Controllers
{
    public class CapacitacionController : Controller
    {

        [HttpGet]
        [Authorize]
        public ActionResult Index(string rE)
        {
            int rutEmpresa = Convert.ToInt32(Helper.Decrypt(rE));
            return View(FormCapacitacionFactory.nuevaCapacitacion(rutEmpresa));
        }

        [HttpGet]
        [Authorize]
        public ActionResult Capacitacion(string rE, string iC)
        {
            int? rutEmpresa = !string.IsNullOrEmpty(rE) ? Convert.ToInt32(Helper.Decrypt(rE)) : (int?)null;
            int? idCapacitacion = !string.IsNullOrEmpty(iC) ? Convert.ToInt32(Helper.Decrypt(iC)) : (int?)null;

            if (rutEmpresa != null && idCapacitacion == null)
            {
                return View(FormCapacitacionFactory.nuevaCapacitacion(Convert.ToInt32(rutEmpresa)));
            }
            else
            {
                return View(FormCapacitacionFactory.leerCapacitacion(Convert.ToInt32(rutEmpresa), Convert.ToInt64(idCapacitacion)));
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult Capacitacion(FormCapacitacion form)
        {
            List<Observacion> Observaciones = new FormCapacitacionValidate().ValidarForm(form);

            Utils.limpiarModelo(ModelState);
            foreach (var item in Observaciones)
            {
                ModelState.AddModelError(string.Empty, ((Observacion)item).detalle);
            }

            if (ModelState.IsValid)
            {

                Session["msjform"] = (bool)FormCapacitacionFactory.Guardar(form) ? "Capacitaci&oacute;n guardada correctamente." : "Hubo un problema al guardar la capacitaci&oacute;n, favor intententar nuevamente m&aacute;s tarde";
                return RedirectToAction("Index", "Capacitacion", new {rE = Helper.Encrypt(form.empresa.rut.rut)});
            }

            else
            {
                FormCapacitacionFactory.CargaDDL(form);
                return View(form);
            }
        }

        [Authorize]
        public ActionResult AgregarAsistencia(long idCapacitacion, int rutTrabajador, int rutEmpresa)
        {
            return Json(FormCapacitacionFactory.agregarAsistenciaCapacitacion(idCapacitacion, rutTrabajador, rutEmpresa), JsonRequestBehavior.AllowGet);
        }
    }
}
