﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Abstract
{
    public class Observacion
    {
        public string id
        {
            get
            {
                return new Guid().ToString();
            }
        }
        public DateTime fecha { get; set; }
        public string detalle { get; set; }
    }
}
