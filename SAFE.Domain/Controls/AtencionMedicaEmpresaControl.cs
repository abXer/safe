﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class AtencionMedicaEmpresaControl
    {
        private static IList<AtencionMedica> _atencionesMedicasEmpresas = null;

        public static IList<AtencionMedica> obtenerAtencionesMedicasEmpresa(int idEmpresa)
        {
            if (_atencionesMedicasEmpresas == null)
            {
                _atencionesMedicasEmpresas = new List<AtencionMedica>();
                _atencionesMedicasEmpresas = AtencionMedicaFactory.AtencionesMedicasEmpresa(idEmpresa);
            }
            else
            {
                if (!_atencionesMedicasEmpresas.Any(x => x.idEmpresa == idEmpresa))
                {
                    foreach (var _atm in AtencionMedicaFactory.AtencionesMedicasEmpresa(idEmpresa))
                    {
                        _atencionesMedicasEmpresas.Add(_atm);
                    }
                }
            }

            return _atencionesMedicasEmpresas.Where(x => x.idEmpresa == idEmpresa).ToList<AtencionMedica>();
        }

        public static void ActualizarAtencionesMedicasEmpresa(int idEmpresa)
        {
            var _atencionesNuevas = AtencionMedicaFactory.AtencionesMedicasEmpresa(idEmpresa);

            if (!_atencionesMedicasEmpresas.Any(x => x.idEmpresa == idEmpresa))
            {
                _atencionesMedicasEmpresas = AtencionMedicaFactory.AtencionesMedicasEmpresa(idEmpresa);
            }
            else
            {
                var _atencionesEliminar = _atencionesMedicasEmpresas.Where(x => x.idEmpresa == idEmpresa).ToList<AtencionMedica>();

                foreach (var _aE in _atencionesEliminar)
                    _atencionesMedicasEmpresas.Remove(_aE);

                foreach (var _aN in _atencionesNuevas)
                    _atencionesMedicasEmpresas.Add(_aN);

            }
        }
    }
}
