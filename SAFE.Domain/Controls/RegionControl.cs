﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class RegionControl
    {
        private static IList<Region> listaRegiones = null;

        public static IList<Region> obtenerRegiones()
        {

            if (listaRegiones == null)
            {
                listaRegiones = new List<Region>();

                foreach (Region _r in RegionFactory.listadoRegiones().OrderBy(x => x.idRegion))
                {
                    listaRegiones.Add(_r);
                }
            }

            return listaRegiones;
        }
    }
}
