﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class EmpresaControl
    {
        private static IList<Empresa> listaEmpresas = null;
        private static IList<Instalacion> listaInstalacionesEmpresa = null;

        public static IList<Empresa> obtenerEmpresas()
        {
            if (listaEmpresas == null || listaEmpresas?.Count == 0)
            {
                listaEmpresas = new List<Empresa>();
                listaEmpresas = EmpresaFactory.ListadoEmpresas();
            }

            return listaEmpresas;
        }

        public static IList<Instalacion> obtenerInstalacionesEmpresa(Rut rutEmpresa)
        {
            var idEmpresa = (int)obtenerEmpresas().FirstOrDefault(x => x.rut.rut == rutEmpresa.rut).idEmpresa;

            if (listaInstalacionesEmpresa == null)
            {
                listaInstalacionesEmpresa = EmpresaFactory.ListadoInstalacionesEmpresa(rutEmpresa);
            }
            else
            {
                if (!listaInstalacionesEmpresa.Any(x => x.idEmpresa == idEmpresa))
                {
                    foreach (Instalacion i in EmpresaFactory.ListadoInstalacionesEmpresa(rutEmpresa))
                    {
                        listaInstalacionesEmpresa.Add(i);
                    }
                }
            }


            return listaInstalacionesEmpresa.Where(x => x.idEmpresa == idEmpresa).ToList<Instalacion>();
        }

        public static IList<Instalacion> obtenerInstalacionesEmpresa(long idEmpresa)
        {
            Rut rutEmpresa = obtenerEmpresas().FirstOrDefault(x => x.idEmpresa == idEmpresa)?.rut;

            if (listaInstalacionesEmpresa == null)
            {
                listaInstalacionesEmpresa = EmpresaFactory.ListadoInstalacionesEmpresa(rutEmpresa);
            }
            else
            {
                if (!listaInstalacionesEmpresa.Any(x => x.idEmpresa == idEmpresa))
                {
                    foreach (Instalacion i in EmpresaFactory.ListadoInstalacionesEmpresa(rutEmpresa))
                    {
                        listaInstalacionesEmpresa.Add(i);
                    }
                }
            }


            return listaInstalacionesEmpresa.Where(x => x.idEmpresa == idEmpresa).ToList<Instalacion>();
        }

        public static void ActualizarListadoEmpresas()
        {
            var nuevaListaEmpresas = EmpresaFactory.ListadoEmpresas();
            var empresasEliminar = listaEmpresas.Where(x => x.idEmpresa != null).ToList();

            foreach (var emp in empresasEliminar)
                listaEmpresas.Remove(emp);

            foreach (var emp in nuevaListaEmpresas)
                listaEmpresas.Add(emp);
        }
    }
}
