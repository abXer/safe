﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Menu = SAFE.Domain.Entities.Menu;
using Perfil = SAFE.Domain.Entities.Perfil;

namespace SAFE.Domain.Constrols
{
    public class UsuarioControl
    {
        private static IList<Usuario> listadoUsuarios = null;
        private static IList<Perfil> listadoPerfiles = null;
        private static IList<Menu> listadoMenus = null;
        private static Dictionary<int, int> listadoAsociacionPerfilMenu = null;

        internal static IList<Usuario> listaUsuarios()
        {
            if (listadoUsuarios == null)
            {
                listadoUsuarios = new List<Usuario>();
                listadoUsuarios = UsuarioFactory.listaUsuarios();
            }
            return listadoUsuarios;
        }

        internal static IList<Perfil> listaPerfiles()
        {
            if (listadoPerfiles == null)
            {
                listadoPerfiles = new List<Perfil>();
                listadoPerfiles = PerfilFactory.listaPerfiles();
            }
            return listadoPerfiles;
        }

        internal static IList<Menu> listaMenus(int idPerfil)
        {
            if (listadoMenus == null)
            {
                listadoMenus = new List<Menu>();
                listadoMenus = MenuFactory.listaMenus();
            }
            return listadoMenus.Where(x => asociacionPerfilMenu().ContainsKey(idPerfil)).ToList();
        }

        private static Dictionary<int, int> asociacionPerfilMenu()
        {
            if (listadoAsociacionPerfilMenu == null)
            {
                listadoAsociacionPerfilMenu = new Dictionary<int, int>();
                listadoAsociacionPerfilMenu = PerfilFactory.listaAsociacionMenus();
            }
            return listadoAsociacionPerfilMenu;
        }

        internal static void actualizarListaUsuarios()
        {
            var usuariosEliminar = listadoUsuarios.ToList();
            var usuariosNuevos = UsuarioFactory.listaUsuarios();

            foreach (var _u in usuariosEliminar)
                listadoUsuarios.Remove(_u);

            foreach (var _u in usuariosNuevos)
                listadoUsuarios.Add(_u);
        }
    }
}
