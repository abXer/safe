﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class EvaluacionControl
    {
        private static IList<Evaluacion> listaEvaluaciones = null;
        private static IList<RevisionEvaluacion> listaRevisionesEvaluaciones = null;

        public static IList<Evaluacion> obtenerListaEvaluaciones(int idEmpresa)
        {
            int rutEmpresa = EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.idEmpresa == idEmpresa).rut.rut;
            if (listaEvaluaciones == null)
            {
                listaEvaluaciones = EvaluacionFactory.ObtenerEvaluacionesEmpresa(rutEmpresa);
            }
            else
            {
                if (!listaEvaluaciones.Any(x => x.idEmpresa == idEmpresa))
                {
                    foreach (Evaluacion eva in EvaluacionFactory.ObtenerEvaluacionesEmpresa(rutEmpresa))
                    {
                        listaEvaluaciones.Add(eva);
                    }
                }
            }


            return listaEvaluaciones.Where(x => x.idEmpresa == idEmpresa).ToList<Evaluacion>();
        }

        public static Evaluacion obtenerListaEvaluaciones(int tipoEvaluacion, long idEvaluacion)
        {
            return listaEvaluaciones?.Where(x => x.tipoEvaluacion == tipoEvaluacion && x.idEvaluacion == idEvaluacion)?.FirstOrDefault<Evaluacion>();
        }
        
        public static void ActualizarEvaluaciones(long idEmpresa)
        {
            int rutEmpresa = EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.idEmpresa == idEmpresa).rut.rut;
            if (listaEvaluaciones == null)
            {
                listaEvaluaciones = EvaluacionFactory.ObtenerEvaluacionesEmpresa(rutEmpresa);
            }
            else
            {

                listaEvaluaciones = new List<Evaluacion>();
                foreach (Evaluacion eva in EvaluacionFactory.ObtenerEvaluacionesEmpresa(rutEmpresa))
                    listaEvaluaciones.Add(eva);
            }
        }
    }
}
