﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class IngenieroControl
    {
        private static IList<Ingeniero> listadoIngenieros = null;

        public static IList<Ingeniero> listaIngenieros()
        {
            if (listadoIngenieros == null)
            {
                listadoIngenieros = new List<Ingeniero>();
                listadoIngenieros = IngenieroFactory.ObtenerIngenieros();
            }
            return listadoIngenieros;
        }
    }
}
