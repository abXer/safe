﻿using SAFE.Dao.Dao;
using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class TrabajadorControl
    {
        private static IList<Trabajador> listadoTrabajadores = null;
        private static IList<FichaMedica> listadoFichasMedicas = null;
        private static IList<Examen> listadoExamenesMedicos = null;

        public static IList<Trabajador> listaTrabajadoresEmpresas(long idEmpresa)
        {
            if (listadoTrabajadores == null)
            {
                listadoTrabajadores = new List<Trabajador>();
                listadoTrabajadores = TrabajadorFactory.listaTrabajadoresEmpresa(idEmpresa);
            }
            else
            {
                if (!listadoTrabajadores.Any(x => x.idEmpresa == idEmpresa))
                {
                    foreach (Trabajador _t in TrabajadorFactory.listaTrabajadoresEmpresa(idEmpresa))
                    {
                        listadoTrabajadores.Add(_t);
                    }
                }
            }

            return listadoTrabajadores.Where(x => x.idEmpresa == idEmpresa).ToList<Trabajador>();
        }

        internal static IList<Trabajador> fichasMedicasTrabajadoresConExamen(int idEmpresa)
        {
            Empresa _emp = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == idEmpresa);

            List<Task> poolTareas = new List<Task>();

            IList<Trabajador> trabajadores = new List<Trabajador>();
            IList<FichaMedica> fichasMedicas = new List<FichaMedica>();
            IList<Examen> examenes = new List<Examen>();


            poolTareas.Add(Task.Run(() => trabajadores = listaTrabajadoresEmpresas(idEmpresa).Where(x => x.idEmpresa == idEmpresa).ToList<Trabajador>()));

            poolTareas.Add(Task.Run(() => fichasMedicas = fichasMedicasEmpresa(_emp.rut.rut)));

            poolTareas.Add(Task.Run(() => examenes = ExamenesEmpresa(_emp.rut.rut)));

            Task.WaitAll(poolTareas.ToArray());


            for (var i = 0; i < trabajadores.Count; i++)
            {
                trabajadores[i].fichasMedicas = new List<FichaMedica>();
                trabajadores[i].fichasMedicas = fichasMedicas.Where(x => x.idTrabajador == trabajadores[i].idTrabajador).ToList<FichaMedica>();

                if (trabajadores[i].fichasMedicas?.Count > 0)
                {
                    for (int j = 0; j < trabajadores[i].fichasMedicas.Count; j++)
                    {
                        trabajadores[i].fichasMedicas[j].examenes = new List<Examen>();
                        trabajadores[i].fichasMedicas[j].examenes = examenes.Where(x => x.idFichaMedica == trabajadores[i].fichasMedicas[j].idFichaMedica).ToList<Examen>();
                    }
                }
            }

            return trabajadores;
        }

        public static void ActualizarFichasMedica(int idEmpresa)
        {
            var empresa = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == idEmpresa);

            var fichasNuevas = fichaMedicaFactory.listadoFichasMedicasEmpresa(empresa.rut.rut);
            var fichasEliminar = listadoFichasMedicas.Where(x => x.idEmpresa == empresa.idEmpresa).ToList();

            foreach (var f in fichasEliminar)
                listadoFichasMedicas.Remove(f);

            foreach (var f in fichasNuevas)
                listadoFichasMedicas.Add(f);


        }

        private static IList<FichaMedica> fichasMedicasEmpresa(int rutEmpresa)
        {
            var _emp = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);

            if (listadoFichasMedicas == null)
            {
                listadoFichasMedicas = new List<FichaMedica>();
                listadoFichasMedicas = fichaMedicaFactory.listadoFichasMedicasEmpresa(rutEmpresa);
            }
            else
            {
                if (!listadoFichasMedicas.Any(x => x.idEmpresa == _emp.idEmpresa))
                {
                    foreach (FichaMedica _f in fichaMedicaFactory.listadoFichasMedicasEmpresa(rutEmpresa))
                    {
                        listadoFichasMedicas.Add(_f);
                    }
                }
            }

            return listadoFichasMedicas.Where(x => x.idEmpresa == _emp.idEmpresa).ToList<FichaMedica>();

        }

        internal static IList<Examen> ExamenesEmpresa(int rutEmpresa)
        {
            Empresa _emp = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);

            if (listadoExamenesMedicos == null)
            {
                listadoExamenesMedicos = new List<Examen>();
                listadoExamenesMedicos = ExamenFactory.listadoExamenesEmpresa(rutEmpresa);
            }
            else
            {
                if (!listadoExamenesMedicos.Any(x => x.idEmpresa == _emp.idEmpresa))
                {
                    foreach (Examen _e in ExamenFactory.listadoExamenesEmpresa(rutEmpresa))
                    {
                        listadoExamenesMedicos.Add(_e);
                    }
                }
            }

            return listadoExamenesMedicos.Where(x => x.idEmpresa == _emp.idEmpresa).ToList<Examen>();
            ;

        }
    }
}
