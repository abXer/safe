﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class ExpositorControl
    {
        private static IList<Expositor> listadoExpositores = null;

        public static IList<Expositor> listaExpositores()
        {
            if (listadoExpositores == null)
            {
                listadoExpositores = new List<Expositor>();
                listadoExpositores = ExpositorFactory.listaExpositores();
            }
            return listadoExpositores;
        }
    }
}
