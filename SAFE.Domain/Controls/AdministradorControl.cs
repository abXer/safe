﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class AdministradorControl
    {
        private static IList<Administrador> listadoAdministradores = null;

        public static IList<Administrador> listaAdministradores()
        {
            if (listadoAdministradores == null)
            {
                listadoAdministradores = new List<Administrador>();
                listadoAdministradores = AdministradorFactory.listadoAdministradores();
            }
            return listadoAdministradores;
        }
    }
}
