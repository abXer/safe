﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class DoctorControl
    {
        private static IList<Medico> listadoMedicos = null;

        public static IList<Medico> listaDoctores()
        {
            if (listadoMedicos == null)
            {
                listadoMedicos = new List<Medico>();
                listadoMedicos = MedicoFactory.listaMedicos();
            }
            return listadoMedicos;
        }
    }
}
