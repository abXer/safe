﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class SupervisorControl
    {
        private static IList<Supervisor> listadoSupervisores = null;

        public static IList<Supervisor> listaExpositores()
        {
            if (listadoSupervisores == null)
            {
                listadoSupervisores = new List<Supervisor>();
                listadoSupervisores = SupervisorFactory.listaSupervisores();
            }
            return listadoSupervisores;
        }
    }
}
