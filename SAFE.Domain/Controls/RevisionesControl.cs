﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Entities;

namespace SAFE.Domain.Constrols
{
    public class RevisionesControl
    {
        private static IList<RevisionEvaluacion> listaRevisiones = null;

        internal static IList<RevisionEvaluacion> ListaRevisionesEvaluacion(int rutEmpresa, int tipoEvaluacion, long idEvaluacion)
        {
            Empresa emp = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);

            if (listaRevisiones == null)
            {
                listaRevisiones = new List<RevisionEvaluacion>();
                listaRevisiones = RevisionEvaluacionFactory.listadoRevisiones(emp.rut.rut, idEvaluacion);
            }
            else
            {
                if (listaRevisiones.Any(x => x.idEmpresa == emp.idEmpresa))
                    return listaRevisiones.Where(x => x.idEmpresa == emp.idEmpresa && x.idTipoEvaluacion == tipoEvaluacion && x.idEvaluacion == idEvaluacion).ToList<RevisionEvaluacion>();

                foreach (RevisionEvaluacion com in RevisionEvaluacionFactory.listadoRevisiones(emp.rut.rut, idEvaluacion))
                {
                    listaRevisiones.Add(com);
                }
            }


            return listaRevisiones.Where(x => x.idEmpresa == emp.idEmpresa && x.idTipoEvaluacion == tipoEvaluacion && x.idEvaluacion == idEvaluacion).ToList<RevisionEvaluacion>();
        }

        internal static void ActualizarRevisionesEvaluacion(int rutEmpresa, int tipoEvaluacion, long idEvaluacion)
        {
            var emp = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);

            var revisionesNuevas = RevisionEvaluacionFactory.listadoRevisiones(rutEmpresa, idEvaluacion).ToList();
            var revisionesEliminar = listaRevisiones.Where(x => x.idEmpresa == emp.idEmpresa && x.idEvaluacion == idEvaluacion).ToList();

            foreach (var rv in revisionesEliminar)
                listaRevisiones.Remove(rv);

            foreach (var rv in revisionesNuevas)
                listaRevisiones.Add(rv);

        }
    }
}
