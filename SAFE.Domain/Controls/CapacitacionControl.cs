﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Entities;

namespace SAFE.Domain.Constrols
{
    public class CapacitacionControl
    {
        private static IList<Capacitacion> listaCapacitaciones = null;

        internal static IList<Capacitacion> listaCapacitacionesEmpresa(int rutEmpresa)
        {
            var _emp = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);

            if (listaCapacitaciones == null)
            {
                listaCapacitaciones = CapacitacionFactory.listaCapacitaciones((int)_emp.idEmpresa);
            }
            else
            {
                if (!listaCapacitaciones.Any(x => x.idEmpresa == _emp.idEmpresa))
                {
                    foreach (Capacitacion cap in CapacitacionFactory.listaCapacitaciones((int)_emp.idEmpresa))
                    {
                        listaCapacitaciones.Add(cap);
                    }
                }
            }


            return listaCapacitaciones.Where(x => x.idEmpresa == _emp.idEmpresa).ToList<Capacitacion>();
        }

        public static void actualizaCapacitacionesEmpresa(long idEmpresa)
        {
            var emp = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == idEmpresa);
            var capacitacionesNuevas = CapacitacionFactory.listaCapacitaciones((int)emp.idEmpresa);
            var capacitacionesEliminar = listaCapacitaciones.Where(x => x.idEmpresa == idEmpresa).ToList();

            foreach (var c in capacitacionesEliminar)
                listaCapacitaciones.Remove(c);

            foreach (var c in capacitacionesNuevas)
                listaCapacitaciones.Add(c);

        }
    }
}
