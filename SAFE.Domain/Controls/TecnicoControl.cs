﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Constrols
{
    public class TecnicoControl
    {
        private static IList<Tecnico> listadoTecnicos = null;

        public static IList<Tecnico> listaTecnicos()
        {
            if (listadoTecnicos == null)
            {
                listadoTecnicos = new List<Tecnico>();
                listadoTecnicos = TecnicoFactory.listaTecnicos();
            }
            return listadoTecnicos;
        }
    }
}
