﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Entities;

namespace SAFE.Domain.Constrols
{
    public class ComunaControl
    {
        private static IList<Comuna> listaComuna = null;

        internal static IList<Comuna> listaComunas(int idRegion)
        {
            if (idRegion != 0)
            {
                if (listaComuna == null)
                {
                    listaComuna = ComunaFactory.listadoComunas(idRegion);
                }
                else
                {
                    if (!listaComuna.Any(x => x.idRegion == idRegion))
                    {
                        foreach (Comuna com in ComunaFactory.listadoComunas(idRegion))
                        {
                            listaComuna.Add(com);
                        }
                    }
                }
            }

            return listaComuna.Where(x => x.idRegion == idRegion).OrderBy(x => x.idComuna).ToList<Comuna>();
        }
    }
}
