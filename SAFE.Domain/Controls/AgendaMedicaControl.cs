﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Entities;

namespace SAFE.Domain.Constrols
{
    public class AgendaMedicaControl
    {
        private static IList<AgendaMedica> agendaMedicaEmpresas = null;
        private static IList<AgendaMedica> agendaMedicaDoctores = null;

        internal static IList<AgendaMedica> agendaMedicaEmpresa(int rutEmpresa)
        {
            var idEmpresa = (int)EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa).idEmpresa;
            var rut = new Rut() { rut = rutEmpresa };
            if (agendaMedicaEmpresas == null)
            {
                agendaMedicaEmpresas = new List<AgendaMedica>();
                agendaMedicaEmpresas = AgendaMedicaFactory.listaAgendaMedicaEmpresa(rut);
            }
            else
            {
                if (!agendaMedicaEmpresas.Any(x => x.idEmpresa == idEmpresa))
                {
                    foreach (AgendaMedica _a in AgendaMedicaFactory.listaAgendaMedicaEmpresa(rut))
                    {
                        agendaMedicaEmpresas.Add(_a);
                    }
                }
            }

            return agendaMedicaEmpresas.Where(x => x.idEmpresa == idEmpresa).ToList<AgendaMedica>();
        }

        internal static IList<AgendaMedica> agendaMedicaDoctor(int rutDoctor)
        {
            var idMedico = (long)DoctorControl.listaDoctores().Single(x => x.rut.rut == rutDoctor).idMedico;
            var rut = new Rut() { rut = rutDoctor };

            if (agendaMedicaDoctores == null)
            {
                agendaMedicaDoctores = new List<AgendaMedica>();
                agendaMedicaDoctores = AgendaMedicaFactory.listaAgendaMedicaDoctor(rut);
            }
            else
            {
                if (!agendaMedicaDoctores.Any(x => x.medico.idMedico == idMedico))
                {
                    foreach (AgendaMedica _a in AgendaMedicaFactory.listaAgendaMedicaDoctor(rut))
                    {
                        agendaMedicaDoctores.Add(_a);
                    }
                }
            }

            return agendaMedicaDoctores.Where(x => x.medico.idMedico == idMedico).ToList<AgendaMedica>();
        }

        public static void actualizaAgendasMedicasEmpresa(long idEmpresa)
        {
            var emp = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == idEmpresa);
            var nuevasAgendasMedicas = AgendaMedicaFactory.listaAgendaMedicaEmpresa(emp.rut);

            if (agendaMedicaEmpresas?.Count > 0)
            {
                var agendasMedicasEliminar = agendaMedicaEmpresas.ToList();

                foreach (var agenda in agendasMedicasEliminar)
                    agendaMedicaEmpresas.Remove(agenda);
            }

            if (agendaMedicaEmpresas == null)
            {
                agendaMedicaEmpresas = new List<AgendaMedica>();
            }

            foreach (var agenda in nuevasAgendasMedicas)
                agendaMedicaEmpresas.Add(agenda);
        }
    }
}
