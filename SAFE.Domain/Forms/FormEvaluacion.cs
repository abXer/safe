﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Abstract;
using SAFE.Domain.Constrols;
using SAFE.Domain.Entities;
using SAFE.Domain.Interfaces;
using SAFE.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Forms
{
    public class FormEvaluacion
    {
        public Empresa empresa { get; set; }
        public IList<Evaluacion> listaEvaluaciones { get; set; }
        public IList<Instalacion> listaInstalacionesEmpresa { get; set; }
        public Evaluacion evaluacion { get; set; }
        public int informeOK { get; set; }
        public RevisionEvaluacion nuevaRevision { get; set; }
        public IList<Region> _listaRegiones { get; set; }
        public IList<Comuna> _listaComunas { get; set; }
        public IList<Tecnico> _listaTecnicos { get; set; }

    }
    public class FormEvaluacionFactory
    {

        public static FormEvaluacion EvaluacionesEmpresa(int rutEmpresa)
        {
            FormEvaluacion _form = new FormEvaluacion();
            _form.empresa = EmpresaFactory.Obtener(new Rut() { rut = rutEmpresa });
            _form.listaEvaluaciones = EvaluacionControl.obtenerListaEvaluaciones((int)_form.empresa.idEmpresa);
            return _form;
        }

        public static FormEvaluacion nuevaEvaluacion(int rutEmpresa)
        {
            FormEvaluacion _form = new FormEvaluacion();
            _form.empresa = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);
            _form.listaEvaluaciones = EvaluacionControl.obtenerListaEvaluaciones((int)_form.empresa.idEmpresa);
            CargaDDL(_form);
            return _form;
        }

        public static FormEvaluacion Obtener(int rutEmpresa, int tipoEvaluacion, long idEvaluacion)
        {
            FormEvaluacion _form = new FormEvaluacion();
            _form._listaTecnicos = FormServiciosGeneralesFactory.obtenerTecnicos();

            _form.empresa = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);
            _form.evaluacion = EvaluacionFactory.Obtener(tipoEvaluacion, idEvaluacion);
            _form.evaluacion.revisiones = RevisionesControl.ListaRevisionesEvaluacion(rutEmpresa, tipoEvaluacion, (long)_form.evaluacion.idEvaluacion).ToList<RevisionEvaluacion>();

            _form.listaInstalacionesEmpresa = FormServiciosGeneralesFactory.obtenerInstalacionesEmpresa(rutEmpresa);

            CargaDDL(_form);

            return _form;
        }

        public static bool Guardar(FormEvaluacion form)
        {
            /*
                /agregarEvaluacion
                Nota: tipoEvaluacion 1 = instalacion, 2 = trabajador
                {"id": 10,
                  "fechaEvaluacion": "2009-02-15T00:00:00Z",
                  "observacion": "azure",
                  "idTecnico": 1,
                  "idInstalacion": 1,
                  "informeOk": 1,
                  "etapaEva": 1,
                   "tipoEvaluacion":1}            
             */
            FormEvaluacion _form = new FormEvaluacion();

            _form.evaluacion = new Evaluacion();

            _form.evaluacion.idEvaluacion = form.evaluacion.idEvaluacion;
            _form.evaluacion.tipoEvaluacion = form.evaluacion.tipoEvaluacion;
            _form.evaluacion.fechaEvaluacion = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            _form.evaluacion.Observaciones = form.evaluacion.Observaciones;
            _form.evaluacion.tecnico = TecnicoControl.listaTecnicos().FirstOrDefault(x => x.idTecnico == form.evaluacion.tecnico.idTecnico);
            _form.evaluacion.idEmpresa = (int)form.empresa.idEmpresa;
            if (form.evaluacion.tipoEvaluacion == Enums.tipoEvaluacion.INSTALACIONES)
                _form.evaluacion.instalacion = EmpresaControl.obtenerInstalacionesEmpresa(form.empresa.rut).FirstOrDefault(x => x.idInstalacionEmpresa == form.evaluacion.instalacion.idInstalacionEmpresa);


            return EvaluacionFactory.GuardarEvaluacion(_form.evaluacion);
        }

        public static bool AgregarRevision(RevisionEvaluacion rev)
        {
            Empresa _emp = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == rev.idEmpresa);

            bool resultado = EvaluacionFactory.AgregarRevision(rev);

            if (resultado)
                RevisionesControl.ActualizarRevisionesEvaluacion(_emp.rut.rut, rev.idTipoEvaluacion, (long)rev.idEvaluacion);

            return resultado;
        }

        public static void CargaDDL(FormEvaluacion form)
        {
            if (form.empresa?.rut?.rut != null)
            {
                form.listaInstalacionesEmpresa = FormServiciosGeneralesFactory.obtenerInstalacionesEmpresa(form.empresa.rut.rut);
            }

            if (form.evaluacion?.comuna?.idRegion != null && form.evaluacion?.comuna?.idRegion != 0)
            {
                form._listaComunas = FormServiciosGeneralesFactory.obtenerComunas(form.evaluacion.region.idRegion);
            }
            else
                form._listaComunas = new List<Comuna>();


            form._listaRegiones = FormServiciosGeneralesFactory.obtenerRegiones();
            form._listaTecnicos = FormServiciosGeneralesFactory.obtenerTecnicos();
        }
    }

    public class FormEvaluacionValidate : IFormValidate
    {
        public List<Observacion> ValidarForm(object form)
        {
            List<Observacion> _Observaciones = new List<Observacion>();
            FormEvaluacion _form = form as FormEvaluacion;

            if (_form.evaluacion == null)
            {
                _Observaciones.Add(new Observacion
                {
                    detalle = "Debe completar la evaluación"
                });
            }
            else
            {
                if (_form.evaluacion.tipoEvaluacion != Enums.tipoEvaluacion.INSTALACIONES && _form.evaluacion.tipoEvaluacion != Enums.tipoEvaluacion.TRABAJADORES)
                {
                    _Observaciones.Add(new Observacion
                    {
                        detalle = "Debe seleccionar un tipo de evaluación"
                    });
                }

                if (_form.evaluacion.tipoEvaluacion == Enums.tipoEvaluacion.INSTALACIONES && _form.evaluacion.instalacion?.idInstalacionEmpresa == null)
                {
                    _Observaciones.Add(new Observacion
                    {
                        detalle = "Debe seleccionar una instalación de la empresa"
                    });
                }

                if (_form.evaluacion.region?.idRegion == null || _form.evaluacion.region?.idRegion == 0)
                {
                    _Observaciones.Add(new Observacion
                    {
                        detalle = "Debe seleccionar una región"
                    });
                }

                if (_form.evaluacion.comuna?.idComuna == null || _form.evaluacion.comuna?.idComuna == 0)
                {
                    _Observaciones.Add(new Observacion
                    {
                        detalle = "Debe seleccionar una comuna"
                    });
                }

                if (_form.evaluacion.tecnico?.idTecnico == null || _form.evaluacion.tecnico?.idTecnico == 0)
                {
                    _Observaciones.Add(new Observacion
                    {
                        detalle = "Debe seleccionar un técnico"
                    });
                }

                if (string.IsNullOrEmpty(_form.evaluacion.Observaciones))
                {
                    _Observaciones.Add(new Observacion
                    {
                        detalle = "Debe completar el informe"
                    });
                }
            }

            return _Observaciones;
        }
    }
}
