﻿using SAFE.Domain.Constrols;
using SAFE.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Web.SessionState;
using SAFE.Dao.Dao;

namespace SAFE.Domain.Forms
{
    public class FormServiciosGeneralesFactory
    {
        public static IList<Region> obtenerRegiones()
        {
            return RegionControl.obtenerRegiones();
        }

        public static IList<Comuna> obtenerComunas(int idRegion)
        {
            if (idRegion != 0)
                return ComunaControl.listaComunas(idRegion);
            else return new List<Comuna>();
        }

        public static IList<Empresa> obtenerEmpresas()
        {
            return EmpresaControl.obtenerEmpresas();
        }

        public static IList<Instalacion> obtenerInstalacionesEmpresa(int rutEmpresa)
        {
            return EmpresaControl.obtenerInstalacionesEmpresa(new Rut() { rut = rutEmpresa });
        }

        public static AgendaMedica obtenerUltimaAgendaMedicaEmpresa(int rutEmpresa)
        {
            return AgendaMedicaControl.agendaMedicaEmpresa(rutEmpresa).OrderBy(x => x.idAgendaMedica).LastOrDefault();
        }

        public static bool GuardarPersona(Persona persona, int idTipoPersona)
        {
            /* TIPOS
             *  1	ADMINISTRADOR
                2	EXPOSITOR
                3	INGENIERO
                4	SUPERVISOR
                5	TECNICO
                6	TRABAJADOR
                7	MEDICO
                8	EMPRESA
             */
            return PersonaFactory.GuardarPersona(persona, idTipoPersona);
        }

        /*
         * Servicios claudiño
         */

        /*
         * Metodo: ListaIngenieros
            Entrada: nada 
            Retorno: 
            - Todos los campos de tabla persona e Ingeniero"
         */
        public static IList<Ingeniero> ListaIngenieros() { return IngenieroControl.listaIngenieros(); }


        /*
         * 
            Metodo: ListaMedicos
            Entrada: nada 
            Retorno: 
          - Todos los campos de tabla persona y Médico"
         */

        public static IList<Medico> listaMedicos() { return DoctorControl.listaDoctores(); }

        /*
         *Metodo: ListaPersonas
            Entrada: nada 
            Retorno: 
          - Todos los campos de tabla persona y sus derivaciones, ej, Ingeniero, técnico, etc"
         */
        // Ya hay servicios por cada uno!
        public static IList<Tecnico> obtenerTecnicos()
        {
            return TecnicoControl.listaTecnicos();
        }

        public static IList<Expositor> obtenerExpositores()
        {
            return ExpositorControl.listaExpositores();
        }

        public static IList<Administrador> obtenerAdministradores()
        {
            return AdministradorControl.listaAdministradores();
        }

        public static Persona obtenerPersonaRut(int rut)
        {
            return PersonaFactory.obtenerPersona(new Rut() { rut = rut });
        }

        public static IList<Supervisor> obtenerSupervisores()
        {
            return SupervisorControl.listaExpositores();
        }

        public static IList<Usuario> obtenerListaUsuarios()
        {
            return UsuarioControl.listaUsuarios();
        }

        /*
         * Metodo: ListaTrabajadores
            Entrada: nada 
            Retorno: 
          - Todos los campos de tabla persona y Trabajadores"

            --solo con idEmpresa como filtro
         */
        public static IList<Trabajador> obtenerTrabajadoresEmpresa(long idEmpresa)
        {
            return TrabajadorControl.listaTrabajadoresEmpresas(idEmpresa);
        }

        /*
         * Metodo: ListaEvaluacionTrabajador
            Entrada: nada 
            Retorno: 
                      - Todos los campos de tabla Evaluacion_trabajador"
            Metodo: ListaEvaluacionInstalacion
            Entrada: nada 
            Retorno: 
          - Todos los campos de tabla Evaluacion_instalacion"
         */

        public static IList<Evaluacion> listaEvaluacionTrabajador(int idEmpresa)
        {
            return EvaluacionControl.obtenerListaEvaluaciones(idEmpresa).Where(x => x.tipoEvaluacion == Utils.Enums.tipoEvaluacion.TRABAJADORES).ToList<Evaluacion>();
        }

        public static IList<Evaluacion> listaEvaluacionInstalacion(int idEmpresa)
        {
            return EvaluacionControl.obtenerListaEvaluaciones(idEmpresa).Where(x => x.tipoEvaluacion == Utils.Enums.tipoEvaluacion.INSTALACIONES).ToList<Evaluacion>();
        }

        public static Usuario Autenticacion(int rut, string password)
        {
            return FormUsuarioFactory.Autenticacion(rut, password);
        }

        public static IList<Perfil> listaPerfiles()
        {
            return UsuarioFactory.listaPerfiles();
        }

        public static IList<Menu> listaMenus()
        {
            return UsuarioFactory.listaMenu();
        }

        public static Dictionary<int, int> listaAsociacionPerfilMenu()
        {
            return UsuarioFactory.listaAsociacionPerfilMenu();
        }

        public static void OnBD()
        {
            EmpresaDAO.ondb();
        }
    }
}
