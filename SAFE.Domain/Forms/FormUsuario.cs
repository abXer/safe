﻿using SAFE.Dao.Utils;
using SAFE.Domain.Abstract;
using SAFE.Domain.Entities;
using SAFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SAFE.Domain.Utils.Enums;
using UsuarioFactory = SAFE.Domain.Entities.UsuarioFactory;

namespace SAFE.Domain.Forms
{
    public class FormUsuario
    {
        public Usuario usuario { get; set; }
    }

    public class FormUsuarioFactory
    {
        public static Usuario Autenticacion(long rut, string password)
        {
            return UsuarioFactory.Autenticar(rut, password);
        }
    }

    public class FormUsuarioValidate : IFormValidate
    {
        public List<Observacion> ValidarForm(object form)
        {
            //FormUsuario _form = form as FormUsuario;
            List<Observacion> _lst = new List<Observacion>();

            //if (_form?.usuario?.rut?.rut != null)
            //{
            //    if (string.IsNullOrEmpty(_form.usuario.password))
            //    {
            //        _lst.Add(new Observacion()
            //        {
            //            fecha = DateTime.Now,
            //            detalle = "La contraseña no fue ingresada"
            //        });
            //    }
            //}
            //else
            //{
            //    _lst.Add(new Observacion()
            //    {
            //        fecha = DateTime.Now,
            //        detalle = "El Rut no fue ingresado"
            //    });
            //}

            return _lst;
        }
    }
}
