﻿using SAFE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Forms
{
    public class FormEmpresa
    {
        public IList<Empresa> listaEmpresas { get; set; }
        public Empresa empresa { get; set; }
    }

    public class FormEmpresaFactory
    {
        public static bool GuardarEmpresa(FormEmpresa form)
        {

            Empresa emp = new Empresa
            {
                idEmpresa = form.empresa.idEmpresa,
                rut = form.empresa.rut,
                nombre = form.empresa.nombre,
                telefono = form.empresa.telefono,
                email = form.empresa.email,
                direccion = form.empresa.direccion,
                fechaIngreso = DateTime.Now.ToShortDateString(),
                rubroEmpresa = form.empresa.rubroEmpresa,
                comuna = new Comuna() { idComuna = form.empresa.comuna.idComuna }
            };

            return EmpresaFactory.Guardar(emp);
        }
    }
}
