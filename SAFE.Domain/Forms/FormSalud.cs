﻿using SAFE.Domain.Abstract;
using SAFE.Domain.Constrols;
using SAFE.Domain.Entities;
using SAFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SAFE.Domain.Forms
{
    public class FormSalud
    {
        public Empresa empresa { get; set; }
        public Medico medico { get; set; }
        public IList<AgendaMedica> agendasMedicasPrevias { get; set; }
        public IList<AtencionMedica> atencionesMedicasPrevias { get; set; }
        public IList<Trabajador> trabajadoresFichaMedica { get; set; }

        public AgendaMedica agendaMedica { get; set; }
        public AtencionMedica atencionMedica { get; set; }
        public IList<Medico> _listaMedicos { get; set; }
        public IList<Trabajador> _listaTrabajadores { get; set; }
        public Trabajador trabajador { get; set; }
        public FichaMedica nuevaFichaMedica { get; set; }
    }

    public class FormSaludFactory
    {
        public static FormSalud Portada(int _rutEmpresa)
        {
            FormSalud _form = new FormSalud();
            _form.empresa = EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == _rutEmpresa);
            _form.agendasMedicasPrevias = AgendaMedicaFactory.listaAgendaMedicaEmpresa(_form.empresa.rut);
            _form.atencionesMedicasPrevias = AtencionMedicaEmpresaControl.obtenerAtencionesMedicasEmpresa((int)_form.empresa.idEmpresa);

            return _form;
        }

        public static object leerAgendaMedica(long idAgendaMedica)
        {
            FormSalud _form = new FormSalud();
            _form.agendaMedica = AgendaMedicaFactory.obtenerPorID(idAgendaMedica);
            _form.empresa = EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.idEmpresa == _form.agendaMedica.idEmpresa);

            CargaDDL(_form);
            return _form;
        }

        public static FormSalud nuevaAgendaMedica(int _rutEmpresa)
        {
            FormSalud _form = new FormSalud();
            _form.empresa = EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == _rutEmpresa);


            CargaDDL(_form);
            return _form;
        }

        public static bool GuardarAgendaMedica(FormSalud form, string urlConfirmarvisitaMedica)
        {
            FormSalud _form = new FormSalud();

            _form.agendaMedica = new AgendaMedica();

            _form.agendaMedica.idAgendaMedica = form.agendaMedica.idAgendaMedica;
            _form.agendaMedica.fechaAgenda = form.agendaMedica.fechaAgenda;
            _form.agendaMedica.medico = DoctorControl.listaDoctores().FirstOrDefault(x => x.idMedico == form.agendaMedica.medico.idMedico);
            _form.agendaMedica.confirmado = form.agendaMedica.confirmado;
            _form.agendaMedica.idEmpresa = (int)form.empresa.idEmpresa;


            return AgendaMedicaFactory.Guardar(_form.agendaMedica, urlConfirmarvisitaMedica);
        }

        public static object nuevaVisitaMedica(long rutEmpresa, long idAgendaMedica)
        {
            FormSalud form = new FormSalud();

            form.empresa = FormServiciosGeneralesFactory.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == rutEmpresa);
            form.agendaMedica = AgendaMedicaFactory.obtenerPorID(idAgendaMedica);

            CargaDDL(form);
            return form;
        }

        public static object nuevaFichaMedica(long idAgendaMedica)
        {
            var form = new FormSalud { agendaMedica = AgendaMedicaFactory.obtenerPorID(idAgendaMedica) };

            form.atencionMedica = AtencionMedicaFactory.Obtener((int)form.agendaMedica.idEmpresa, (long)form.agendaMedica.idAgendaMedica);
            form.empresa = FormServiciosGeneralesFactory.obtenerEmpresas().FirstOrDefault(x => x.idEmpresa == form.agendaMedica.idEmpresa);
            form.trabajadoresFichaMedica = TrabajadorControl.fichasMedicasTrabajadoresConExamen((int)form.empresa.idEmpresa);


            CargaDDL(form);
            return form;
        }

        public static object leerVisitaMedica(long idVisitaMedica)
        {
            throw new NotImplementedException();
        }

        public static void CargaDDL(FormSalud _form)
        {
            _form._listaMedicos = DoctorControl.listaDoctores();
            if (_form.empresa?.idEmpresa != null)
                _form._listaTrabajadores = TrabajadorControl.listaTrabajadoresEmpresas((int)_form.empresa.idEmpresa);

        }

        public static bool GuardarAtencionMedica(FormSalud form)
        {
            var _form = new FormSalud
            {
                atencionMedica = new AtencionMedica
                {
                    idAtencionMedica = form.atencionMedica.idAtencionMedica,
                    fechaAtencion = form.atencionMedica.fechaAtencion,
                    recomendaciones = form.atencionMedica.recomendaciones,
                    idAgendaMedica = form.atencionMedica.idAgendaMedica ?? form.agendaMedica.idAgendaMedica,
                    idEmpresa = form.empresa.idEmpresa ?? form.agendaMedica.idEmpresa
                }
            };

            var _exito = AtencionMedicaFactory.Guardar(_form.atencionMedica);

            return _exito;
        }

        public static AgendaMedica ConfirmarAgendaMedica(int rutEmpresa, long idAgendaMedica)
        {
            var agenda = AgendaMedicaControl.agendaMedicaEmpresa(rutEmpresa).FirstOrDefault(x => x.idAgendaMedica == idAgendaMedica);
            agenda.confirmado = true;
            var exito = AgendaMedicaFactory.Guardar(agenda, string.Empty);
            return agenda;
        }

        public static bool GuardarFichaMedica(FormSalud form)
        {
            return fichaMedicaFactory.Guardar(form.nuevaFichaMedica);
        }

        public static FormSalud leerPerfilMedicoTrabajador(int idEmpresa, long rutTrabajador, long idAtencionMedica)
        {
            FormSalud _form = new FormSalud();

            _form.trabajador = TrabajadorControl.listaTrabajadoresEmpresas(idEmpresa).FirstOrDefault(x => x.rut.rut == rutTrabajador);
            _form.trabajador = TrabajadorControl.fichasMedicasTrabajadoresConExamen(idEmpresa).FirstOrDefault(x => x.idTrabajador == (long)_form.trabajador.idTrabajador);
            _form.empresa = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == _form.trabajador.idEmpresa);
            _form.atencionMedica = AtencionMedicaEmpresaControl.obtenerAtencionesMedicasEmpresa(idEmpresa).Single(x => x.idAtencionMedica == idAtencionMedica);

            return _form;
        }

        public static object leerPerfilMedicoTrabajador(long rutTrabajador)
        {
            throw new NotImplementedException();
        }

        public static object leerPerfilMedicoTrabajador(long idAgendaMedica, long rutTrabajador)
        {
            throw new NotImplementedException();
        }
    }

    public class FormSaludValidate : IFormValidate
    {


        public List<Observacion> ValidarFormAgendaMedica(object form)
        {
            List<Observacion> _Observaciones = new List<Observacion>();
            FormSalud _form = form as FormSalud;

            if (_form.agendaMedica == null)
                _Observaciones.Add(new Observacion { detalle = "Debe completar la Agenda" });

            if (string.IsNullOrEmpty(_form.agendaMedica.fechaAgenda))
                _Observaciones.Add(new Observacion { detalle = "Debe seleccionar una fecha" });

            if (_form.agendaMedica?.medico?.idMedico == null)
                _Observaciones.Add(new Observacion { detalle = "Debe seleccionar un médico" });

            return _Observaciones;
        }

        public List<Observacion> ValidarFormAtencionMedica(object form)
        {
            List<Observacion> _Observaciones = new List<Observacion>();
            FormSalud _form = form as FormSalud;

            if (_form.atencionMedica == null)
                _Observaciones.Add(new Observacion { detalle = "Debe completar la atención médica" });

            if (string.IsNullOrEmpty(_form.atencionMedica?.fechaAtencion))
                _Observaciones.Add(new Observacion { detalle = "Debe seleccionar una fecha para la atención médica" });

            if (string.IsNullOrEmpty(_form.atencionMedica?.recomendaciones))
                _Observaciones.Add(new Observacion { detalle = "Debe seleccionar una fecha para la atención médica" });

            return _Observaciones;
        }

        public List<Observacion> ValidarForm(object form)
        {
            throw new NotImplementedException();
        }

        public List<Observacion> ValidarFormFichaMedica(object form)
        {
            List<Observacion> _Observaciones = new List<Observacion>();
            FormSalud _form = form as FormSalud;

            return _Observaciones;
        }
    }
}