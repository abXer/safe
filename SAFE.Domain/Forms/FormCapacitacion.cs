﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Abstract;
using SAFE.Domain.Constrols;
using SAFE.Domain.Entities;
using SAFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Forms
{
    public class FormCapacitacion
    {
        public Empresa empresa { get; set; }
        public Expositor expositor { get; set; }
        public Capacitacion capacitacion { get; set; }
        public IList<Capacitacion> capacitacionesPrevias { get; set; }
        public Trabajador asistenteCapacitacion { get; set; }
        public IList<AsistenteCapacitacion> asistentesCapacitacion { get; set; }
        public IList<Expositor> _listaExpositores { get; set; }
        public IList<Supervisor> _listaSupervisores { get; set; }
        public IList<Trabajador> _listaTrabajadoresEmpresa { get; set; }
    }

    public class FormCapacitacionFactory
    {
        public static FormCapacitacion nuevaCapacitacion(int rutEmpresa)
        {
            FormCapacitacion _form = new FormCapacitacion();
            _form.empresa = FormServiciosGeneralesFactory.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == rutEmpresa);
            _form.capacitacionesPrevias = CapacitacionControl.listaCapacitacionesEmpresa(rutEmpresa);
            _form._listaTrabajadoresEmpresa = TrabajadorControl.listaTrabajadoresEmpresas((int)_form.empresa.idEmpresa);


            CargaDDL(_form);
            return _form;
        }

        public static FormCapacitacion leerCapacitacion(int rutEmpresa, long idCapacitacion)
        {
            FormCapacitacion _form = new FormCapacitacion();

            _form.empresa = FormServiciosGeneralesFactory.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == rutEmpresa);
            _form.capacitacion = CapacitacionControl.listaCapacitacionesEmpresa(_form.empresa.rut.rut).Single(x => x.idCapacitacion == idCapacitacion);
            _form.asistentesCapacitacion = AsistenteCapacitacionFactory.listaAsistentesCapacitacion(idCapacitacion, FormServiciosGeneralesFactory.obtenerTrabajadoresEmpresa(_form.capacitacion.idEmpresa));
            _form._listaTrabajadoresEmpresa = TrabajadorControl.listaTrabajadoresEmpresas((int)_form.empresa.idEmpresa);

            CargaDDL(_form);
            return _form;
        }

        public static bool agregarAsistenciaCapacitacion(long idCapacitacion, int rutTrabajador, int rutEmpresa)
        {
            return CapacitacionFactory.AgregarAsistenciaCapacitacion(new AsistenteCapacitacion()
            {
                idCapacitacion = idCapacitacion,
                trabajador = TrabajadorControl.listaTrabajadoresEmpresas(rutEmpresa).Single(x => x.rut.rut == rutTrabajador)
            });
        }

        public static bool Guardar(FormCapacitacion form)
        {
            FormCapacitacion _form = new FormCapacitacion();
            _form.capacitacion = new Capacitacion();

            _form.capacitacion.idCapacitacion = form.capacitacion.idCapacitacion;
            _form.capacitacion.asunto = form.capacitacion.asunto;
            _form.capacitacion.fechaInicio = form.capacitacion.fechaInicio;
            _form.capacitacion.fechaTermino = form.capacitacion.fechaTermino;
            _form.capacitacion.cupoMinimo = form.capacitacion.cupoMinimo;

            if (form.capacitacion.supervisor?.idSupervisor != null)
                _form.capacitacion.supervisor = SupervisorControl.listaExpositores().Single(x => x.idSupervisor == form.capacitacion.supervisor.idSupervisor);

            _form.capacitacion.expositor = ExpositorControl.listaExpositores().Single(x => x.idExpositor == form.capacitacion.expositor.idExpositor);
            _form.capacitacion.idEmpresa = (int)form.empresa.idEmpresa;
            _form.capacitacion.cantidadAsistentes = form.asistentesCapacitacion?.Count ?? 0;

            var exito = CapacitacionFactory.Guardar(_form.capacitacion);

            if (exito)
                CapacitacionControl.actualizaCapacitacionesEmpresa((int)form.empresa.idEmpresa);

            return exito;
        }

        public static void CargaDDL(FormCapacitacion form)
        {
            form._listaExpositores = FormServiciosGeneralesFactory.obtenerExpositores();
            form._listaSupervisores = FormServiciosGeneralesFactory.obtenerSupervisores();
        }
    }

    public class FormCapacitacionValidate : IFormValidate
    {
        public List<Observacion> ValidarForm(object form)
        {
            List<Observacion> _Observaciones = new List<Observacion>();
            FormCapacitacion _form = form as FormCapacitacion;

            if (_form.capacitacion == null)
                _Observaciones.Add(new Observacion { detalle = "Debe completar la capacitación" });

            if (_form.capacitacion.asunto == null)
                _Observaciones.Add(new Observacion { detalle = "Debe completar el asunto" });

            if (_form.capacitacion.cupoMinimo == null || _form.capacitacion.cupoMinimo == 0)
            {
                _Observaciones.Add(new Observacion { detalle = "Debe completar el cupo mínimo" });
            }

            if (_form.capacitacion.expositor?.idExpositor == null)
                _Observaciones.Add(new Observacion { detalle = "Debe seleccionar un Expositor" });

            if (_form.capacitacion.supervisor?.idSupervisor == null)
                _Observaciones.Add(new Observacion { detalle = "Debe seleccionar un Supervisor" });

            if (string.IsNullOrEmpty(_form.capacitacion.fechaInicio))
                _Observaciones.Add(new Observacion { detalle = "Debe elegir una fecha de inicio" });

            if (string.IsNullOrEmpty(_form.capacitacion.fechaTermino))
                _Observaciones.Add(new Observacion { detalle = "Debe elegir una fecha de termino" });

            return _Observaciones;

        }
    }
}
