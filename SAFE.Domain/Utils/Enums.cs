﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SAFE.Domain.Utils
{
    public class Enums
    {
        public static class DATOS_SAFE
        {
            public const int rut = 99123456
            ;
        }

        public static class tipoUsuario
        {
            public const string
                PERSONA = "Persona",
                EMPRESA = "Empresa"
            ;
        }
        public class tipoEvaluacion
        {
            public const int
                TRABAJADORES = 2,
                INSTALACIONES = 1
            ;
        }
        public class ETAPAEVALUACION
        {
            public static Dictionary<int, string> listaEtapasEvaluacion = new Dictionary<int, string>() {
                { 1, "Técnico"},
                { 2, "Ingeniero" },
                { 3, "Supervisor" },
                { 4, "Completado" }
            };
        }

        public class TIPOPERSONA
        {
            public static Dictionary<int, string> listaTipoPersona = new Dictionary<int, string>() {
                { 1, "Administrador"},
                { 2, "Expositor"},
                { 3, "Ingeniero" },
                { 4, "Supervisor" },
                { 5, "Técnico" },
                { 6, "Trabajador" }
            };
        }

        public static class EMAIL
        {
            public const string
                REMITENTE = "safe@gmail.com",
                NOMBRE_REMITENTE = "SAFE",
                HOST_SERVER = "smtp.sendgrid.net",
                API_KEY = "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",
                destinatariosEquipoCC = "en.venegas@alumnos.duoc.cl;Enrique Venegas Correo Duoc|abxer2010@gmail.com; Enrique Venegas Gmail"
                ;

            public const int
                HOST_PUERTO = 465
                ;
        }

        public static class PLANTILLAS_EMAIL
        {
            public const string
                CORREO_CONFIRMACION_AGENDA_MEDICA = "/Content/PlantillasCorreos/CorreoConfirmacionAgendaMedica.html",
                CORREO_CONFIRMACION_AGENDA_MEDICA_ASUNTO = "Se ha agendado una visita a su nombre"

            ;
        }

        public static class PLANTILLAS_REPORTES
        {
            public const string
                CSS = "/Content/PlantillasReportes/style.css",
                PRUEBA = "/Content/PlantillasReportes/index.html",
                REPORTE_EVALUACION_TERRENO = "/Content/PlantillasReportes/maqueta.html"
                ;
        }
    }
}
