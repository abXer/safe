﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.SqlServer.Server;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace SAFE.Domain.Utils
{

    public static class Helper
    {
        private const string password = "A";

        public static string Encrypt(object _plainText)
        {
            string plainText = _plainText.ToString();

            if (plainText == null)
            {
                return null;
            }

            var bytesToBeEncrypted = Encoding.UTF8.GetBytes(plainText);
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesEncrypted = Helper.Encrypt(bytesToBeEncrypted, passwordBytes);

            return Convert.ToBase64String(bytesEncrypted);
        }

        public static string Decrypt(object _encryptedText)
        {
            string encryptedText = _encryptedText.ToString();

            if (encryptedText == null)
            {
                return null;
            }

            var bytesToBeDecrypted = Convert.FromBase64String(encryptedText);
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            var bytesDecrypted = Helper.Decrypt(bytesToBeDecrypted, passwordBytes);

            return Encoding.UTF8.GetString(bytesDecrypted);
        }

        private static byte[] Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }

                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        private static byte[] Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);
                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }

                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public static void EnviarMail(string asunto, string destinatario, string nombreDestinatario, string destinatariocc, string mensaje)
        {

            var apiKey = Domain.Utils.Enums.EMAIL.API_KEY;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(Domain.Utils.Enums.EMAIL.REMITENTE, Domain.Utils.Enums.EMAIL.NOMBRE_REMITENTE);
            var subject = asunto;
            var to = new EmailAddress(destinatario, nombreDestinatario);

            var htmlContent = mensaje;
            var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlContent);

            if (!string.IsNullOrEmpty(destinatariocc))
            {
                var CCs = new List<EmailAddress>();

                foreach (var email in destinatariocc.Split('|'))
                {
                    string[] emailNombre = email.Split(';');
                    CCs.Add(new EmailAddress(emailNombre[0].Trim(), emailNombre[1].Trim()));
                }

                msg.AddCcs(CCs);
            }

            client.SendEmailAsync(msg);
        }

        public static byte[] obtenerPDFdesdeHTML(string html, bool usaCSS = false)
        {
            byte[] pdf;

            var css = usaCSS ? leerTextoArchivo(Domain.Utils.Enums.PLANTILLAS_REPORTES.CSS) : string.Empty;


            html = html.Replace("<br>", "<br />");
            html = html.Replace("<p><br /></p>", "<br/>");

            using (var memoryStream = new MemoryStream())
            {
                var document = new Document(PageSize.A4, 50, 50, 60, 60);
                var writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                using (var cssMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(css)))
                {
                    using (var htmlMemoryStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html)))
                    {
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, htmlMemoryStream, cssMemoryStream);
                    }
                }
                document.Close();

                pdf = memoryStream.ToArray();
            }

            return pdf;
        }

        public static string leerTextoArchivo(string rutaHtml)
        {
            return System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~" + rutaHtml));
        }
    }
}

