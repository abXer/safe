﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;

namespace SAFE.Domain.Entities
{
    public class Administrador : Persona
    {
        public int id { get; set; }
    }

    public class AdministradorFactory
    {
        public static IList<Administrador> listadoAdministradores()
        {
            var lstAdministradores = new List<Administrador>();

            foreach (var admin in AdministradorDAO.listaAdministradores())
            {
                lstAdministradores.Add(new Administrador()
                {
                    id = (int)admin.id,
                    apellidoMaterno = admin.apemat,
                    apellidoPaterno = admin.apepat,
                    contacto = new Contacto() { celular = admin.celular, email = admin.email },
                    direccion = admin.direccion,
                    fechaIngreso = admin.fechaIngreso,
                    fechaNacimiento = admin.fechaNacimiento,
                    nombre = admin.nombre,
                    rut = new Rut() { rut = admin.rut }
                });
            }

            return lstAdministradores;
        }
    }
}
