﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class AsistenteCapacitacion
    {
        public long idCapacitacion { get; set; }
        public string fecha { get; set; }
        public string registroAsistencia { get; set; }
        public Trabajador trabajador { get; set; }
    }

    public class AsistenteCapacitacionFactory
    {
        public static IList<AsistenteCapacitacion> listaAsistentesCapacitacion(long idCapacitacion, IList<Trabajador> trabajadoresEmpresa)
        {
            IList<AsistenteCapacitacion> _asistentes = new List<AsistenteCapacitacion>();

            foreach (AsistenciaCapacitacionModel _asis in CapacitacionDAO.listaAsistentesCapacitacion(idCapacitacion))
            {
                _asistentes.Add(new AsistenteCapacitacion()
                {
                    idCapacitacion = _asis.id,
                    fecha = _asis.fecha,
                    registroAsistencia = _asis.registro,
                    trabajador = trabajadoresEmpresa.Where(x => x.idTrabajador == _asis.idTrabajador).FirstOrDefault<Trabajador>()
                });
            }

            return _asistentes;
        }
    }
}
