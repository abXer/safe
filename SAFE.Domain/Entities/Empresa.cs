﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Constrols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Empresa
    {
        public int? idEmpresa { get; set; }
        public Rut rut { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string fechaIngreso { get; set; }
        public string rubroEmpresa { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public Comuna comuna { get; set; }
    }

    public class EmpresaFactory
    {


        internal static Empresa Obtener(Rut rutEmpresa)
        {
            return EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == rutEmpresa.rut);
        }

        internal static IList<Empresa> ListadoEmpresas()
        {
            return (from e in EmpresaDAO.listadoEmpresas()?.OrderBy(x => x.empresa.id)
                    select new Empresa()
                    {
                        idEmpresa = e.empresa.id,
                        rut = new Rut() { rut = e.empresa.rut },
                        nombre = e.empresa.nombre,
                        direccion = e.empresa.direccion,
                        comuna = new Comuna() { idComuna = e.comuna.id, comuna = e.comuna.nombre, idRegion = e.region.id },
                        fechaIngreso = e.empresa.fechaIngreso,
                        rubroEmpresa = e.empresa.rubro,
                        telefono = e.empresa.telefono,
                        email = e.empresa.email
                    }).ToList();
        }

        internal static IList<Instalacion> ListadoInstalacionesEmpresa(Rut rutEmpresa)
        {
            IList<Instalacion> instalaciones = new List<Instalacion>();

            foreach (InstalacionEmpresaModel _iE in EmpresaDAO.listaInstalacionesEmpresa(rutEmpresa.rut).OrderBy(x => x.idInstalacion))
            {
                if (_iE.idInstalacion != null)
                    instalaciones.Add(new Instalacion()
                    {
                        idInstalacionEmpresa = (long)_iE.idInstalacion,
                        idEmpresa = _iE.idEmpresa,
                        comuna = new Comuna()
                        { idComuna = _iE.idComuna, comuna = _iE.nombreComuna, idRegion = _iE.idRegion },
                        contacto = new Contacto() { telefono = _iE.telefonoInstalacion },
                        direccion = _iE.direccionIntalacion
                    });
            }

            return instalaciones;
        }

        internal static bool Guardar(Empresa empresa)
        {
            EmpresaModel _emp = new EmpresaModel();

            _emp.id = empresa.idEmpresa;
            _emp.rut = empresa.rut.rut;
            _emp.nombre = empresa.nombre;
            _emp.telefono = empresa.telefono;
            _emp.direccion = empresa.direccion;
            _emp.fechaIngreso = empresa.fechaIngreso;
            _emp.rubro = empresa.rubroEmpresa;
            _emp.idComuna = empresa.comuna.idComuna;
            _emp.email = empresa.email;
            _emp.estado = "1";
            var exito = EmpresaDAO.GuardarEmpresa(_emp);

            if (exito)
                EmpresaControl.ActualizarListadoEmpresas();

            return exito;
        }
    }
}
