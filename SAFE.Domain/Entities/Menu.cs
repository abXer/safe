﻿using System.Collections.Generic;
using SAFE.Dao.Dao;

namespace SAFE.Domain.Entities
{
    public class Menu
    {
        public int idMenu { get; set; }
        public string descripcion { get; set; }
    }

    public class MenuFactory
    {
        public static IList<Menu> listaMenus()
        {
            var menus = new List<Menu>();

            foreach (var _m in UsuarioDAO.listaMenus())
            {
                menus.Add(new Menu() { idMenu = _m.id, descripcion = _m.descripcion });
            }

            return menus;
        }
    }
}