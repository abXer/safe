﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Contacto
    {
        public string email { get; set; }
        public string celular { get; set; }
        public string telefono { get; set; }
    }
}
