﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Constrols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class AgendaMedica
    {
        public long? idAgendaMedica { get; set; }
        public string fechaAgenda { get; set; }
        public bool confirmado { get; set; }
        public Medico medico { get; set; }
        public long idEmpresa { get; set; }
    }

    public class AgendaMedicaFactory
    {

        public static IList<AgendaMedica> listaAgendaMedicaEmpresa(Rut rutEmpresa)
        {
            IList<AgendaMedica> _lst = new List<AgendaMedica>();

            Empresa _emp = EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == rutEmpresa.rut);

            foreach (AgendaMedicaModel _ag in SaludDAO.listaAgendaMedicaEmpresa((int)_emp.idEmpresa))
            {
                _lst.Add(new AgendaMedica()
                {
                    idAgendaMedica = _ag.id,
                    fechaAgenda = _ag.fechaDisponible,
                    confirmado = _ag.confirmado == "1" ? true : false,
                    medico = DoctorControl.listaDoctores().FirstOrDefault(x => x.idMedico == _ag.idMedico),
                    idEmpresa = _ag.idEmpresa

                });
            }
            return _lst;
        }
        public static IList<AgendaMedica> listaAgendaMedicaDoctor(Rut rutDoctor)
        {
            IList<AgendaMedica> _lst = new List<AgendaMedica>();

            foreach (AgendaMedicaModel _ag in SaludDAO.listaAgendaMedicaDoctor(rutDoctor.rut))
            {
                _lst.Add(new AgendaMedica()
                {
                    idAgendaMedica = _ag.id,
                    fechaAgenda = _ag.fechaDisponible,
                    confirmado = _ag.confirmado == "1" ? true : false,
                    medico = DoctorControl.listaDoctores().Single(x => x.idMedico == _ag.idMedico),
                    idEmpresa = _ag.idEmpresa
                });
            }
            return _lst;
        }

        internal static bool Guardar(AgendaMedica agendaMedica, string urlConfirmarvisita)
        {
            AgendaMedicaModel _a = new AgendaMedicaModel();

            _a.id = agendaMedica.idAgendaMedica;
            _a.fechaDisponible = agendaMedica.fechaAgenda;
            _a.idMedico = (long)agendaMedica.medico.idMedico;
            _a.confirmado = "0";
            _a.idEmpresa = agendaMedica.idEmpresa;

            var exito = SaludDAO.guardarAgendaMedica(_a);

            if (exito && !string.IsNullOrEmpty(urlConfirmarvisita))
            {
                AgendaMedicaControl.actualizaAgendasMedicasEmpresa(agendaMedica.idEmpresa);
                var emp = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == agendaMedica.idEmpresa);
                var am = AgendaMedicaControl.agendaMedicaEmpresa(emp.rut.rut).OrderBy(x => x.idAgendaMedica).Last();
                Task.Run(() => { EnviarCorreoConfirmacionAgendaMedica(agendaMedica, urlConfirmarvisita.Replace("{idUltimaAgendaMedica}", Utils.Helper.Encrypt(am.idAgendaMedica))); });
            }


            return exito;
        }

        internal static void EnviarCorreoConfirmacionAgendaMedica(AgendaMedica agenda, string urlConfirmarVisita)
        {
            var htmlcorreo = Domain.Utils.Helper.leerTextoArchivo(Utils.Enums.PLANTILLAS_EMAIL.CORREO_CONFIRMACION_AGENDA_MEDICA);
            string nombreDoctor = $"{agenda.medico.nombre} {agenda.medico.apellidoPaterno} {agenda.medico.apellidoMaterno}";

            var empresaCliente = EmpresaControl.obtenerEmpresas().Single(x => x.idEmpresa == agenda.idEmpresa);
            var empresaSAFE = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == Domain.Utils.Enums.DATOS_SAFE.rut);

            string asunto = Utils.Enums.PLANTILLAS_EMAIL.CORREO_CONFIRMACION_AGENDA_MEDICA_ASUNTO;
            string destinatario = agenda.medico.contacto.email;
            string nombreDestinatario = nombreDoctor;
            string destinatariocc = Utils.Enums.EMAIL.destinatariosEquipoCC;
            string mensaje = htmlcorreo
                        .Replace("{nombreDoctor}", nombreDoctor)
                        .Replace("{especialidadDoctor}", agenda.medico.especialidad)
                        .Replace("{celularDoctor}", agenda.medico.contacto.celular)
                        .Replace("{telefonoDoctor}", agenda.medico.contacto.celular)

                        .Replace("{rutEmpresa}", empresaCliente.rut.rutCompleto)
                        .Replace("{nombreEmpresa}", empresaCliente.nombre)
                        .Replace("{telefonoEmpresa}", empresaCliente.telefono)
                        .Replace("{direccionEmpresa}", empresaCliente.direccion)


                        .Replace("{rutSAFE}", empresaSAFE.rut.rutCompleto)
                        .Replace("{nombreSAFE}", empresaSAFE.nombre)
                        .Replace("{telefonoSAFE}", empresaSAFE.telefono)
                        .Replace("{direccionSAFE}", empresaSAFE.direccion)

                        .Replace("{fechavisita}", agenda.fechaAgenda)

                        .Replace("{urlConfirmarVisitaMedica}", urlConfirmarVisita)
            ;

            Utils.Helper.EnviarMail(asunto, destinatario, nombreDestinatario, destinatariocc, mensaje);
        }

        internal static AgendaMedica obtenerPorID(long idAgendaMedica)
        {
            AgendaMedica _ag = new AgendaMedica();

            AgendaMedicaModel ag = SaludDAO.ObtenerAgendaMedicaPorID(idAgendaMedica);

            _ag.idAgendaMedica = ag.id;
            _ag.fechaAgenda = ag.fechaDisponible;
            _ag.confirmado = ag.confirmado == "1" ? true : false;
            _ag.medico = DoctorControl.listaDoctores().Single(x => x.idMedico == ag.idMedico);
            _ag.idEmpresa = ag.idEmpresa;

            return _ag;
        }
    }
}
