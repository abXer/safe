﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;

using System.Collections.Generic;

namespace SAFE.Domain.Entities
{
    public class Tecnico : Persona
    {
        public int idTecnico { get; set; }
    }

    public class TecnicoFactory
    {
        public static IList<Tecnico> listaTecnicos()
        {
            IList<Tecnico> _listaTecnicos = new List<Tecnico>();

            foreach (TecnicoModel _t in TecnicoDAO.listaTecnicos())
            {
                _listaTecnicos.Add(new Tecnico()
                {
                    rut = new Rut() { rut = _t.rut },
                    nombre = _t.nombre,
                    apellidoMaterno = _t.apemat,
                    apellidoPaterno = _t.apepat,
                    contacto = new Contacto() { email = _t.email, celular = _t.celular },
                    direccion = _t.direccion,
                    fechaIngreso = _t.fechaIngreso,
                    fechaNacimiento = _t.fechaNacimiento,
                    idTecnico = _t.idTecnico
                });
            }

            return _listaTecnicos;
        }
    }
}