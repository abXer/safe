﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;

namespace SAFE.Domain.Entities
{
    public class Ingeniero : Persona
    {
        public long? idIngeniero { get; set; }
    }

    public class IngenieroFactory
    {
        internal static IList<Ingeniero> ObtenerIngenieros()
        {
            IList<Ingeniero> _lst = new List<Ingeniero>();

            foreach (IngenieroModel ing in IngenieroDAO.listadoIngenieros())
            {
                _lst.Add(new Ingeniero()
                {
                    idIngeniero = ing.idIngeniero,
                    apellidoMaterno = ing.apemat,
                    apellidoPaterno = ing.apepat,
                    contacto = new Contacto() { celular = ing.celular, email = ing.email },
                    direccion = ing.direccion,
                    fechaIngreso = ing.fechaIngreso,
                    fechaNacimiento = ing.fechaNacimiento,
                    nombre = ing.nombre,
                    rut = new Rut() { rut = ing.rut }
                });
            }

            return _lst;
        }
    }
}