﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Domain.Constrols;

namespace SAFE.Domain.Entities
{
    public class Perfil
    {
        public int idPerfil { get; set; }
        public string descripcion { get; set; }
        public List<Menu> menu { get; set; }
    }

    public class PerfilFactory
    {

        internal static IList<Perfil> listaPerfiles()
        {
            var perfiles = new List<Perfil>();
            var _perfiles = UsuarioDAO.listaPerfiles();

            foreach (var _p in _perfiles)
            {
                perfiles.Add(new Perfil()
                {
                    idPerfil = _p.id,
                    descripcion = _p.descripcion,
                    menu = UsuarioControl.listaMenus(_p.id).ToList()
                });
            }

            return perfiles;
        }

        public static Dictionary<int, int> listaAsociacionMenus()
        {
            return UsuarioDAO.asociacionPerfilMenus().ToDictionary(x => x.idPerfil, x => x.idMenu);
        }
    }
}
