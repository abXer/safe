﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Dao.Models;

namespace SAFE.Domain.Entities
{
    public class Persona
    {
        public Rut rut { get; set; }
        public string nombre { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public Contacto contacto { get; set; }
        public string fechaIngreso { get; set; }
        public string fechaNacimiento { get; set; }
        public string direccion { get; set; }

        public Persona() { }
    }

    public class PersonaFactory
    {
        internal static Persona obtenerPersona(Rut rut)
        {
            var per = new Persona();

            var _t = PersonaDAO.ObtenerRut(rut.rut);

            per.rut = new Rut() { rut = _t.rut };
            per.nombre = _t.nombre;
            per.apellidoMaterno = _t.apemat;
            per.apellidoPaterno = _t.apepat;
            per.contacto = new Contacto() { email = _t.email, celular = _t.celular };
            per.direccion = _t.direccion;
            per.fechaIngreso = _t.fechaIngreso;
            per.fechaNacimiento = _t.fechaNacimiento;

            return per;
        }

        internal static bool GuardarPersona(Persona per, int idTipo)
        {
            /* TIPOS
             *  1	ADMINISTRADOR
                2	EXPOSITOR
                3	INGENIERO
                4	SUPERVISOR
                5	TECNICO
                6	TRABAJADOR
                7	MEDICO
                8	EMPRESA
             */


            var persona = new PersonaModel();

            persona.rut = per.rut.rut;
            persona.nombre = per.nombre;
            persona.apepat = per.apellidoPaterno;
            persona.apemat = per.apellidoMaterno;
            persona.email = per.contacto.email;
            persona.celular = per.contacto.celular;
            persona.direccion = per.direccion;
            persona.fechaIngreso = DateTime.Now.ToString();
            persona.fechaNacimiento = per.fechaNacimiento;
            persona.idTipo = idTipo;

            return PersonaDAO.GuardarPersona(persona);
        }
    }
}
