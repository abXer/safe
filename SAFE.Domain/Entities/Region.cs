﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Region
    {
        [Display(Name = "Región")]
        [Required(ErrorMessage = "Campo requerido")]
        public int idRegion { get; set; }
        public string region { get; set; }
        public Region() { }
    }

    internal class RegionFactory
    {
        internal static IList<Region> listadoRegiones()
        {
            IList<Region> _lst = new List<Region>();

            foreach (RegionModel _r in RegionDAO.obtenerRegiones())
            {
                _lst.Add(new Region
                {
                    idRegion = _r.id,
                    region = _r.nombre
                });
            }

            return _lst;

        }
    }
}
