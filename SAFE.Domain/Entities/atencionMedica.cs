﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Constrols;

namespace SAFE.Domain.Entities
{
    public class AtencionMedica
    {
        public long? idAtencionMedica { get; set; }
        public string fechaAtencion { get; set; }
        public string recomendaciones { get; set; }
        public long? idAgendaMedica { get; set; }
        public long idEmpresa { get; set; }
    }


    public class AtencionMedicaFactory
    {


        public static AtencionMedica Obtener(int idEmpresa, long idAgendaMedica)
        {
            return AtencionMedicaEmpresaControl.obtenerAtencionesMedicasEmpresa(idEmpresa).FirstOrDefault(x => x.idAgendaMedica == idAgendaMedica);
        }
        public static IList<AtencionMedica> AtencionesMedicasEmpresa(long idEmpresa)
        {
            IList<AtencionMedica> _lst = new List<AtencionMedica>();

            foreach (AtencionMedicaModel _am in SaludDAO.listaAtencionesMedicas(idEmpresa))
            {
                _lst.Add(new AtencionMedica()
                {
                    idAtencionMedica = _am.id,
                    fechaAtencion = _am.fechaAtencion,
                    recomendaciones = _am.recomendacionesAM,
                    idEmpresa = _am.idEmpresa,
                    idAgendaMedica = _am.idAgendaMedico
                });
            }

            return _lst;
        }

        public static bool Guardar(AtencionMedica atencionMedica)
        {
            AtencionMedicaModel _atencion = new AtencionMedicaModel();

            _atencion.id = atencionMedica.idAtencionMedica;
            _atencion.fechaAtencion = atencionMedica.fechaAtencion;
            _atencion.recomendacionesAM = atencionMedica.recomendaciones;
            _atencion.idAgendaMedico = (long)atencionMedica.idAgendaMedica;
            _atencion.idEmpresa = atencionMedica.idEmpresa;

            var _exito = SaludDAO.guardarAtencionMedica(_atencion);

            if (_exito)
                AtencionMedicaEmpresaControl.ActualizarAtencionesMedicasEmpresa((int)atencionMedica.idEmpresa);

            return _exito;
        }
    }
}
