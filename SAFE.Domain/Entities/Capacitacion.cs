﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Constrols;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Capacitacion
    {
        public long? idCapacitacion { get; set; }




        [Display(Name = "Asunto")]
        [Required(ErrorMessage = "Campo requerido")]
        public string asunto { get; set; }

        [Display(Name = "Fecha de inicio")]
        [Required(ErrorMessage = "Campo requerido")]
        public string fechaInicio { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [Display(Name = "Fecha de termino")]
        public string fechaTermino { get; set; }

        [Display(Name = "Cantidad de asistentes mínima")]
        public int cupoMinimo { get; set; }
        public Supervisor supervisor { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public Expositor expositor { get; set; }
        public long idEmpresa { get; set; }
        public int cantidadAsistentes { get; set; }
    }

    public class CapacitacionFactory
    {
        internal static IList<Capacitacion> listaCapacitaciones(int idEmpresa)
        {
            IList<Capacitacion> _capacitaciones = new List<Capacitacion>();

            foreach (CapacitacionModel _cM in CapacitacionDAO.listaCapacitacionesEmpresa(idEmpresa))
            {
                _capacitaciones.Add(new Capacitacion()
                {
                    idCapacitacion = _cM.id,
                    asunto = _cM.asunto,
                    fechaInicio = _cM.fechaInicio,
                    fechaTermino = _cM.fechaTermino,
                    cupoMinimo = _cM.cupoMinimo,
                    expositor = ExpositorControl.listaExpositores().FirstOrDefault(x => x.idExpositor == _cM.idExpositor),
                    idEmpresa = _cM.idEmpresa,
                    supervisor = SupervisorControl.listaExpositores().FirstOrDefault(x => x.idSupervisor == _cM.idSupervisor)
                });
            }

            return _capacitaciones;
        }

        internal static bool Guardar(Capacitacion capacitacion)
        {
            CapacitacionModel _cap = new CapacitacionModel();

            /*
             "id":1,
              "asunto":"esto es una prueba",
              "fechaInicio":"2011-07-14",
              "fechaTermino":"2011-07-14",
              "cupoMinimo":3,
              "idSupervisor":1,
              "idExpositor":1,
              "idEmpresa":1,
              "cantidadAsistencia":9
             */

            _cap.id = capacitacion.idCapacitacion;
            _cap.asunto = capacitacion.asunto;

            _cap.fechaInicio = capacitacion.fechaInicio; // DateTime.ParseExact(capacitacion.fechaInicio, "/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy'/'MM'/'dd");
            //var a = 
            _cap.fechaTermino = capacitacion.fechaTermino;
            _cap.cupoMinimo = capacitacion.cupoMinimo;
            _cap.idSupervisor = capacitacion.supervisor?.idSupervisor;
            _cap.idExpositor = capacitacion.expositor.idExpositor;
            _cap.idEmpresa = capacitacion.idEmpresa;
            _cap.cantidadAsistencia = capacitacion.cantidadAsistentes;

            return CapacitacionDAO.Guardar(_cap);
        }

        internal static bool AgregarAsistenciaCapacitacion(AsistenteCapacitacion asistenteCapacitacion)
        {
            AsistenciaCapacitacionModel _asi = new AsistenciaCapacitacionModel();

            _asi.id = asistenteCapacitacion.idCapacitacion;
            _asi.fecha = DateTime.Now.ToShortDateString();
            _asi.registro = "1";
            _asi.idTrabajador = asistenteCapacitacion.trabajador.idTrabajador;


            return CapacitacionDAO.AgregarAsistenciaCapacitacion(_asi);
        }
    }
}