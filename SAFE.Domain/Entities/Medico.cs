﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Medico : Persona
    {
        public long? idMedico { get; set; }
        public string especialidad { get; set; }
    }

    public class MedicoFactory
    {
        internal static IList<Medico> listaMedicos()
        {
            IList<Medico> _lst = new List<Medico>();

            foreach (MedicoModel _m in SaludDAO.listaMedicos())
            {
                _lst.Add(new Medico()
                {
                    rut = new Rut() { rut = _m.rut },
                    nombre = _m.nombre,
                    apellidoMaterno = _m.apemat,
                    apellidoPaterno = _m.apepat,
                    contacto = new Contacto() { email = _m.email, celular = _m.celular },
                    direccion = _m.direccion,
                    fechaIngreso = _m.fechaIngreso,
                    fechaNacimiento = _m.fechaNacimiento,
                    idMedico = _m.id,
                    especialidad = _m.especialidad
                });
            }

            return _lst;
        }
    }
}
