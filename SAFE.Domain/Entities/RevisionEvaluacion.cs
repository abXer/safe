﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Constrols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class RevisionEvaluacion
    {
        public int idTipoEvaluacion { get; set; }
        public long? idRevisionInstalacion { get; set; }
        public Ingeniero ingeniero { get; set; }
        public long? idEvaluacion { get; set; }
        public string recomendacion { get; set; }
        public Supervisor Supervisor { get; set; }
        public long? idEmpresa { get; set; }
    }

    public class RevisionEvaluacionFactory
    {
        internal static bool GuardarRevisionEvaluacion(RevisionEvaluacion revision)
        {
            RevisionEvaluacionModel _rev = new RevisionEvaluacionModel();

            /*
             https://safeservices.azurewebsites.net/ProyectoSAFE-1.0/AddAndUpdateRevEvaluacion

                Nota: tipoEvaluacion 1 = instalacion, 2 = trabajador
                Nota: Si id = null or 0  insert, id > 0 update
                
                
                {"id":null,
                "idIngeniero":1,
                "idEvaInstOTrab":25,
                "recomendaciones":"cotooooooooooo",
                "idSupervisor":1,
                "tipoRevEvaluacion":2}

             
             */

            _rev.tipoEvaluacion = revision.idTipoEvaluacion;
            _rev.idEvaInstOTrab = revision.idEvaluacion;
            _rev.recomendacion = revision.recomendacion;
            _rev.idSupervisor = revision.Supervisor?.idSupervisor;

            return EvaluacionDAO.AgregarRevision(_rev);
        }

        internal static IList<RevisionEvaluacion> listadoRevisiones(int rut, long idEvaluacion)
        {
            IList<RevisionEvaluacion> _lst = new List<RevisionEvaluacion>();

            foreach (RevisionEvaluacionModel rev in EvaluacionDAO.listadoRevisionesEvaluacion(rut))
            {
                _lst.Add(new RevisionEvaluacion()
                {
                    idEvaluacion = idEvaluacion,
                    idRevisionInstalacion = rev.idRevision,
                    idTipoEvaluacion = rev.tipoEvaluacion,
                    ingeniero = IngenieroControl.listaIngenieros().Single(x => x.idIngeniero == rev.idIngeniero),
                    recomendacion = rev.recomendacion,
                    Supervisor = SupervisorControl.listaExpositores().Single(x => x.idSupervisor == rev.idSupervisor),
                    idEmpresa = rev.idEmpresa
                });
            }

            return _lst;
        }
    }
}
