﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Examen
    {


        public long idExamen { get; set; }
        public string tipoExamen { get; set; }
        public string fechaExamen { get; set; }
        public long idFichaMedica { get; set; }
        public int idEmpresa { get; set; }
    }

    public class ExamenFactory
    {
        internal static IList<Examen> listadoExamenesEmpresa(int rutEmpresa)
        {
            IList<Examen> _lst = new List<Examen>();

            foreach (ExamenModel e in SaludDAO.listaExamenesEmpresa(rutEmpresa))
            {
                _lst.Add(new Examen()
                {
                    idExamen = e.id,
                    tipoExamen = e.tipoExamen,
                    fechaExamen = e.fechaExamen,
                    idFichaMedica = e.idFichaMedica,
                    idEmpresa = e.idEmpresa
                });
            }

            return _lst;
        }
    }
}
