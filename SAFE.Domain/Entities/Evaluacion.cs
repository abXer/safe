﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using SAFE.Domain.Constrols;
using SAFE.Domain.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Evaluacion
    {
        [Display(Name = "Tipo de evaluación")]
        [Required(ErrorMessage = "Debe seleccionar un tipo de evaluación")]
        [WhiteList(Enums.tipoEvaluacion.TRABAJADORES, Enums.tipoEvaluacion.INSTALACIONES, ErrorMessage = "Debe seleccionar un tipo de evaluacíon")]
        public int tipoEvaluacion { get; set; }
        public long? idEvaluacion { get; set; }
        public string fechaEvaluacion { get; set; }
        [Required(ErrorMessage = "Él informe es obligatorio")]
        [MinLength(5, ErrorMessage = "La observación es demasiado corta!")]
        public string Observaciones { get; set; }
        [Display(Name = "Instalación")]
        public Instalacion instalacion { get; set; }
        [Display(Name = "Tecnico")]
        public Tecnico tecnico { get; set; }

        [Display(Name = "Región")]
        public Region region { get; set; }
        [Display(Name = "Comuna")]
        public Comuna comuna { get; set; }
        public long idEmpresa { get; set; }
        public int idEtapa { get; set; }
        public string etapa { get; set; }

        public int informeOK { get; set; }
        public IList<RevisionEvaluacion> revisiones { get; set; }
    }

    public class EvaluacionFactory
    {
        internal static IList<Evaluacion> ObtenerEvaluacionesEmpresa(int rutEmpresa)
        {
            IList<Evaluacion> _evaluaciones = new List<Evaluacion>();
            var idEmpresa = (int)EmpresaControl.obtenerEmpresas().FirstOrDefault(x => x.rut.rut == rutEmpresa).idEmpresa;

            foreach (EvaluacionModel _eva in EvaluacionDAO.ObtenerEvaluacionRutEmpresa(rutEmpresa))
            {
                Evaluacion _e = new Evaluacion()
                {
                    idEvaluacion = Convert.ToInt64(_eva.id),
                    fechaEvaluacion = _eva.fechaEvaluacion,
                    Observaciones = _eva.observacion,
                    tipoEvaluacion = _eva.tipoEvaluacion,
                    tecnico = TecnicoControl.listaTecnicos().FirstOrDefault(x => x.idTecnico == _eva.idTecnico),
                    idEtapa = 0, //_eva.etapaEva,
                    etapa = "Técnico",
                    idEmpresa = idEmpresa,
                    //etapa = Enums.ETAPAEVALUACION.listaEtapasEvaluacion.Single(x => x.Key == _eva.etapaEva).Value,
                    comuna = new Comuna() { idComuna = _eva.idComuna, idRegion = _eva.idRegion },
                    region = new Region() { idRegion = _eva.idRegion }
                };

                if (_eva.tipoEvaluacion == Enums.tipoEvaluacion.INSTALACIONES)
                    _e.instalacion = EmpresaControl.obtenerInstalacionesEmpresa(idEmpresa).FirstOrDefault(x => x.idInstalacionEmpresa == _eva.idInstaoTraba);

                _evaluaciones.Add(_e);
            }

            return _evaluaciones;
        }

        internal static Evaluacion Obtener(int tipoEvaluacion, long idEvaluacion)
        {
            return EvaluacionControl.obtenerListaEvaluaciones(tipoEvaluacion, idEvaluacion);
        }

        internal static bool GuardarEvaluacion(Evaluacion evaluacion)
        {
            EvaluacionModel _eva = new EvaluacionModel();

            _eva.id = evaluacion.idEvaluacion;
            _eva.tipoEvaluacion = evaluacion.tipoEvaluacion;
            _eva.fechaEvaluacion = DateTime.Now.ToString();
            _eva.observacion = evaluacion.Observaciones;

            if (evaluacion.instalacion?.idInstalacionEmpresa != null)
            {
                _eva.idInstaoTraba = (long)evaluacion.instalacion.idInstalacionEmpresa;
                _eva.idInstalacion = (long)evaluacion.instalacion.idInstalacionEmpresa;
            }
            else
                _eva.idInstaoTraba = 1;


            _eva.idTecnico = evaluacion.tecnico.idTecnico;
            _eva.informeOK = evaluacion.informeOK;
            //_eva.etapaEva = evaluacion.idEtapa;
            _eva.idEmpresa = evaluacion.idEmpresa;

            bool r = EvaluacionDAO.GuardarEvaluacion(_eva);
            if (r)
                EvaluacionControl.ActualizarEvaluaciones(_eva.idEmpresa);
            return r;
        }

        internal static bool AgregarRevision(RevisionEvaluacion rev)
        {
            RevisionEvaluacionModel _rev = new RevisionEvaluacionModel();

            /*
             https://safeservices.azurewebsites.net/ProyectoSAFE-1.0/AddAndUpdateRevEvaluacion
                Nota: tipoEvaluacion 1 = instalacion, 2 = trabajador
                Nota: Si id = null or 0  insert, id > 0 update                
                
                {"id":null,
                "idIngeniero":1,
                "idEvaInstOTrab":25,
                "recomendaciones":"cotooooooooooo",
                "idSupervisor":1,
                "tipoRevEvaluacion":2}             
             */
            _rev.id = null;
            _rev.idIngeniero = rev.ingeniero.idIngeniero;
            _rev.idEvaInstOTrab = rev.idEvaluacion;
            _rev.recomendacion = rev.recomendacion;
            _rev.recomendaciones = rev.recomendacion;
            _rev.idSupervisor = rev.Supervisor.idSupervisor;
            _rev.tipoEvaluacion = rev.idTipoEvaluacion;

            return EvaluacionDAO.AgregarRevision(_rev);
        }
    }
}
