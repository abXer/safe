﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Supervisor : Persona
    {
        public long idSupervisor { get; set; }
    }

    public class SupervisorFactory
    {
        public static IList<Supervisor> listaSupervisores()
        {
            IList<Supervisor> _lst = new List<Supervisor>();

            foreach (SupervisorModel _sM in SupervisorDAO.listaSupervisores())
            {
                _lst.Add(new Supervisor()
                {
                    rut = new Rut() { rut = _sM.rut },
                    nombre = _sM.nombre,
                    apellidoMaterno = _sM.apemat,
                    apellidoPaterno = _sM.apepat,
                    contacto = new Contacto() { email = _sM.email, celular = _sM.celular },
                    direccion = _sM.direccion,
                    fechaIngreso = _sM.fechaIngreso,
                    fechaNacimiento = _sM.fechaNacimiento,
                    idSupervisor = _sM.idSupervisor
                });
            }

            return _lst;
        }
    }
}
