﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System.Collections.Generic;

namespace SAFE.Domain.Entities
{
    public class Trabajador : Persona
    {
        public long idTrabajador { get; set; }
        public long idEmpresa { get; set; }
        public string cargo { get; set; }
        public IList<FichaMedica> fichasMedicas { get; set; }
    }

    public class TrabajadorFactory
    {

        public static IList<Trabajador> listaTrabajadoresEmpresa(long idEmpresa)
        {
            IList<Trabajador> _listaTrabajadores = new List<Trabajador>();

            foreach (TrabajadorModel _t in TrabajadorDAO.listaTrabajadoresEmpresa(idEmpresa))
            {
                _listaTrabajadores.Add(new Trabajador()
                {
                    rut = new Rut() { rut = _t.rut },
                    nombre = _t.nombre,
                    apellidoMaterno = _t.apemat,
                    apellidoPaterno = _t.apepat,
                    contacto = new Contacto() { email = _t.email, celular = _t.celular },
                    direccion = _t.direccion,
                    fechaIngreso = _t.fechaIngreso,
                    fechaNacimiento = _t.fechaNacimiento,
                    idTrabajador = _t.id,
                    cargo = _t.cargo,
                    idEmpresa = _t.idEmpresa
                });
            }

            return _listaTrabajadores;
        }
    }
}