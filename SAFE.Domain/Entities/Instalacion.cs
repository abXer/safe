﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Instalacion
    {
        public long idInstalacionEmpresa { get; set; }
        public Contacto contacto { get; set; }
        public string direccion { get; set; }
        public Comuna comuna { get; set; }
        public long idEmpresa { get; set; }
    }
}
