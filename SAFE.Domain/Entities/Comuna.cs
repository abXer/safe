﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Comuna
    {
        [Required(ErrorMessage = "Campo requerido")]
        [RegularExpression(@"^[1-9][0-9]*$", ErrorMessage = "Campo requerido")]

        public int idComuna { get; set; }
        public string comuna { get; set; }
        public int idRegion { get; set; }
        public Comuna() { }
    }

    public class ComunaFactory
    {
        public static IList<Comuna> listadoComunas(int idRegion)
        {
            IList<Comuna> _lst = new List<Comuna>();

            foreach (ComunaModel _com in ComunaDAO.listaComunas(idRegion))
            {
                _lst.Add(new Comuna
                {
                    idComuna = _com.id,
                    comuna = _com.nombre,
                    idRegion = idRegion
                });
            }

            return _lst;
        }

        public static Comuna Obtener(int idComuna)
        {
            Comuna _com = null;
            ComunaModel com = ComunaDAO.obtenerComuna(idComuna);

            if (com != null)
            {
                _com = new Comuna()
                {
                    idComuna = com.id,
                    comuna = com.nombre,
                    idRegion = com.id
                };
            }

            return _com;
        }
    }
}
