﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Domain.Constrols;

namespace SAFE.Domain.Entities
{
    public class FichaMedica
    {

        public long? idFichaMedica { get; set; }
        public string diagnostico { get; set; }
        public long idTrabajador { get; set; }
        public long idAtencionMedica { get; set; }
        public Medico medico { get; set; }
        public long idEmpresa { get; set; }
        public IList<Examen> examenes { get; set; }
    }

    public class fichaMedicaFactory
    {

        public static IList<FichaMedica> listadoFichasMedicasEmpresa(int rutEmpresa)
        {
            IList<FichaMedica> _lst = new List<FichaMedica>();

            Empresa emp = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == rutEmpresa);
            var idEmpresa = (int)emp.idEmpresa;
            foreach (FichaMedicaModel fm in SaludDAO.listaFichasMedicasEmpresa(emp.rut.rut))
            {
                var ficha = new FichaMedica()
                {
                    idFichaMedica = (long)fm.id,
                    diagnostico = fm.diagnostico,
                    idTrabajador = (long)fm.idTrabajador,
                    idAtencionMedica = (long)fm.idAtencionMedica,
                    idEmpresa = idEmpresa
                };

                var atencionMedica = AtencionMedicaEmpresaControl.obtenerAtencionesMedicasEmpresa(idEmpresa).FirstOrDefault(x => x.idEmpresa == idEmpresa);
                var agendaMedica = new AgendaMedica();

                if (atencionMedica != null)
                    agendaMedica = AgendaMedicaControl.agendaMedicaEmpresa(emp.rut.rut)
                        .FirstOrDefault(x => x.idAgendaMedica == atencionMedica.idAgendaMedica);

                ficha.medico = agendaMedica.medico;

                _lst.Add(ficha);
            }

            return _lst;
        }

        public static bool Guardar(FichaMedica ficha)
        {
            FichaMedicaModel _ficha = new FichaMedicaModel();

            _ficha.id = ficha.idFichaMedica;
            _ficha.diagnostico = ficha.diagnostico;
            _ficha.idTrabajador = ficha.idTrabajador;
            _ficha.idAtencionMedica = ficha.idAtencionMedica;
            _ficha.idEmpresa = ficha.idEmpresa;

            var exito = SaludDAO.guardarFichaMedica(_ficha);

            if (exito)
                TrabajadorControl.ActualizarFichasMedica((int)_ficha.idEmpresa);


            return exito;
        }
    }
}
