﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Dao;
using SAFE.Dao.Models;

namespace SAFE.Domain.Entities
{
    public class Token
    {
        public bool cumple { get; set; }
        public string token { get; set; }
        public string tipoToken { get; set; }
    }

    public class TokenFactory
    {
        public static Token Autenticar(Rut rut, string pass)
        {
            var token = new Token();

            var _token = new TokenModel()
            {
                rutUsuario = rut.rut,
                password = pass
            };

            var respuesta = UsuarioDAO.Autenticar(_token); ;


            return token;
        }
    }
}
