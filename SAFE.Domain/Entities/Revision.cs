﻿using System.Collections.Generic;

namespace SAFE.Domain.Entities
{
    public class Revision
    {
        public long idRevision { get; set; }
        public string tipoRevision { get; set; }
        public Ingeniero ingeniero { get; set; }
    }

    public class RevisionFactory
    {
        public static Revision Obtener(long idRevision)
        {
            return new Revision();
        }

        public static IList<Revision> ObtenerTodas(long rutEmpresa)
        {
            return new List<Revision>();
        }

        public static bool Guardar(Revision revision)
        {
            return true;
        }
    }
}