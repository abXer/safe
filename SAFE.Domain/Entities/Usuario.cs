﻿using SAFE.Dao.Dao;
using SAFE.Domain.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using SAFE.Dao.Models;
using SAFE.Domain.Constrols;
using SAFE.Domain.Forms;

namespace SAFE.Domain.Entities
{
    public class Usuario
    {
        public int id { get; set; }
        public string password { get; set; }
        public string estado { get; set; }
        public Persona persona { get; set; }
        public Empresa empresa { get; set; }
        public Perfil perfil { get; set; }
        internal string _token { get; set; }
        internal string _fechaExpiracion { get; set; }

        public bool autenticado => (!string.IsNullOrEmpty(this._token) && !string.IsNullOrEmpty(this._fechaExpiracion) && DateTime.Parse(this._fechaExpiracion) > DateTime.Now);


    }

    public class UsuarioFactory
    {
        internal static bool Guardar(Usuario user)
        {
            return true;
        }

        internal static IList<Usuario> listaUsuarios()
        {
            IList<Usuario> _listaUsuarios = new List<Usuario>();

            foreach (var _u in UsuarioDAO.listaUsuarios())
            {
                var u = new Usuario();
                u.id = _u.usuario.id;
                u.estado = _u.usuario.estado;
                u.perfil = UsuarioControl.listaPerfiles().FirstOrDefault(x => x.idPerfil == _u.usuario.idPerfil);

                if (_u.persona != null)
                {
                    u.persona = new Persona()
                    {
                        rut = new Rut() { rut = _u.persona.rut },
                        nombre = _u.persona.nombre,
                        apellidoPaterno = _u.persona.apepat,
                        apellidoMaterno = _u.persona.apemat,
                        contacto = new Contacto() { email = _u.persona.email, celular = _u.persona.celular },
                        direccion = _u.persona.direccion,
                        fechaIngreso = _u.persona.fechaIngreso,
                        fechaNacimiento = _u.persona.fechaNacimiento
                    };
                }

                if (_u.empresa != null)
                {
                    u.empresa = EmpresaControl.obtenerEmpresas().Single(x => x.rut.rut == _u.empresa.rut);
                }

                _listaUsuarios.Add(u);
            }
            return _listaUsuarios;
        }

        public static Usuario Autenticar(long rut, string password)
        {
            var usuario = new Usuario();

            var _autentica = UsuarioDAO.Autenticar(new TokenModel()
            {
                rutUsuario = rut,
                password = password
            });

            if (_autentica?.loginCorrecto == true)
            {
                usuario = FormServiciosGeneralesFactory.obtenerListaUsuarios().Single(x => x.persona?.rut?.rut == rut || x.empresa?.rut?.rut == rut);
                usuario._token = _autentica.token;
                usuario._fechaExpiracion = _autentica.fechaExpiracion;
            }

            return usuario;
        }

        public static IList<Perfil> listaPerfiles()
        {
            return PerfilFactory.listaPerfiles();
        }

        public static IList<Menu> listaMenu()
        {
            return MenuFactory.listaMenus();
        }

        public static Dictionary<int, int> listaAsociacionPerfilMenu()
        {
            return PerfilFactory.listaAsociacionMenus();
        }
    }
}
