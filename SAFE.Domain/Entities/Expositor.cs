﻿using SAFE.Dao.Dao;
using SAFE.Dao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Entities
{
    public class Expositor : Persona
    {
        public long idExpositor { get; set; }
    }

    public class ExpositorFactory
    {
        public static IList<Expositor> listaExpositores()
        {
            IList<Expositor> _lst = new List<Expositor>();

            foreach (ExpositorModel _eM in ExpositorDAO.listaExpositores())
            {
                _lst.Add(new Expositor()
                {
                    rut = new Rut() { rut = _eM.rut },
                    nombre = _eM.nombre,
                    apellidoMaterno = _eM.apemat,
                    apellidoPaterno = _eM.apepat,
                    contacto = new Contacto() { email = _eM.email, celular = _eM.celular },
                    direccion = _eM.direccion,
                    fechaIngreso = _eM.fechaIngreso,
                    fechaNacimiento = _eM.fechaNacimiento,
                    idExpositor = _eM.id
                });
            }

            return _lst;
        }
    }
}
