﻿using SAFE.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAFE.Domain.Interfaces
{
    public interface IFormValidate
    {
        List<Observacion> ValidarForm(object form);
    }
}
