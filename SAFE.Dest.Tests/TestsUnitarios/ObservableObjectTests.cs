﻿namespace SAFE.Desk.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ObservableObjectTests
    {
        [TestMethod]
        public void PropertyChangedEventHandlerIsRaised()
        {
            var obj = new StubObservableObject();
            bool raised = false;
            obj.PropertyChanged += (sender, e) =>
            {
                Assert.IsTrue(e.PropertyName == "ChangedProperty");
                raised = true;
            };
            obj.ChangedProperty = "Algun valor";
            if (!raised) Assert.Fail("PropertyChanged no ha sido invocada");
        }
        class StubObservableObject : ObservableObject
        {
            private string changedProperty;
            public string ChangedProperty
            {
                get { return changedProperty; }
                set {
                    changedProperty = value;
                    NotifyPropertyChanged();
                    }
            }
        }

    }

   
}
